// TransferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "TransferDlg.h"
#include ".\transferdlg.h"
#include "ComThread.h"
#include "Usefull.h"
#include "SampleSuiteForms.h"

// CTransferDlg dialog

IMPLEMENT_DYNAMIC(CTransferDlg, CDialog)
CTransferDlg::CTransferDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTransferDlg::IDD, pParent)
	, m_csFile(_T(""))
{
	ASSERT(pParent != NULL);

	m_bQuit = FALSE;
	m_bAbort = FALSE;
	m_bError = FALSE;

	m_pCComThread = NULL;
	m_csFilename = _T("");
	m_csFilenameShort = _T("");
	m_csDestpath = _T("");
	m_nLength = 0;
	m_nDirection = 0;
	m_nType = 0;
	m_nDevice = 0;

	m_nSerial = -1;
	m_nProdID = -1;
	m_nLevel = -1;
	m_nCode = -1;
	m_csLang = _T("");

	// dialog text
	m_csFile = _T("");
	m_csComport = _T("");
	m_csBaudrate = _T("");
	m_csLicense = _T("");
	m_csStatus = _T("");
}

CTransferDlg::~CTransferDlg()
{
}

void CTransferDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_TRANSFER, m_cProgress);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_FILE2, m_csFile);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_COMPORT2, m_csComport);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_BAUDRATE2, m_csBaudrate);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_LICENSE2, m_csLicense);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_STATUS2, m_csStatus);
}


BEGIN_MESSAGE_MAP(CTransferDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_TRANSFER, OnBnClickedButtonTransfer)
	ON_MESSAGE(WM_COM_PACKET, OnPacket)
	ON_MESSAGE(WM_COM_LENGTH, OnLength)
	ON_MESSAGE(WM_COM_DONE, OnDone)
	ON_MESSAGE(WM_COM_FILE, OnFile)
END_MESSAGE_MAP()


// CTransferDlg message handlers
void CTransferDlg::OnCancel()
{
	// Abort the filetransfer and close the dialog.
	if(m_bQuit == FALSE)
		m_pCComThread->Quit();

	if(m_bAbort == TRUE || m_bError == TRUE)
		CDialog::OnCancel();
	else
		CDialog::OnOK();
}


void CTransferDlg::OnBnClickedButtonTransfer()
{
	if(m_bQuit == TRUE) return;
	m_bQuit = TRUE;
	m_bAbort = TRUE;

	// Abort the filetransfer
	m_pCComThread->Quit();

	OnCancel();
}

BOOL CTransferDlg::OnInitDialog()
{
	SetWindowText(g_pXML->str(1021));
	SetDlgItemText(IDC_BUTTON_TRANSFER, g_pXML->str(1023));
	SetDlgItemText(IDC_STATIC_TRANSFER_FILE, g_pXML->str(1067));
	SetDlgItemText(IDC_STATIC_TRANSFER_COMPORT, g_pXML->str(1068));
	SetDlgItemText(IDC_STATIC_TRANSFER_BAUDRATE, g_pXML->str(1069));
	SetDlgItemText(IDC_STATIC_TRANSFER_LICENSE, g_pXML->str(1070));
	SetDlgItemText(IDC_STATIC_TRANSFER_STATUS, g_pXML->str(1071));


	if(m_nDevice == 0)	// DigitechPro
	{
		m_csBaudrate = theApp.m_csDP_Baudrate;
		m_csComport = theApp.m_csDP_Comport;
	}
	else if(m_nDevice == 1)	// Mantax computer
	{
		m_csBaudrate = theApp.m_csMC_Baudrate;
		m_csComport = theApp.m_csMC_Comport;
	}

	if(m_nType == 0)	// kermit
	{
		if(m_nDirection == 0)	// upload
		{
			if(m_nCode == -1)
			{
				m_csFile.Format(_T("%s"), m_csFilenameShort);
				m_csStatus.Format(_T("%s"), g_pXML->str(1029));
			}
			else	// we got a keycode and a serialnumber
			{
				GetDlgItem(IDC_STATIC_TRANSFER_LICENSE)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_TRANSFER_LICENSE2)->ShowWindow(SW_SHOW);

				m_csFile.Format(_T("%s"), m_csFilenameShort);
				m_csLicense.Format(_T("%s %d, %s %d"), g_pXML->str(1013), m_nSerial, g_pXML->str(1015), m_nCode);
				m_csStatus.Format(_T("%s"), g_pXML->str(1029));
			}
		}
		else if(m_nDirection == 1)	// download
		{
			m_csStatus.Format(_T("%s"), g_pXML->str(1029));
		}
	}
	else if(m_nType == 2)	// inet
	{
		m_csFile.Format(_T("%s"), m_csFilenameShort);
		m_csComport.Format(_T("%s"), g_pXML->str(1072));

		m_csBaudrate.Format(_T(""));
		GetDlgItem(IDC_STATIC_TRANSFER_BAUDRATE)->ShowWindow(FALSE);

		m_csStatus.Format(_T("%s"), g_pXML->str(1030));
	}
	else if(m_nType == 3)	// local
	{
		m_csBaudrate.Format(_T(""));
		GetDlgItem(IDC_STATIC_TRANSFER_BAUDRATE)->ShowWindow(FALSE);

		m_csFile.Format(_T("%s"), m_csFilenameShort);
		m_csStatus.Format(_T("%s"), g_pXML->str(1030));
	}


	CDialog::OnInitDialog();


	int pos = 0;
	CString csBuf = m_csComport.Tokenize(_T("COM:"), pos);

 	m_pCComThread = (CComThread*) ::AfxBeginThread( RUNTIME_CLASS( CComThread ), 0);
	m_pCComThread->m_hWnd = this->m_hWnd;
	m_pCComThread->m_csFilename = m_csFilename;
	m_pCComThread->m_csFilenameShort = m_csFilenameShort;
	m_pCComThread->m_nDirection = m_nDirection;
	m_pCComThread->m_csDestpath = m_csDestpath;

	m_pCComThread->m_nSerial = m_nSerial;
	m_pCComThread->m_nCode = m_nCode;
	m_pCComThread->m_nLevel = m_nLevel;
	m_pCComThread->m_nProdID = m_nProdID;
	m_pCComThread->m_csLang = m_csLang;

	m_pCComThread->m_nType = m_nType;
	m_pCComThread->m_hWnd = GetSafeHwnd();
	m_pCComThread->m_nBaud = CStringToInt(m_csBaudrate);
	m_pCComThread->m_nComport = CStringToInt(csBuf);
	m_pCComThread->DoContinuous();

	m_cProgress.SetRange32(0, 1);
	m_cProgress.SetPos(0);

	return TRUE;
}

// we have recieved a packet.
LRESULT CTransferDlg::OnPacket(WPARAM wParm, LPARAM lParm)
{
	if(m_nDirection == 1)	// download?
	{
		m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1030), (int)wParm);

		if(m_nType != 0)
			m_cProgress.SetPos((int)wParm);
	}
	else
	{
		m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1022), m_nLength);
		m_cProgress.SetPos((int)wParm);
	}
	
	UpdateData(FALSE);

	return 0;
}

// we have recieved a length.
LRESULT CTransferDlg::OnLength(WPARAM wParm, LPARAM lParm)
{
	if(m_nType == 0)	// kermit
	{
		if(m_nDirection == 0)	// up
		{
			if(m_nCode == -1)
			{
				m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), (int)wParm);
			}
			else	// we got a keycode and a serialnumber
			{
				m_csLicense.Format(_T("%s %d, %s %d"), g_pXML->str(1013), m_nSerial, g_pXML->str(1015), m_nCode);
				m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), (int)wParm);
			}

			m_nLength = (int)wParm;
		}
		else if(m_nDirection == 1)	// down
		{
			m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), (int)wParm);
		}
	}
	else if(m_nType == 2)	// inet
	{
		if(m_nDirection == 1)	// down
		{
			m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1030), (int)wParm);
		}
	}

	m_cProgress.SetRange32(0, (int)wParm);
	m_cProgress.SetPos(0);

	UpdateData(FALSE);

	return 0;
}

// the transfer is done
LRESULT CTransferDlg::OnDone(WPARAM wParm, LPARAM lParm)
{
	BOOL bError = FALSE;

	// transfer ended, tell why.
	switch(wParm)
	{
		case 0:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1066));
			bError = TRUE;
			break;
		case 1:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1031));
			break;
		case 2:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1032));
			bError = TRUE;
			break;
		case 3:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1033));
			bError = TRUE;
			break;
		case 4:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1034));
			bError = TRUE;
			break;
		case 5:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1035));
			bError = TRUE;
			break;
		case 6:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1036));
			bError = TRUE;
			break;
		case 7:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1037));
			bError = TRUE;
			break;
		case 8:
			MessageBox(g_pXML->str(1115), g_pXML->str(1021), MB_ICONSTOP);
			m_csStatus.Format(_T("%s"),	g_pXML->str(1115));	// forbidden without valid license
			bError = TRUE;
			break;
	}

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);

	if(bError == FALSE)
	{
		m_bError = FALSE;
		m_bAbort = FALSE;
		OnCancel();	// close the dialog
	}
	else
	{
		m_bError = TRUE;
	}

	UpdateData(FALSE);

	return 0;
}

// we got the filename we are receiving
LRESULT CTransferDlg::OnFile(WPARAM wParm, LPARAM lParm)
{
	m_csFilenameShort.Format(_T("%s"), lParm);

	m_csFile.Format(_T("%s"), m_csFilenameShort);
	m_csStatus.Format(_T("%s (0 Bytes)"), g_pXML->str(1030));

	UpdateData(FALSE);

	return 0;
}
