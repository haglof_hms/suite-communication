// InvRenamer.cpp : implementation file
//

#include "stdafx.h"
#include "InvRenamer.h"
#include "samplesuite.h"


// CInvRenamer dialog

IMPLEMENT_DYNAMIC(CInvRenamer, CDialog)

CInvRenamer::CInvRenamer(CWnd* pParent /*=NULL*/)
	: CDialog(CInvRenamer::IDD, pParent)
	, m_csOrigName(_T(""))
	, m_csNewName(_T(""))
	, m_csSubOrig(_T(""))
	, m_csSubNew(_T(""))
{
	 m_bSub = FALSE;
}

CInvRenamer::~CInvRenamer()
{
}

void CInvRenamer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ORIG, m_csOrigName);
	DDX_Text(pDX, IDC_EDIT_NEW, m_csNewName);
	DDX_Text(pDX, IDC_EDIT_SUB_ORIG, m_csSubOrig);
	DDX_Text(pDX, IDC_EDIT_SUB_NEW, m_csSubNew);
}


BEGIN_MESSAGE_MAP(CInvRenamer, CDialog)
END_MESSAGE_MAP()


// CInvRenamer message handlers

BOOL CInvRenamer::OnInitDialog()
{
	int nPos;

	if( ( nPos = m_csOrigName.Find(_T(".INV"))) != -1)
	{
		m_csOrigName = m_csOrigName.Left(nPos);
	}

	if( ( nPos = m_csOrigName.Find(_T("#"))) != -1)
	{
		m_csSubOrig = m_csOrigName.Right(m_csOrigName.GetLength() - nPos - 1);
		m_csOrigName = m_csOrigName.Left(nPos);

		GetDlgItem(IDC_EDIT_NEW)->EnableWindow(FALSE);

		m_bSub = TRUE;
	}
	else
	{
		GetDlgItem(IDC_EDIT_SUB_NEW)->EnableWindow(FALSE);

		m_bSub = FALSE;
	}

	m_csNewName = m_csOrigName;
	m_csSubNew = m_csSubOrig;


	CDialog::OnInitDialog();


	SetWindowText(g_pXML->str(1119));
	SetDlgItemText(IDC_STATIC_ORIG, g_pXML->str(1120));
	SetDlgItemText(IDC_STATIC_SUB, g_pXML->str(1122));


	GetDlgItem(IDC_EDIT_NEW)->SetFocus();

	return FALSE;
}

void CInvRenamer::OnOK()
{
	UpdateData(TRUE);

	if(m_bSub == TRUE && m_csSubNew == "")
	{
		MessageBox(g_pXML->str(1123), g_pXML->str(1101));
		return;
	}

	m_csNewName += _T("#") + m_csSubNew;

	CDialog::OnOK();
}