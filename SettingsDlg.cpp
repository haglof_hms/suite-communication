// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SettingsDlg.h"
#include ".\settingsdlg.h"
#include "CustomItems.h"

IMPLEMENT_DYNCREATE(CSettingsFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSettingsFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSettingsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame construction/destruction

XTPDockingPanePaintTheme CSettingsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CSettingsFrame::CSettingsFrame()
{
	m_bOnce = TRUE;
}

CSettingsFrame::~CSettingsFrame()
{
}

BOOL CSettingsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame diagnostics

#ifdef _DEBUG
void CSettingsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSettingsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame message handlers

int CSettingsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	return 0;
}

void CSettingsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Settings"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CSettingsFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

LRESULT CSettingsFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CSettingsFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	((CSettingsDlg*)GetActiveView())->OnSave();

	theApp.SaveRegSettings();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Settings"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

void CSettingsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CSettingsDlg

IMPLEMENT_DYNCREATE(CSettingsDlg, CXTResizeFormView)

CSettingsDlg::CSettingsDlg()
	: CXTResizeFormView(CSettingsDlg::IDD)
{
}

CSettingsDlg::~CSettingsDlg()
{
}

void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PLACEHOLDER, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsDlg, CXTResizeFormView)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()


// CSettingsDlg diagnostics

#ifdef _DEBUG
void CSettingsDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSettingsDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CSettingsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

LRESULT CSettingsDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMSETTINGS_COMMUNICATION);
	return 0;
}

void CSettingsDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	// get the size of the placeholder, this will be used when creating the grid.
	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );

	CString csBuf;

	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

		// Media category
		CString csBuf;
		int nIndex;
		if(theApp.m_bLocal == TRUE)
		{
			csBuf = g_pXML->str(1057);
			nIndex = 0;
		}
		else
		{
			csBuf = g_pXML->str(1058);
			nIndex = 1;
		}



		CXTPPropertyGridItem* pMedia = m_wndPropertyGrid.AddCategory(g_pXML->str(1055));
		m_pItemMedia = pMedia->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1056), csBuf));
		m_pItemMedia->SetDescription(g_pXML->str(1061));
		CXTPPropertyGridItemConstraints* pList = m_pItemMedia->GetConstraints();
		pList->AddConstraint(g_pXML->str(1057));
		pList->AddConstraint(g_pXML->str(1058));
		m_pItemMedia->SetFlags(xtpGridItemHasComboButton);
		pMedia->Expand();
		pList->SetCurrent(nIndex);
		m_pItemMedia->Select();


		// Settings category.
		CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(g_pXML->str(1041));

		// add child items to category.
		m_pItemServer = pPaths->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1051), theApp.m_csWebserver));
		m_pItemServer->SetDescription(g_pXML->str(1052));

		m_pItemPathWeb = pPaths->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1053), theApp.m_csWebpath));
		m_pItemPathWeb->SetDescription(g_pXML->str(1054));

		m_pItemPathLocal = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1059), theApp.m_csLocalpath));
		m_pItemPathLocal->SetDescription(g_pXML->str(1060));

		pPaths->Expand();


		if(theApp.m_bLocal == TRUE)
		{
			m_pItemServer->SetReadOnly(TRUE);
			m_pItemPathWeb->SetReadOnly(TRUE);
			m_pItemPathLocal->SetReadOnly(FALSE);
		}
		else
		{
			m_pItemServer->SetReadOnly(FALSE);
			m_pItemPathWeb->SetReadOnly(FALSE);
			m_pItemPathLocal->SetReadOnly(TRUE);
		}
	}

	// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}


// CSettingsDlg message handlers
void CSettingsDlg::OnSave()
{
}


void CSettingsDlg::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

void CSettingsDlg::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

LRESULT CSettingsDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_SORTORDER_CHANGED:
		{
		}
		break;

		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemMedia)
			{
				CXTPPropertyGridItemConstraints* pList = m_pItemMedia->GetConstraints();
				int nRet = pList->GetCurrent();

				if(nRet == 0)	// local
				{
					theApp.m_bLocal = TRUE;

					m_pItemServer->SetReadOnly(TRUE);
					m_pItemPathWeb->SetReadOnly(TRUE);
					m_pItemPathLocal->SetReadOnly(FALSE);
				}
				else	// web
				{
					theApp.m_bLocal = FALSE;

					m_pItemServer->SetReadOnly(FALSE);
					m_pItemPathWeb->SetReadOnly(FALSE);
					m_pItemPathLocal->SetReadOnly(TRUE);
				}
			}
			else if(pItem == m_pItemPathLocal)
			{
				theApp.m_csLocalpath = m_pItemPathLocal->GetValue();
			}
			else if(pItem == m_pItemServer)
			{
				theApp.m_csWebserver = m_pItemServer->GetValue();
			}
			else if(pItem == m_pItemPathWeb)
			{
				theApp.m_csWebpath = m_pItemPathWeb->GetValue();
				if(theApp.m_csWebpath.GetAt(theApp.m_csWebpath.GetLength()-1) != '/')
						theApp.m_csWebpath += _T("/");
			}
			else if(pItem == m_pItemPathLocal)
			{
				theApp.m_csLocalpath = m_pItemPathLocal->GetValue();
				if(theApp.m_csLocalpath.GetAt(theApp.m_csLocalpath.GetLength()-1) != '\\')
					theApp.m_csLocalpath += _T("\\");
			}
		}
		break;
	}

	return FALSE;
}
