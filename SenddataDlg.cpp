// TransferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "SenddataDlg.h"
#include "TransferDlg.h"
#include "Usefull.h"

IMPLEMENT_DYNCREATE(CSenddataFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSenddataFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSenddataFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSenddataFrame construction/destruction

CSenddataFrame::CSenddataFrame()
{
	m_bOnce = TRUE;
}

CSenddataFrame::~CSenddataFrame()
{
}

BOOL CSenddataFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSenddataFrame diagnostics

#ifdef _DEBUG
void CSenddataFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSenddataFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSenddataFrame message handlers

int CSenddataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CSenddataFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Senddata"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

LRESULT CSenddataFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CSenddataFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CSenddataFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Senddata"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CSenddataFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


LRESULT CSenddataFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	switch(wParam)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0L;
}


// CSenddataDlg

IMPLEMENT_DYNCREATE(CSenddataDlg, CXTResizeFormView)

CSenddataDlg::CSenddataDlg()
	: CXTResizeFormView(CSenddataDlg::IDD)
{
	//{{AFX_DATA_INIT(CSenddataDlg)
	//}}AFX_DATA_INIT
	
	m_csFilename = _T("");
	m_csFilenameShort = _T("");
	m_bQuit = FALSE;
	m_nLength = 0;
	m_bFile = FALSE;

	m_csOptions = _T("");

	// dialog text
	m_csFile = _T("");
	m_csComport = _T("");
	m_csBaudrate = _T("");
	m_csLicense = _T("");
	m_csStatus = _T("");

	m_bBLA = TRUE;
	m_bFOO = FALSE;
	m_bFirst = TRUE;
}

CSenddataDlg::~CSenddataDlg()
{
	// Abort the filetransfer and close the dialog.
	if(m_bFile == TRUE && m_bQuit == FALSE)
	{
		m_pCComThread->Quit();
	}
}

void CSenddataDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PROGRESS_TRANSFER, m_cProgress);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_FILE2, m_csFile);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_COMPORT2, m_csComport);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_BAUDRATE2, m_csBaudrate);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_LICENSE2, m_csLicense);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_STATUS2, m_csStatus);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSenddataDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramsView)
	//}}AFX_MSG_MAP
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_TRANSFER, OnBnClickedButtonTransfer)
	ON_MESSAGE(WM_COM_PACKET, OnPacket)
	ON_MESSAGE(WM_COM_LENGTH, OnLength)
	ON_MESSAGE(WM_COM_DONE, OnDone)
	ON_MESSAGE(WM_HELP, OnCommandHelp)
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_COM_FILE, OnFile)
END_MESSAGE_MAP()

LRESULT CSenddataDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMSEND_FILE);
	return 0;
}


// CSenddataDlg diagnostics
#ifdef _DEBUG
void CSenddataDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSenddataDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CSenddataDlg message handlers
BOOL CSenddataDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSenddataDlg::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
	theApp.m_bFinished = TRUE;
}


void CSenddataDlg::OnInitialUpdate()
{
	m_bFirst = TRUE;

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	if(pDoc->m_bExtra == TRUE) pDoc->m_nType = 0;

	if(pDoc->m_nType == 0)	// DigitechPro
	{
		if(pDoc->m_bExtra == TRUE)
			m_csFilename = theApp.m_csExtraArg;
		else
			m_csFilename = theApp.m_csDP_Downloadpath;	// digitechpro
		m_csBaudrate = theApp.m_csDP_Baudrate;
		m_csComport = theApp.m_csDP_Comport;

		PORTS port;
		CString csPort = theApp.m_csDP_Comport;
		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.csPort == m_csComport && port.nDP == 1)
			{
				m_csComport += _T(" (DP)");
				break;
			}
		}

		m_csOptions = theApp.m_csOptions;
	}
	else if(pDoc->m_nType == 1)	// Mantax computer
	{
		m_csFilename = theApp.m_csMC_Downloadpath;	// mantax computer
		m_csBaudrate = theApp.m_csMC_Baudrate;
		m_csComport = theApp.m_csMC_Comport;
	}
	else if(pDoc->m_nType == 2)	// Digitech
	{
		m_csFilename = theApp.m_csD_Downloadpath;	// digitech
		m_csBaudrate = theApp.m_csD_Baudrate;
		m_csComport = theApp.m_csD_Comport;
	}

	CXTResizeFormView::OnInitialUpdate();

	SetDlgItemText(IDC_BUTTON_TRANSFER, g_pXML->str(1023));
	SetDlgItemText(IDC_STATIC_TRANSFER_FILE, g_pXML->str(1067));
	SetDlgItemText(IDC_STATIC_TRANSFER_COMPORT, g_pXML->str(1068));
	SetDlgItemText(IDC_STATIC_TRANSFER_BAUDRATE, g_pXML->str(1069));
	SetDlgItemText(IDC_STATIC_TRANSFER_LICENSE, g_pXML->str(1070));
	SetDlgItemText(IDC_STATIC_TRANSFER_STATUS, g_pXML->str(1071));

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}

void CSenddataDlg::OnSetFocus(CWnd* pOldWnd)
{
	if(m_bFirst == TRUE)
	{
		m_bFirst = FALSE;

		if(pDoc->m_bExtra == FALSE)
		{
			OnBnClickedButtonFilechoose();

			if(m_bFile == TRUE)
			{
				m_bFOO = FALSE;

				SendFile(m_csFilename);

				m_csFile.Format(_T("%s"), m_csFilenameShort);
				m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), m_nLength);
			}
			else
			{
				m_csStatus.Format(_T("%s"), g_pXML->str(1065));

				m_bFOO = TRUE;
			}

			UpdateData(FALSE);
		}
		else
		{
			m_bFOO = FALSE;

			//m_csFilename = _T("C:\\Users\\anders.HAGLOF\\Desktop\\1.txt%C:\\Users\\anders.HAGLOF\\Desktop\\2.txt%C:\\Users\\anders.HAGLOF\\Desktop\\3.txt");

			SendFile(m_csFilename, m_csOptions);

			m_csFile.Format(_T("%s"), m_csFilenameShort);
			m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), m_nLength);

			UpdateData(FALSE);
		}
	}

	if(m_bFile == FALSE && m_bFOO == TRUE)
	{
		GetParentFrame()->PostMessage(WM_CLOSE);
		return;
	}

	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

// user has clicked the Abort-button
void CSenddataDlg::OnBnClickedButtonTransfer()
{
	// Abort the filetransfer and close the dialog.
	if(m_bFile == TRUE)
	{
		m_bFile = FALSE;
		m_bQuit = TRUE;

		m_pCComThread->Quit();
	}

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);

	GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

void CSenddataDlg::OnBnClickedButtonFilechoose()
{
	// show the filechooser
	TCHAR tzFilename[1024], tzTitle[50];
	_stprintf(tzFilename, _T("*.*"));
	_stprintf(tzTitle, g_pXML->str(1064));

	CFileDialog dlgF(TRUE);
	dlgF.m_ofn.lpstrTitle = tzTitle;
	dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("Files(*.*)\0*.*\0\0");
	dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
	dlgF.m_ofn.lpstrFile = tzFilename;
	if(pDoc->m_nType == 0) dlgF.m_ofn.lpstrInitialDir = theApp.m_csDP_Downloadpath;	// digitechpro
	else if(pDoc->m_nType == 1) dlgF.m_ofn.lpstrInitialDir = theApp.m_csMC_Downloadpath;	// mantax computer
	else if(pDoc->m_nType == 2) dlgF.m_ofn.lpstrInitialDir = theApp.m_csD_Downloadpath;	// digitech

	int nRet = dlgF.DoModal();
	if(nRet == IDOK)
	{
		m_bFile = TRUE;
		m_csFilename = dlgF.GetPathName();
	}
	else
	{
		m_bFile = FALSE;
	}
}

// we have recieved a packet.
LRESULT CSenddataDlg::OnPacket(WPARAM wParm, LPARAM lParm)
{
	m_cProgress.SetPos((int)wParm);

	return 0;
}

// we have recieved a length.
LRESULT CSenddataDlg::OnLength(WPARAM wParm, LPARAM lParm)
{
	m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1022), (int)wParm);

	m_cProgress.SetRange32(0, (int)wParm);
	m_cProgress.SetPos(0);
	m_nLength = (int)wParm;

	UpdateData(FALSE);

	return 0;
}

// the transfer is done
LRESULT CSenddataDlg::OnDone(WPARAM wParm, LPARAM lParm)
{
	BOOL bError = FALSE;

	// transfer ended, tell why.
	switch(wParm)
	{
		case 0:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1066));
			m_cProgress.SetPos(m_nLength);
			break;
		case 1:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1031));
			m_cProgress.SetPos(m_nLength);
			break;
		case 2:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1032));
			bError = TRUE;
			break;
		case 3:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1033));
			bError = TRUE;
			break;
		case 4:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1034));
			bError = TRUE;
			break;
		case 5:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1035));
			bError = TRUE;
			break;
		case 6:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1036));
			bError = TRUE;
			break;
		case 7:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1037));
			bError = TRUE;
			break;
	}

	m_bQuit = TRUE;	//@b ??
	theApp.m_bFinished = TRUE;

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);

	UpdateData(FALSE);

	return 0;
}

void CSenddataDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow, nStatus);
}

void CSenddataDlg::SendFile(CString csFilename, CString csOptions)
{
	m_bFile = TRUE;

	m_csFilename = csFilename;
//	m_csFilenameShort.Format(_T("%s"), csFilename.Right(csFilename.GetLength() - csFilename.ReverseFind('\\') - 1)  );

	int pos = 0;
	CString csBuf;
	csBuf = m_csComport.Tokenize(_T("COM:"), pos);

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(TRUE);

	m_pCComThread = (CComThread*) ::AfxBeginThread( RUNTIME_CLASS( CComThread ), 0 /*THREAD_PRIORITY_ABOVE_NORMAL*/ );
	m_pCComThread->m_hWnd = GetSafeHwnd();
	m_pCComThread->m_csFilename = m_csFilename;
//	m_pCComThread->m_csFilenameShort = m_csFilenameShort;
	m_pCComThread->m_nDirection = 0;	// up
	m_pCComThread->m_csDestpath = _T(""); //m_csFilename;
	m_pCComThread->m_nType = 0;	// kermit
	m_pCComThread->m_hWnd = GetSafeHwnd();
	m_pCComThread->m_nBaud = CStringToInt(m_csBaudrate);
	m_pCComThread->m_nComport = CStringToInt(csBuf);
	if(csOptions != "")
	{
		int nStart = 0, nI = 0;

		csBuf = csOptions.Tokenize(_T(";"), nStart);
		while(csBuf != _T(""))
		{
			if(nI == 0)
				m_pCComThread->m_nSerial = _tstoi(csBuf);
			else if(nI == 1)
				m_pCComThread->m_nCode = _tstoi(csBuf);
			else if(nI == 2)
				m_pCComThread->m_nLevel = _tstoi(csBuf);
			else if(nI == 3)
				m_pCComThread->m_nProdID = _tstoi(csBuf);

			nI++;
			csBuf = csOptions.Tokenize(_T(";"), nStart);
		}
	}

	m_pCComThread->DoContinuous();

//	m_csFile.Format(_T("%s"), m_csFilenameShort);
	m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1029), m_nLength);
}

void CSenddataDlg::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
	CXTResizeFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

// we got the filename we are receiving
LRESULT CSenddataDlg::OnFile(WPARAM wParm, LPARAM lParm)
{
	m_csFile = CString((LPTSTR)lParm);

	UpdateData(FALSE);

	return 0;
}
