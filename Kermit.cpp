// Serial.cpp

#include "stdafx.h"
#include "resource.h"
#include "Kermit.h"
#include "Samplesuite.h"
#include "InvRenamer.h"
#include "Usefull.h"

#define tochar(x) ((x)+32)
#define unchar(x) ((x)-32)
#define ctl(x) ((x) ^ 64)

#define inc(x) ((x+1) & 63)
#define dec(x) ((x-1) & 63)

#define KER_DATA 'D'
#define KER_ACK 'Y'
#define KER_NAK 'N'
#define KER_SEND_INIT 'S'
#define KER_EOT 'B'
#define KER_FILE 'F'
#define KER_EOF 'Z'
#define KER_ERROR 'E'
#define KER_INIT 'I'
#define KER_SRV_RECV 'R'


CKermit::CKermit()
{
	memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
 	memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );
	m_hIDComDev = NULL;
	m_bOpened = FALSE;
	m_hWnd = NULL;
}

CKermit::~CKermit()
{
	Close();
}

BOOL CKermit::HangUp()
{
	Sleep(1000);

	// hang up
	char szBuf[15];
	sprintf(szBuf, "+++");
	SendData(szBuf, strlen(szBuf));

	Sleep(1000);

	sprintf(szBuf, "ATH0\r");
	SendData(szBuf, strlen(szBuf));

	return TRUE;
}


BOOL CKermit::Open(int nComport, int nBaud)
{
	if( m_bOpened ) return( TRUE );

	DCB dcb;
	CString csBuf;
	csBuf.Format(_T("//./COM%d"), nComport);

	m_hIDComDev = CreateFile(csBuf, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, /*FILE_FLAG_OVERLAPPED*/0, NULL );
	if( m_hIDComDev == NULL || m_hIDComDev == INVALID_HANDLE_VALUE) return( FALSE );

	memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
 	memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );

	COMMTIMEOUTS CommTimeOuts;
	CommTimeOuts.ReadIntervalTimeout = 100;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 3000;
	SetCommTimeouts( m_hIDComDev, &CommTimeOuts );


	m_OverlappedRead.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	m_OverlappedWrite.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );

	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );

	dcb.BaudRate = nBaud;
	dcb.ByteSize = 8;
	dcb.fParity = FALSE;
	dcb.fBinary = TRUE;
	dcb.fNull = FALSE;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	dcb.fRtsControl = RTS_CONTROL_ENABLE;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;
	dcb.fOutxCtsFlow = FALSE;         // No CTS output flow control 
	dcb.fOutxDsrFlow = FALSE;         // No DSR output flow control 
	dcb.fDsrSensitivity = FALSE;      // DSR sensitivity 
	dcb.fTXContinueOnXoff = TRUE;     // XOFF continues Tx 
	dcb.fOutX = FALSE;                // No XON/XOFF out flow control 
	dcb.fInX = FALSE;                 // No XON/XOFF in flow control 
	dcb.fErrorChar = FALSE;           // Disable error replacement. 
	dcb.fAbortOnError = FALSE;        // Do not abort reads/writes on error.

	if( !SetCommState( m_hIDComDev, &dcb ) ||
		!SetupComm( m_hIDComDev, 10000, 10000 ) ||
		m_OverlappedRead.hEvent == NULL ||
		m_OverlappedWrite.hEvent == NULL )
	{
		DWORD dwError = GetLastError();
		if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
		if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
		
		CloseHandle( m_hIDComDev );
		return( FALSE );
	}

	EscapeCommFunction (m_hIDComDev, SETDTR);
	EscapeCommFunction (m_hIDComDev, SETRTS);

	// clear the buffer
	Sleep(300);
	PurgeComm(m_hIDComDev, PURGE_RXCLEAR|PURGE_TXCLEAR);
	Sleep(300);

	m_bOpened = TRUE;
	m_bKermitRunning = TRUE;

	return( m_bOpened );
}

BOOL CKermit::Close( void )
{
	if( !m_bOpened || m_hIDComDev == NULL ) return( TRUE );

	if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
	if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
	CloseHandle( m_hIDComDev );
	m_bOpened = FALSE;
	m_hIDComDev = NULL;

	return( TRUE );
}

BOOL CKermit::WriteCommByte( unsigned char ucByte )
{
	BOOL bWriteStat;
	DWORD dwBytesWritten;

	bWriteStat = WriteFile(m_hIDComDev, (LPSTR) &ucByte, 1, &dwBytesWritten, NULL);
	if( !bWriteStat && (GetLastError() == ERROR_IO_PENDING) )
	{
		if( WaitForSingleObject( m_OverlappedWrite.hEvent, 1000 ) ) dwBytesWritten = 0;
		else
		{
			GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, FALSE );
			m_OverlappedWrite.Offset += dwBytesWritten;
		}
	}

	return( TRUE );
}

int CKermit::SendData( const char *buffer, int size )
{
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );

	DWORD dwBytesWritten = 0;
	BOOL bWriteStat;
	bWriteStat = WriteFile(m_hIDComDev, buffer, size, &dwBytesWritten, NULL);
	if( !bWriteStat && (GetLastError() == ERROR_IO_PENDING) )
	{
		if( WaitForSingleObject( m_OverlappedWrite.hEvent, 1000 ) )
			dwBytesWritten = 0;
		else
		{
			GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, FALSE );
			m_OverlappedWrite.Offset += dwBytesWritten;
		}
	}

	return( (int) dwBytesWritten );
}

int CKermit::ReadDataWaiting( void )
{
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );

	DWORD dwErrorFlags;
	COMSTAT ComStat;

	ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );

	return( (int) ComStat.cbInQue );
}

int CKermit::ReadData(char *buffer, int limit )
{
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );
	
	BOOL bReadStatus;
	DWORD dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;
	
	ClearCommError(m_hIDComDev, &dwErrorFlags, &ComStat );
	if( !ComStat.cbInQue ) return( 0 );
	
	dwBytesRead = (DWORD) ComStat.cbInQue;
	if( limit < (int) dwBytesRead ) dwBytesRead = (DWORD) limit;
	
	bReadStatus = ReadFile(m_hIDComDev, buffer, dwBytesRead, &dwBytesRead, NULL);
	if( !bReadStatus )
	{
		if(GetLastError() == ERROR_IO_PENDING )
		{
			WaitForSingleObject( m_OverlappedRead.hEvent, 1000 );
			return( (int) dwBytesRead );
		}
		return( 0 );
	}
	
	return( (int) dwBytesRead );
}

void CKermit::movmem(unsigned char *src,unsigned char *dest,unsigned len)
{
 if(len)
  while(len--)
    *dest++=*src++;

}
void CKermit::setmem(unsigned char *dest,unsigned len,unsigned char value)
{
 if(len)
  while(len--)
   *dest++=value;
}


////////////////////////////////////////////////////////////////////////

/*********************************************************
 server
**********************************************************/
/*****************************************************
* Server Avbryt med ENTER                            *
******************************************************/
int CKermit::ker_server(char *file_name,unsigned char *area,int size)
{
	int res = 0;
	res = _server(file_name, area, size, 1);
	
	if(ker_filename[0] != 0)
		strcpy(file_name, (char*)ker_filename);

	return res;
}
int CKermit::_server(char *file_name,unsigned char *area,int size,int timeout)
{
	int rec_size;
	strncpy((char *)ker_filename,file_name,127);
	
	switch(getcommand(file_name,area,size,timeout))
	{
	case 'S':
		if ((rec_size=ker_get_file_data(area, size,0)) < 0L)
		{
//AfxMessageBox(_T("_server S"));
			return ker_error("ERR: GETDATA");
		}
		if(ker_get_break())
			return ker_error("ERR: GETBRK"); 
		return rec_size;
	case 'R':
		ker_set_default();
		if(ker_send_init(KER_SEND_INIT))
			return ker_error("ERR: SNDINIT");
		if (ker_send_file_header('F',(char *)ker_filename))
			return ker_error("ERR: FILEHDR");
		if (ker_send_file_data(area, size))
			return ker_error("ERR: SNDDATA");
		if (ker_send_break())
			return ker_error("ERR: SNDBRK!");
		return 0l; 	
		
	case 'G':
		ker_set_default();
		if(ker_filename[0]!=0)

		switch(ker_filename[0])
		{
		case 'L':
		case 'B':  
		strncpy((char *)ker_filename,"LOGOUT      ",127);
		ker_tpack(0, k.ker_seq, KER_ACK,NULL);
		return 0; 
		
		case 'D':
			break;
		default:
			ker_tpack(15, k.ker_seq,KER_ERROR, (unsigned char*)"UNKNOWN COMMAND");	/*  */
			return ker_error("ERR: UNKNOWN");
		}
	
		if(ker_send_init(KER_SEND_INIT))
			return ker_error("ERR: SNDINIT");
		if (ker_send_file_header('X',""))
			return ker_error("ERR: FILEHDR");
		strcpy((char *)ker_filename,file_name);
		strcat((char *)ker_filename,"\r\n");
		
		if (ker_send_file_data(ker_filename,(unsigned long)strlen((char *)ker_filename)))
			return ker_error("ERR: SNDDATA");
		if (ker_send_break())
			return ker_error("ERR: SNDBRK!");
		return 0; 	
		
	case -2:
		return ker_error("ERR: USRBRK");
		
	case -1:
//AfxMessageBox(_T("case -1"));
		return -1;

	default:
		ker_tpack(15, k.ker_seq, KER_ERROR, (unsigned char*)"ERROR FROM HOST");
        return ker_error("ERR: ERROR");
		
	}
}

int CKermit::getcommand(char *file_name,unsigned char *area,int size,int timeout)
{
	int i=0;
	int dlen, rseq, rtype,j;
	dlen=0;
	ker_set_default();
	
	if(timeout)
		j=1;
	else 
		j=0;
	ker_tpack(0,k.ker_seq,KER_NAK, NULL);    
	for(i=0;i<3;i+=j)
	{
		if(ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
		{
			ker_tpack(0,k.ker_seq,KER_NAK, NULL);    
		}
		else
		{
			if (rseq == 0&&(rtype=='S'||rtype=='R'||rtype=='G'))
				goto good_packet;
			else
				if(rseq==0&&rtype=='I')
				{
					ker_prcv_param(dlen);
					spar();
					ker_tpack(9,k.ker_seq, KER_ACK,k.ker_tbuf);
				}
				else
					if (rtype == KER_ERROR)
						goto error_packet;
					else
					{
						ker_tpack(15,k.ker_seq,KER_ERROR, (unsigned char*)"UNKNOWN COMMAND");	/*  */
						ker_set_default();
					};       
		}    
	}
error_packet:
	if(strstr(k.ker_last_error, "QUIT") == NULL)
	{
		movmem(k.ker_rbuf, (unsigned char *)k.ker_last_error, dlen);
		k.ker_last_error[dlen] = 0;
	}
	return -1;
	
good_packet:
	if(rtype=='R'||rtype=='G')
	{
		i=ker_unpack_data(ker_filename,127,dlen);
		ker_filename[i]='\0';
		strncpy(k.ker_last_error,(char *)ker_filename,127);
	}
	else
	{
		ker_prcv_param(dlen);
		spar();
		ker_tpack(9, k.ker_seq, KER_ACK,k.ker_tbuf);
	}
	return rtype;   
}



int CKermit::ker_get_file(char *file_name, unsigned char *area, int size,int command)
{
	int res;
	res=get_ker_file_(file_name,area,size,command);
	return res;
}

int CKermit::get_ker_file_(char *file_name, unsigned char *area, int size,int command)
{
	int i;
	int rec_size;
	ker_set_default();
	if (ker_send_init(KER_INIT))
		return -1;//ker_error("ERR: SNDINIT");
	if ((i=ker_send_rcv_init(file_name,command))==-1)
		return -1;//ker_error("ERR: FILEHDR");
	else
		if(i==-2)
			return 0l;
		if ((rec_size = ker_get_file_data(area, size,command)) < 0L)
			return -1;//ker_error("ERR: GETDATA");
		if (ker_get_break())
			return -1;//ker_error("ERR: GETBRK"); 
		return rec_size;
}

int CKermit::ker_send_file(char *file_name,unsigned char *area,int size)
{
	int res;
	res=ker_send_file_(file_name,area,size);

	if(res == -1)
	{
		m_bKermitRunning = FALSE;
		ker_send_break();
	}
	
	return res;
}
int CKermit::ker_send_file_(char *file_name,unsigned char *area,int size)
{
	ker_set_default();
	ker_error(file_name); //visa filnamn, ej fel
	k.ker_file_len=size;
	if (ker_send_init(KER_SEND_INIT))
		return -1;	// ker_error("ERR: SNDINIT");
	if (ker_send_file_header(KER_FILE, file_name))
		return -1;	//ker_error("ERR: FILEHDR");
	if (ker_send_file_data(area, size))
		return -1;	//ker_error("ERR: SNDDATA");
	if (ker_send_break())
		return -1;	//ker_error("ERR: SNDBRK!");
	return 0;
}

int CKermit::ker_error(char *s)
{
	k.ker_retry_counter++;
	sprintf(k.ker_last_error,"%13s",s);
	return -1;
}

int CKermit::read_ker_char_()
{
	unsigned char szBuf[3];
	int nCharsRead;
	szBuf[1] = 0;

	while(1)
	{
		if((nCharsRead = ReadData((char*)&szBuf, 1)) != 0)
		{
			return (int)szBuf[0];
		}

		SystemIdle();

		if(m_bKermitRunning == FALSE)
			return ker_error("QUIT");
	}  

	return -1;
}
/*ker getch*/
int CKermit::ker_getc()
{
	int i;
	if ((i=read_ker_char_())!=-1) 
	{
		k.ker_check += i;
		return i;
	}
	return -1;		
}

/*send*/
void CKermit::ker_putc(int ch)
{
	k.ker_check += ch;
	WriteCommByte(ch);
}

void CKermit::ker_stimeout()
{
}


void CKermit::ker_set_default()
{
	m_bKermitRunning = TRUE;

	setmem((unsigned char *)&k,sizeof(k),0);
	k.ker_last_error[0] = 0;
	k.ker_seq = 0;
	k.ker_pstart = 0x01;	/* Ctrl-A */
	k.ker_eol = 0x0d;
	k.ker_timeout =5;
	k.ker_maxpacket=94;
	k.ker_npad = 0;
	k.ker_padc = 0;
	k.ker_rx_qctl = k.ker_tx_qctl = '#';
	if (k.ker_qbflg)
		k.ker_qbin = '&';
	else
		k.ker_qbin = 0;
	k.ker_rept = 0;
	k.ker_retry = 5;
	k.ker_my_rept = '~';
	
	k.ker_file_len=0;
	k.ker_bytes=0;
	
	k._rtype=' ';//info vid redraw
}


/* Funktion f�r att ta emot ett KERMIT-paket */
int CKermit::ker_rpack(int *dlen, int *pseq,int *ptype, unsigned char *pdata)
{
	int i, j, ch;
	unsigned char *dp;
	ker_stimeout();
	
	while ((ch=ker_getc())!= k.ker_pstart)
	{
		SystemIdle();

		if(m_bKermitRunning == FALSE)
			return ker_error("QUIT");

		if(ch==-1) return ker_error("TIMEOUT");
	}

new_packet:
	dp=pdata;
	k.ker_check = 0;
	if ((ch=ker_getc())==k.ker_pstart)
		goto new_packet;

	i=unchar(ch - 3);
	/* i �r lika med l�ngden av datadelen i paketet */
	if ((i<0)||(i>94)) 
		return ker_error("PACKETLEN");

	*dlen = i;
	if ((ch=ker_getc())==k.ker_pstart)
		goto new_packet;

	*pseq = unchar(ch);
	if ((ch=ker_getc())==k.ker_pstart)
		goto new_packet;

	k._rtype=ch;//info f�r redraw
	*ptype = ch;

	for (j=0;j<i;j++) 
	{
		if ((ch=ker_getc())==k.ker_pstart)
			goto new_packet;
		if (pdata!=NULL)
			*dp++=ch;
	}

	j = tochar((k.ker_check+((k.ker_check&192)/64))&63);

	if ((ch=ker_getc())==k.ker_pstart)
		goto new_packet;

	if(j!= ch) 
		return ker_error("CHKSUM");

	return 0;
}

/* Funktion f�r att skicka ett KERMIT-paket */
int CKermit::ker_tpack(int len, int seq, int type, unsigned char *buf)
{
	if((len!=0)&&(buf==NULL)) 
	{
		return -1;
	}

	k._tlen = len;
	k._tseq = seq;
	k._ttype = type;
	k._tbuf = buf;

	return ker_resend();
}

/////////////////////////////////////////////////////////////////////////////////
//@b: binary

int CKermit::start(char *szFilename)
{
	int ch, nRet;

	m_bKermitRunning = TRUE;
	error=retries=rseq=type=seq=len=0;
	filelength=bytes=0l;

	ch=getcommand2();

	switch(ch)
	{
	case 'G':
		nRet = sendfiledata(szFilename);
		break;

//	case 'F':
//		getfiledata((char *)rbuff+5);
//		break;

	default:
		break;
	}

	return nRet;
}

int CKermit::getcommand2()
{
	int ch;
	while(1)
	{
		if(m_bKermitRunning == FALSE)
			return 0;

		if(readpacket2()=='L')
		{
			for(int i=0;i<3;i++)
			{
				spack('A',0,0,NULL);
				if((ch=readpacket2())>0)
				{
					seq=rseq;
					return ch;
				}
				else
					retries++;
			}
		}

	}
}

int CKermit::readpacket2()
{
	int i,ch,timer=TIMEOUT;
	while((ch=ker_getc()) != SOH)
	{
		if(ch == -1)
		{
			if(!timer--)
			{
				return 0;
			}
		}
	}

	rbuff[0]=ch;

	if((ch=ker_getc()) == -1)
	{
		return 0;
	}

	rbuff[1]=type=ch;
	if((ch=ker_getc()) == -1)
	{
		return 0;
	}
	rbuff[2]=rseq=ch;
	if((ch=ker_getc()) == -1)
	{
		return 0;
	}

	rbuff[3]=ch;
	len=(ch<<8)&0xFF00;
	if((ch=ker_getc()) == -1)
	{
		return 0;
	}
	rbuff[4]=ch;
	len|=(ch&0xff);

	if(len>MAXLEN)
		return 0;


	for(i=0;i<len;i++)
		if((ch=ker_getc()) == -1)
		{
			return 0;
		}
		else
			rbuff[5+i]=ch;

	i=5+len;

	if((ch=ker_getc()) == -1)
	{
		return 0;
	}
	rbuff[i++]=ch;

	if(checksum((char *)rbuff,i))
		return 0;

	if((ch=ker_getc()) == -1)
	{
		return 0;
	}

	if(ch!=ETB)
		return 0;

	return type;
};

int CKermit::sendfiledata(char *FileName)
{
	int i,n,ch;
	char s[MAXFILE];

	strcpy(s, FileName);

	FILE *handle;
	if((handle = fopen( FileName, "rb" )) == NULL)
	{
		return -1;
	}

    if( fseek(handle, 0, SEEK_SET) != 0)
	{
		fclose(handle);
		return -1;
	}

	spos = 0;
	m_bKermitRunning = TRUE;

	for(i=0;i<3;i++)
	{
		spack('A',seq,strlen(s),(unsigned char*)&s);
		ch=readpacket2();
		if(ch=='A'&&rseq==seq)
		{
			i=10;
			seq=(seq+1)&0xFF;
		}
	}

	if(i<10)
	{
		fclose(handle);
		return 0;
	}


	n = fread(tbuff, sizeof( char ), MAXLEN, handle);
	while(n)
	{
		for(i=0;i<3;i++)
		{
			spack('D',seq,n,tbuff);
			if(m_bKermitRunning == FALSE)
			{
				i = 3;
				break;
			}

			if((ch=readpacket2())=='A'&&seq==rseq)
			{
				bytes+=n;
				n = fread(tbuff, sizeof( char ), MAXLEN, handle);
				seq=(seq+1)&0xff;
				i=10;
			}
			else
			{
				retries++;
			}
		}

		if(i==3)
		{
			fclose(handle);
			return 0;
		}
	}

	spack('Z',seq,0,NULL);
	fclose(handle);

	return 1;
}

int CKermit::checksum(char *ch,int len)
{
	int sum=0;
	for(int i=0;i<len;i++)
		sum+=ch[i];
	return sum&0xFF;
}

//int CKermit::ker_tpack(int len, int seq, int type, unsigned char *buf)
void CKermit::spack(char type,int seq,int len,unsigned char *tbuff)
{
	char startbuf[5];
	int sum=0;

	startbuf[0]=SOH;
	sum+=startbuf[0];

	startbuf[1]=type;
	sum+=startbuf[1];

	startbuf[2]=seq;
	sum+=startbuf[2];

	startbuf[3]=(len>>8)&0xff;
	sum+=startbuf[3];

	startbuf[4]=(len&0xff);
	sum+=startbuf[4];

	for(int i=0;i<len;i++)
		sum+=tbuff[i];

	if(m_bKermitRunning != FALSE)
	{
		SendData(startbuf,5);
		SendData((const char*)tbuff,len);
		ker_putc((-sum) & 0xff);
		ker_putc(ETB);

		if(m_hWnd != NULL)
		{
			PostMessage(m_hWnd, WM_COM_PACKET, spos, 0);
			spos+=len;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////


int CKermit::ker_resend()
{
	char szBuf[1024];
	int i=0, j;

	for(i=0;i<k.ker_npad;i++)
		szBuf[i] = k.ker_padc;
	szBuf[i++] = k.ker_pstart;	

	k.ker_check = 0;
	szBuf[i++] = tochar(k._tlen+3); k.ker_check += szBuf[i-1];
	szBuf[i++] = tochar(k._tseq); k.ker_check += szBuf[i-1];
	szBuf[i++] = k._ttype; k.ker_check += szBuf[i-1];
	for (j=0; j<k._tlen; j++)
	{
		szBuf[i++] = k._tbuf[j];
		k.ker_check += szBuf[i-1];
	}

	j = tochar((k.ker_check+((k.ker_check&192)/64))& 63);
	szBuf[i++] = j;
	szBuf[i++] = k.ker_eol;
	szBuf[i++] = '\0';

	SendData(szBuf, i-1);

	return 0;
}


/* Fyll 'ker_tbuf' med packat data, 'ker_tblen' f�r l�ngden av den
f�rdiga bufferten. Return-v�rde �r antalet byte som �r plockade
fr�n 'src'. 'len' anger hur m�nga bytar som finns kvar. */	    
int CKermit::ker_pack_data(unsigned char *src, int len)
{
	int i, j;
	int cnt, repcnt, pcnt;
	
	k.ker_tblen = 0;
	for (cnt=0;cnt<len;) 
	{
		pcnt = cnt;
		i=*src++;
		cnt++;
		j=k.ker_tblen;
		if (((cnt + 2) <= len) && (k.ker_rept != 0)) 
		{
			if ((i == src[0]) && (i == src[1])) 
			{	/* Repeat! */
				for (repcnt = 1; (repcnt < 94) && (cnt < len) && (i == *src); repcnt++) 
				{
					src++;
					cnt++;
				}
				k.ker_tbuf[j++]=k.ker_rept;
				k.ker_tbuf[j++]=tochar(repcnt);
			}
		}
		if ((i & 0x80) && (k.ker_qbin != 0)) 
        {
			k.ker_tbuf[j++]=k.ker_qbin;
			i=i&0x7f;
		}
		if (((i&0x7f)<' ')||((i&0x7f)==127)) 
        {
			k.ker_tbuf[j++]=k.ker_tx_qctl;
			i=ctl(i);
		}
		else if ((i==k.ker_qbin)||(i==k.ker_tx_qctl)||(i==k.ker_rept)) 
        {
			k.ker_tbuf[j++]=k.ker_tx_qctl;
		}
		k.ker_tbuf[j++]=i;
		if ((j + 3)>k.ker_maxpacket)
			return pcnt; 
		k.ker_tblen = j;
	}
	return cnt;
}



/* Tag hand om datat i 'ker_rbuf', 'size' anger hur m�nga tecken som finns
i 'ker_rbuf'. 'dst' �r plats att skriva avkodat data i, 'max_size' anger
hur stor plats som finns kvar. Returnv�rde �r antalet bytar som �r lagda
i 'dst'. */
int CKermit::ker_unpack_data(unsigned char *dst, int max_size, int size)
{
	int i, j;
	int cnt, repcnt, ch;
	cnt = 0;
	for (i = 0; i < size;)
	{
		repcnt=1;
		ch=0;
		j=k.ker_rbuf[i++];
		if((k.ker_rept!=0)&&(j==k.ker_rept))
		{
			repcnt=unchar(k.ker_rbuf[i++]);
			j=k.ker_rbuf[i++];
		}
		if((k.ker_qbin!=0)&&(j==k.ker_qbin)) 
		{
			ch|=0x80;
			j=k.ker_rbuf[i++];			
		}
		if(j==k.ker_rx_qctl) 
		{
			ch|=k.ker_rbuf[i++];
			j=ctl(ch)&0x7f;
			if ((j<' ')||(j==127))
				ch = ctl(ch);
		}
		else
			ch|=j;
		if(repcnt>max_size)
			return -1;
		if (repcnt != 1)
			setmem(dst, repcnt, ch);
		else
			*dst=ch;

		dst = dst+repcnt;
		max_size -= repcnt;
		cnt += repcnt;
    }
	return cnt;	
}


int CKermit::spar()
{
	int i;
	k.ker_tbuf[0] = tochar(k.ker_maxpacket);		/* MAXL */
	k.ker_tbuf[1] = tochar(5);			/* TIME */
	k.ker_tbuf[2] = tochar(0);			/* NPAD */
	k.ker_tbuf[3] = ctl(00);				/* PADC */
	k.ker_tbuf[4] = tochar(k.ker_eol);			/* EOL */
	k.ker_tbuf[5] = k.ker_tx_qctl;			/* QCTL */
	i=k.ker_qbin;
	if (((i >= 33) && (i <= 62)) || ((i >= 96) && (i <= 126)))
		k.ker_tbuf[6]=k.ker_qbin;			/* QBIN */
	else
		k.ker_tbuf[6]='Y';			        /* QBIN */
	k.ker_tbuf[7]='1';				/* CHKT */
	if (k.ker_rept!= 0)
		k.ker_tbuf[8]=k.ker_my_rept;		        /* REPT */
	else
		k.ker_tbuf[8] = ' ';
	return 0;
}


int CKermit::ker_send_init(int type)
{
	int i;
	int dlen, rseq, rtype;
	dlen=0;
	k.ker_tbuf[0]=tochar(k.ker_maxpacket);	/* MAXL */
	k.ker_tbuf[1]=tochar(5);				/* TIME */
	k.ker_tbuf[2]=tochar(0);				/* NPAD */
	k.ker_tbuf[3]=ctl(00);					/* PADC */
	k.ker_tbuf[4]=tochar(k.ker_eol);		/* EOL */
	k.ker_tbuf[5]=k.ker_tx_qctl;			/* QCTL */
	if (k.ker_qbflg)
		k.ker_tbuf[6]=k.ker_qbin;			/* QBIN */
	else
		k.ker_tbuf[6]='Y';					/* QBIN */
	k.ker_tbuf[7]= '1';						/* CHKT */
	k.ker_tbuf[8]=k.ker_my_rept;			/* REPT */
	
	ker_tpack(9, k.ker_seq, type, k.ker_tbuf);
	for (i=0; i < k.ker_retry; ker_resend(),i++) 
	{
		if(ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
			continue;                              /* Tj3000 nedan */
		if ((rseq == k.ker_seq) && ((rtype == KER_ACK)||(rtype == KER_INIT)||(rtype == KER_SEND_INIT) ))
			goto good_packet;
		if (rtype == KER_ERROR)
			goto error_packet;
	}
	return -1;

error_packet:
	movmem(k.ker_rbuf,(unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;
good_packet:
	ker_prcv_param(dlen);
	k.ker_seq = inc(k.ker_seq);
	return 0;
}

int CKermit::ker_send_rcv_init(char *fn,int command)
{
	int len, i;
	int dlen, rseq, rtype;
	if((len=ker_pack_data((unsigned char *)fn, strlen(fn))) < 0)
		return -1;
	if(len!=strlen(fn))
		return -1;
	k.ker_seq=dec(k.ker_seq);
	if(command==0)
		ker_tpack(k.ker_tblen,k.ker_seq,KER_SRV_RECV, k.ker_tbuf);
	else
		ker_tpack(k.ker_tblen,k.ker_seq,'G',k.ker_tbuf);
	for (i=0;i<k.ker_retry;ker_resend(),i++) 
	{
		if (ker_rpack(&dlen,&rseq,&rtype,k.ker_rbuf))
			continue;
		if ((rseq==k.ker_seq)&&(rtype ==KER_SEND_INIT||rtype==KER_ACK))
			goto good_packet;
		if (rtype == KER_ERROR)
			goto error_packet;
	}
	return -1;
error_packet:
	movmem(k.ker_rbuf, (unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen]=0;
	return -1;
good_packet:
	if(rtype==KER_ACK)
		return -2;
	
	ker_prcv_param(dlen);		      
	k.ker_tbuf[0] =tochar(k.ker_maxpacket);		/* MAXL */
	k.ker_tbuf[1]=tochar(5);			/* TIME */
	k.ker_tbuf[2]=tochar(0);			/* NPAD */
	k.ker_tbuf[3]=ctl(00);				/* PADC */
	k.ker_tbuf[4]=tochar(k.ker_eol);			/* EOL */
	k.ker_tbuf[5]=k.ker_tx_qctl;			/* QCTL */
	i=k.ker_qbin;
	if(((i>= 33)&&(i<=62))||((i>=96)&&(i <= 126)))
		k.ker_tbuf[6]=k.ker_qbin;			/* QBIN */
	else
		k.ker_tbuf[6]='Y';			/* QBIN */
	k.ker_tbuf[7] ='1';				/* CHKT */
	if(k.ker_rept!= 0)
		k.ker_tbuf[8]=k.ker_my_rept;		/* REPT */
    else
		k.ker_tbuf[8] = ' ';
	ker_tpack(9, k.ker_seq, KER_ACK,k.ker_tbuf);	
	
	return 0;
}


int CKermit::ker_prcv_param(int dlen)
{
	int i;
	if (dlen >= 1) 
	{
		if((i=unchar(k.ker_rbuf[0]))<k.ker_maxpacket)
			k.ker_maxpacket=i;
		if(i<10)
			k.ker_maxpacket=80;
	}
	else
		k.ker_maxpacket=80;
	
	if(dlen >= 2)
		k.ker_timeout=unchar(k.ker_rbuf[1]);
	if(k.ker_timeout < 5)
		k.ker_timeout = 5;
	if (dlen>=3)
		k.ker_npad=unchar(k.ker_rbuf[2]);
	if(dlen>=4)
		k.ker_padc=ctl(k.ker_rbuf[3]);
	if(dlen>=5)
		k.ker_eol=unchar(k.ker_rbuf[4]);
	if (dlen >= 6)
		k.ker_rx_qctl=k.ker_rbuf[5];
	if (dlen>=7) 
	{
		i=k.ker_rbuf[6];
		if(((i >= 33) && (i <= 62)) ||((i >= 96) && (i <= 126)))
			k.ker_qbin = i;
	}
	
	/* ker_rbuf[7], CHKT ignoreras, vi anv�nder ALLTID type 1. */
	if (dlen >= 9) 
	{
		i=k.ker_rbuf[8];
		if (i==k.ker_my_rept)
			k.ker_rept = i;
	}
	
	return 0;	
	/* Resten av paketet (CAPAS) ignoreras */
}


int CKermit::ker_send_file_data(unsigned char *area, long int size)
{
	int i;
	int spos;
	int thi, left;
	int dlen, rseq, rtype;
	for(spos = 0; spos < size;) 
	{
		// Progressbar
		if(m_hWnd != NULL)
			PostMessage(m_hWnd, WM_COM_PACKET, spos, 0);
		
		if ((size - spos) > 8192)
			left = 8192;
		else
			left = (int)(size - spos);
		if ((thi = ker_pack_data(area+spos, left)) < 0)
			return ker_error("PACKDATA"); 
		
		ker_tpack(k.ker_tblen, k.ker_seq, KER_DATA, k.ker_tbuf);
		
		for (i = 0; i <k.ker_retry; ker_resend(), i++) 
		{
			if (ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
				continue;
			if ((rseq ==k.ker_seq) && (rtype == KER_ACK))
				goto good_packet;
			if (rtype == KER_ERROR)
				goto error_packet;

			if(m_bKermitRunning == FALSE)
			{
				sprintf(k.ker_last_error,"QUIT");
				break;
			}
		}
		return -1;
		
good_packet:
		spos += thi;
		k.ker_bytes=spos;
		k.ker_seq = inc(k.ker_seq);
	}
	
	if(spos!= size)
		return -1;

	ker_tpack(0,k.ker_seq, KER_EOF,k.ker_tbuf);

    for (i = 0; i <k.ker_retry; ker_resend(), i++) 
	{
		if (ker_rpack(&dlen, &rseq, &rtype,k.ker_rbuf))
			continue;
		if ((rseq ==k.ker_seq) && (rtype == KER_ACK))
			goto good_packet_EOF;
		if (rtype == KER_ERROR)
			goto error_packet;
	}
	return -1;

error_packet:
	movmem(k.ker_rbuf,(unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;

good_packet_EOF:
	k.ker_seq = inc(k.ker_seq);
	return 0;
}



int CKermit::ker_get_file_data(unsigned char *area,int size,int command)
{
	int i, j;
	int dlen, rseq, rtype;
	int spos;
	bool bInv = FALSE;
	CString csNewName;
	
	for (i = 0; i <k.ker_retry; ker_resend(), i++) 
	{
		if (ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
			continue;

		if(command==0)
		{
			if((rseq == inc(k.ker_seq)) && (rtype == KER_FILE))
				goto good_packet;
		}
		else if(command==1)
		{
			if ((rseq == inc(k.ker_seq)) && (rtype == 'X'/* KER_FILE*/))
				goto good_packet;
		}

		if (rtype == KER_ERROR)
		{
			goto error_packet;
		}
		if (rtype == KER_EOT)
			goto end_tx;

		if(m_bKermitRunning == FALSE)
		{
			sprintf(k.ker_last_error,"QUIT");
			goto error_packet;
		}
	}
	return -1;

error_packet:
	movmem(k.ker_rbuf, (unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;

good_packet:
	k.ker_seq = rseq;
	i=ker_unpack_data(ker_filename, 127, dlen);
	ker_filename[i]=0;
	strncpy(k.ker_last_error, (char *)ker_filename, 127);
	spos=0;

// �ppna/skapa filen
	FILE* stream;
	char szBuf[2048];
	sprintf(szBuf, "%S%s", ker_filepath, ker_filename);

	// filen finns redan?
save_file:
	if( (stream = fopen(szBuf, "r")) != NULL )
	{
		fclose(stream);
		int nRet = ::MessageBox(m_hWnd, g_pXML->str(1114), g_pXML->str(1101), MB_YESNOCANCEL|MB_ICONQUESTION);

		if(nRet == IDNO)
		{
			if(strstr(szBuf, ".INV") != NULL)	// INV-file
			{
				// show the special "INV-rename-dialog"
				CInvRenamer dlg;
				dlg.m_csOrigName = ker_filename;
				int nRet = dlg.DoModal();

				if(nRet == IDOK)
				{
					CString csBuf;
					bInv = TRUE;
					char *pPos=0;

					if( (pPos = strchr((char*)&ker_filename, '#')) != NULL)
					{
						csBuf = dlg.m_csSubNew;

						pPos++;
						*pPos = '\0';
						dlg.m_csNewName.Format(_T("%s%s"), ker_filename, csBuf);
					}

					csNewName = dlg.m_csNewName;
					csBuf = dlg.m_csNewName;
					csBuf.MakeUpper();
					if(csBuf.Find(_T(".INV"), 0) == -1) dlg.m_csNewName += _T(".INV");

					dlg.m_csNewName.Replace(_T(":"), _T(";"));
					dlg.m_csNewName.Replace(_T("\\"), _T("-"));



					sprintf((char*)&ker_filename, "%S", dlg.m_csNewName);
					sprintf(szBuf, "%S%s", ker_filepath, ker_filename);

					goto save_file;
				}
				else
				{
					return -1;
				}
			}
			else	// not an INV-file
			{
				// open a filesavedialog to choose other filename
				CString csBuf;
				OPENFILENAME ofn;
				int nPos;
				memset((void *) &ofn, 0, sizeof(OPENFILENAME));

				ofn.lStructSize = sizeof(OPENFILENAME);
				ofn.nMaxFile = _MAX_PATH;
				ofn.nMaxFileTitle = _MAX_FNAME + _MAX_EXT;
				ofn.hwndOwner = this->m_hWnd;

				CHAR* pDest = strrchr( szBuf, '.' );
				pDest++;

				TCHAR tzBuf[MAX_PATH], tzBuf2[5];
				_stprintf(tzBuf, _T("%S"), szBuf);
				ofn.lpstrFile = tzBuf;
				_stprintf(tzBuf2, _T("%S"), pDest);
				ofn.lpstrDefExt = tzBuf2;
				ofn.Flags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;

				if(!GetSaveFileName(&ofn)) return -1;

				csBuf = ofn.lpstrFile;
				sprintf(szBuf, "%S", ofn.lpstrFile);

				nPos = csBuf.ReverseFind('\\');
				sprintf( (char*)&ker_filename, "%S", csBuf.Right(csBuf.GetLength()-nPos-1) );
				ker_filepath = csBuf.Left(nPos+1);
			}
		}
		else if(nRet == IDCANCEL)
		{
			// just return
			return -1;
		}
	}


	if( (stream = fopen(szBuf, "wb" )) == NULL )
	{
		AfxMessageBox(g_pXML->str(1118));
		return -1;
	}

	if(m_hWnd != NULL)
		PostMessage(m_hWnd, WM_COM_FILE, 0, (LPARAM)&ker_filename);

	while(1)
	{
		ker_tpack(0,k.ker_seq, KER_ACK, NULL);
		for (i = 0; i <k.ker_retry; ker_resend(), i++) 
		{
			if (ker_rpack(&dlen, &rseq, &rtype,k.ker_rbuf))
				continue;
			if ((rseq == inc(k.ker_seq)) && (rtype == KER_DATA))
				goto good_data_packet;
			if ((rseq == inc(k.ker_seq)) && (rtype == KER_EOF))
				goto good_eof_packet;
			if (rtype == KER_ERROR)
				goto error_packet;
			if (rtype == KER_EOT)
				goto end_tx;

			if(m_bKermitRunning == FALSE)
			{
				sprintf(k.ker_last_error,"QUIT");
				goto shut_down;
			}
		}
		fclose(stream);
		return -1;

		good_data_packet:
		k.ker_seq = rseq;
		i = size;

		j=ker_unpack_data(area, size, dlen);
		if (j < 0)
			goto shut_down;
		spos += j;

		// loopa in datat i filen
		for(int nLoop=0; nLoop<j; nLoop++)
		{
			fputc(area[nLoop], stream);
		}

		// Progressbar
		if(m_hWnd != NULL)
			PostMessage(m_hWnd, WM_COM_PACKET, spos, 0);
	}

good_eof_packet:
	fclose(stream);
	if(bInv == TRUE) RenameINV(CString(szBuf), csNewName);
	k.ker_seq = rseq;
	ker_tpack(0,k.ker_seq,KER_ACK, NULL);
	return spos;
end_tx:
	fclose(stream);
	if(bInv == TRUE) RenameINV(CString(szBuf), csNewName);
	ker_tpack(0, k.ker_seq, KER_ACK, NULL);
	return 0L;
shut_down:
	fclose(stream);
	ker_tpack(11,k.ker_seq, KER_ERROR, (unsigned char*)"MEMORY FULL");
	return -1L;
}

int CKermit::ker_send_break()
{
	int i;
	int dlen, rseq, rtype;
	ker_tpack(0,k.ker_seq, KER_EOT, NULL);
	for (i = 0; i <k.ker_retry; ker_resend(), i++) 
	{
		if (ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
			continue;
		if ((rseq == k.ker_seq) && (rtype == KER_ACK))
			goto good_packet;
		if (rtype == KER_ERROR)
			goto error_packet;
    }
	return -1;
error_packet:
	movmem(k.ker_rbuf,(unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;
good_packet:
	k.ker_seq = inc(k.ker_seq);	      
	return 0;
}

int CKermit::ker_get_break()
{
	int  i;
	int dlen, rseq, rtype;
	
	for (i = 0; i < k.ker_retry; ker_resend(), i++) 
	{
		if (ker_rpack(&dlen, &rseq, &rtype, k.ker_rbuf))
			continue;
		if ((rseq == inc(k.ker_seq))/* && (rtype == KER_EOT)*/)
			goto good_packet;
		if (rtype == KER_ERROR)
			goto error_packet;
	}
	return -1;
error_packet:
	movmem(k.ker_rbuf,(unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;
good_packet:
	k.ker_seq = rseq;
	if(rtype==KER_EOT)
		ker_tpack(0, k.ker_seq,KER_ACK, NULL);
	else
		ker_tpack(0, k.ker_seq,KER_EOT, NULL);	/* break if more */
	return 0;
}

int CKermit::ker_send_file_header(int type,char *fn)
{
	int len, i;
	int dlen, rseq, rtype;
	if ((len = ker_pack_data((unsigned char *)fn, strlen(fn))) < 0)
		return -1;
	if (len != strlen(fn))
		return -1;
	ker_tpack(k.ker_tblen,k.ker_seq, type,k.ker_tbuf);
	for (i = 0; i <k.ker_retry; ker_resend(), i++) 
    {
		if (ker_rpack(&dlen, &rseq, &rtype,k.ker_rbuf))
			continue;
		if ((rseq ==k.ker_seq) && (rtype == KER_ACK))
			goto good_packet;
		if (rtype == KER_ERROR)
			goto error_packet;
    }
	return -1;
error_packet:
	movmem(k.ker_rbuf,(unsigned char *)k.ker_last_error, dlen);
	k.ker_last_error[dlen] = 0;
	return -1;
good_packet:
	k.ker_seq = inc(k.ker_seq);	      
	return 0;
}

void CKermit::SystemIdle()
{
	MSG msg;
		
	while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}
