#pragma once


// CDigiExportDlg dialog

class CDigiExportDlg : public CDialog
{
	DECLARE_DYNAMIC(CDigiExportDlg)

	afx_msg void OnLbnSelchangeList1();
	CListBox m_cListBox;
	CString m_csName;
	int m_nPart;
	CComboBox m_cMethod;
	int m_nMethod;
	CString m_csRadius;
	float m_fRadius;
	CString m_csArea;
	float m_fArea;
	CString m_csInfo;

	int m_nPrevSel;

public:
	CDigiExportDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDigiExportDlg();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_DIGIEXPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditRadius();
	afx_msg void OnEnChangeEditArea();
	afx_msg void OnEnUpdateEditName();
	afx_msg void OnBnClickedOk();
};
