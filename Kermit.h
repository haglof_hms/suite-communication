// Serial.h

#ifndef __SERIAL_H__
#define __SERIAL_H__

#define FC_DTRDSR       0x01
#define FC_RTSCTS       0x02
#define FC_XONXOFF      0x04
#define ASCII_BEL       0x07
#define ASCII_BS        0x08
#define ASCII_LF        0x0A
#define ASCII_CR        0x0D
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13



class CKermit
{

public:
	CKermit();
	~CKermit();

	HWND m_hWnd;

	BOOL Open(int nComport, int nBaud);
	BOOL Close(void);
	BOOL HangUp(void);

	int ReadData(char *, int);
	int SendData(const char *, int);
	int ReadDataWaiting(void);
	int CheckReturnCode(int nSize);

	BOOL IsOpened(void ){ return( m_bOpened); }

	// KERMIT
	int ker_send_file(char *file_name,unsigned char *area,int size);
	int ker_get_file(char *file_name, unsigned char *area, int size,int command);
	void ker_cancel(void) {m_bKermitRunning = FALSE;}
	int ker_server(char *file_name,unsigned char *area,int size);
	int read_ker_char_(void);

protected:
	BOOL WriteCommByte(unsigned char);

public:
	HANDLE m_hIDComDev;
protected:
	OVERLAPPED m_OverlappedRead, m_OverlappedWrite;
	BOOL m_bOpened;

	// KERMIT
	BOOL m_bKermitRunning;

	void SystemIdle(void);

	void movmem(unsigned char *src,unsigned char *dest,unsigned len);
	void setmem(unsigned char *dest,unsigned len,unsigned char value);

	int ker_getcommand(int port);

	int ker_getc();
public:
	void ker_putc(int ch);
protected:

	void ker_stimeout();

	void ker_set_default();
	int ker_resend();
	int ker_rpack(int *dlen, int *pseq,int *ptype, unsigned char *pdata);
	int ker_tpack(int len,int seq,int type,unsigned char *buf);

	int spar();
	int ker_prcv_param(int dlen);
	int get_ker_file_(char *file_name, unsigned char *area, int size,int command);
	int ker_send_file_(char *file_name,unsigned char *area,int size);
	int ker_send_init(int type);
	int ker_error(char *s);
	int ker_send_rcv_init(char *fn,int command);
	int ker_get_file_data(unsigned char *area,int size,int command);
	int ker_send_break();
	int ker_get_break();
	int ker_send_file_header(int type,char *fn);
	int ker_send_file_data(unsigned char *area, long int size);
	int getcommand(char *file_name,unsigned char *area,int size,int timeout);
	int ker_unpack_data(unsigned char *dst,int max_size,int size);
	int ker_pack_data(unsigned char *src, int len);
	
	int _server(char *file_name,unsigned char *area,int size,int timeout);
	
	struct Swi_Ker_Command_type
	{
		char *file_name;
		unsigned char *area;
		int size;
	};

	unsigned char ker_filename[128];

public:
	CString ker_filepath;

	struct ker_data_type
	{
		// struct disp_bar_type p;
		char ker_type;
		int sectim;//timeout
		int ker_port;
		int ker_check;
		int mcheck;
		int ker_qbflg;
		char ker_last_error[128];
		int ker_seq;
		char ker_pstart;
		char ker_eol;
		int ker_timeout;
		int ker_maxpacket;
		int ker_npad;
		int ker_padc;
		int ker_rx_qctl;
		int ker_tx_qctl;
		int ker_qbin;
		int ker_rept;
		int ker_retry;
		int ker_my_rept;
		int ker_file_len;
		int ker_bytes;
		unsigned char ker_tbuf[128];
		unsigned char ker_rbuf[128];
		int _tlen;
		int _tseq;
		int _ttype;
		int _rtype;//info vid redraw
		unsigned char *_tbuf;
		int ker_tblen;
		int ker_retry_counter;
	}
	k;

// lots of extras due to mantax binary format
#define MAXFILE 255
#define TIMEOUT 5
#define MAXLEN 1024
#define SOH 1
#define ETB 23
#define MAXDRIVE 255
#define MAXDIR 255
#define MAXFILE 255
#define MAXEXT 255

	int start(char *szFilename);
	int readpacket2();
	int getcommand2();
	int sendfiledata(char *FileName);
	int checksum(char *ch,int len);
	void spack(char type,int seq,int len,unsigned char *tbuff);

	int bytes, error, filelength, len, retries;
	int rseq, seq, type;
	int spos;

	unsigned char tbuff[MAXLEN+10];
	unsigned char rbuff[MAXLEN+10];
};

#endif
