#include "stdafx.h"
#include "DP_Code.h"

#define POLY 0x01021 /* CCITT: G-Polynom $11021 16-12-5-0 */

unsigned short crc16_typ2(unsigned char *z,int n)
{
	int i;
	unsigned short zlr;
	static unsigned int crc, g;
	crc=POLY;
	
	g = (POLY << 8) & 0x00ffff00;

	for(i=0; i<n; i++)
	{
		crc |= ((long) (*z++) & 0x000000ff);

		if(i == n-2) i = n-2;

		for(zlr=0; zlr<8; zlr++)
		{
			crc <<= 1;
			if ((crc & 0x01000000) != 0) crc ^= g;
		}
	}
	
	crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
	
	return crc;
}

unsigned short crc16_typ1(unsigned char *z,int n)
{
	int i;
	unsigned short zlr;
	static unsigned int crc, g;
	crc=0;
	
	g = (POLY << 8) & 0x00ffff00;

	for(i=0; i<n; i++)
	{
		crc |= ((long) (*z++) & 0x000000ff);

		if(i == n-2) i = n-2;

		for(zlr=0; zlr<8; zlr++)
		{
			crc <<= 1;
			if ((crc & 0x01000000) != 0) crc ^= g;
		}
	}
	
	crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
	
	return crc;
}

BOOL CheckCode(int nProdID, int nSerial, int nLevel, int nCode)
{
	static int a, b;
	unsigned char p[8];

	b = nProdID + nLevel;
	a = nSerial;
	memcpy(&p,(void*)&a, 4);
	memcpy(p+4,(char *)&b, 4);
	int nTmp = crc16_typ1(p, 8);
	
	// checka om nyckeln �r densamma som anv�ndaren matat in
	if(nTmp == nCode)
	{
		nTmp = 1234567890;
		return TRUE;
	}
	else
	{
		nTmp = 1234567890;
		return FALSE;
	}
}
