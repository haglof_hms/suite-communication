// PrgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "PrgDlg.h"


// CPrgDlg dialog

IMPLEMENT_DYNAMIC(CPrgDlg, CDialog)
CPrgDlg::CPrgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrgDlg::IDD, pParent)
{
}

CPrgDlg::~CPrgDlg()
{
}

void CPrgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPrgDlg, CDialog)
END_MESSAGE_MAP()


// CPrgDlg message handlers
