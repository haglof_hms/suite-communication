// SampleSuite.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "SettingsDPDlg.h"
#include "SettingsMCDlg.h"
#include "SettingsDDlg.h"
#include "SettingsDlg.h"
#include "ProgramsDlg.h"
#include "LicensesDlg.h"
#include "SenddataDlg.h"
#include "RecvdataDlg.h"
#include "ASCIIDlg.h"
#include "RecvdataDDlg.h"
#include "Usefull.h"

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines
#include <shlobj.h>

std::vector<HINSTANCE> m_vecHInstTable;
RLFReader* g_pXML;
CZipArchive* g_pZIP;
static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE SampleSuiteDLL = { NULL, NULL };

CSampleSuite::CSampleSuite()
{
	static const char* name = "SHGetFolderPathW"; 

	m_hDll = LoadLibrary(_T("shell32.dll")); 
	if (m_hDll) 
	{ 
		m_pSHGetFolderPath = (HRESULT (__stdcall*)(HWND hwndOwner, int nFolder, HANDLE hToken, DWORD dwFlags, LPTSTR pszPath))GetProcAddress(m_hDll, name);
		if (!m_pSHGetFolderPath) 
		{ 
			FreeLibrary(m_hDll); 
			m_hDll = 0; 
		} 
	} 
	if (!m_pSHGetFolderPath) 
	{ 
		m_hDll = LoadLibrary(_T("SHFolder.dll")); 
		if (m_hDll) 
		{ 
			m_pSHGetFolderPath = (HRESULT (__stdcall*)(HWND hwndOwner, int nFolder, HANDLE hToken, DWORD dwFlags, LPTSTR pszPath))GetProcAddress(m_hDll, name);
			if (!m_pSHGetFolderPath)
			{ 
				FreeLibrary(m_hDll); 
				m_hDll = 0; 
			} 
		}
	} 

	TCHAR szPath[MAX_PATH];
	CString csBuf;

	// Program
	if(SUCCEEDED(m_pSHGetFolderPath(NULL, 
		CSIDL_COMMON_APPDATA,	// C:\Documents and Settings\All Users\Application Data
		NULL, 
		0, 
		szPath))) 
	{
		csBuf.Format(_T("%s\\HMS"), szPath);
		CreatePublicDirectory(csBuf);	// make sure that <COMMON_APPDATA>\HMS gets created

		m_csFilepath.Format(_T("%s\\HMS\\Programs\\"), szPath);
		CreatePublicDirectory(m_csFilepath);	// make sure that <COMMON_APPDATA>\HMS gets created
	}

	// Data
	if(SUCCEEDED(m_pSHGetFolderPath(NULL, 
		CSIDL_PERSONAL,		// My Documents
		NULL, 
		0, 
		szPath))) 
	{
		csBuf.Format(_T("%s\\HMS"), szPath);
		CreateDirectory(csBuf);	// make sure that <My documents>\HMS gets created

		m_csDownloadpath.Format(_T("%s\\HMS\\Data\\"), szPath);
		CreateDirectory(m_csDownloadpath);	// make sure that <My documents>\HMS gets created
	}

	GetTempPath(MAX_PATH, szPath);
	m_csTemppath = szPath;


	// default
//	m_csWebpath = _T("/DP/");
//	m_csWebserver = _T("www.haglofmanagementsystems.com");
	m_bLocal = 0;
	m_csLocalpath = _T("C:\\");

	m_csDP_Baudrate = _T("115200");
	m_csDP_Downloadpath = m_csDownloadpath; // + _T("DigitechPro");
	m_csDP_Filepath = m_csFilepath + _T("DigitechPro");

	m_csMC_Baudrate = _T("19200");
	m_csMC_Comport = _T("COM1:");
	m_csMC_Downloadpath = m_csDownloadpath; // + _T("Mantax computer");
	m_csMC_Filepath = m_csFilepath + _T("Mantax computer");

	m_csD_Baudrate = _T("9600");
	m_csD_Comport = _T("COM1:");
	m_bD_Del0 = 1;
	m_bD_Height8 = 1;
	m_bD_Two0 = 1;
	m_bD_Three0 = 1;
	m_csD_Downloadpath = m_csDownloadpath; // + _T("Digitech");
	m_csD_Filepath = m_csFilepath + _T("Digitech");
	m_nD_OutFormat = 0;	// XML
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));
	m_caD_Species.Add(_T(""));

	m_csWebpath = regGetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webpath"), _T(""));
	if(m_csWebpath == _T(""))
		m_csWebpath = regGetStr_LM(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webpath"), _T("/DP/"));

	m_csWebserver = regGetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webserver"), _T(""));
	if(m_csWebserver == _T(""))
		m_csWebserver = regGetStr_LM(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webserver"), _T("www.haglofmanagementsystems.com"));

	m_bLocal = regGetInt(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Local"), m_bLocal);
	m_csLocalpath = regGetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Localpath"), m_csLocalpath);

	// DigitechPro
	m_csDP_Baudrate = regGetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Baudrate"), m_csDP_Baudrate);
	m_csDP_Comport = regGetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Comport"), _T(""));
	GetComPorts();
	if(m_csDP_Comport == _T(""))	// blank registry setting, see if we have a DP connected
	{
		PORTS port;
		for(int nLoop=0; nLoop<m_caComports.GetSize(); nLoop++)
		{
			port = m_caComports.GetAt(nLoop);
			if(port.nDP == 1)	// Digitech Pro?
			{
				m_csDP_Comport = port.csPort;
				break;
			}
		}
	}

	m_csDP_Downloadpath = regGetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Downloadpath"), m_csDP_Downloadpath);
	m_csDP_Filepath = regGetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Filepath"), m_csDP_Filepath);
	m_csDP_Langs = regGetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Languages"), m_csDP_Langs);

	// Mantax computer
	m_csMC_Baudrate = regGetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Baudrate"), m_csMC_Baudrate);
	m_csMC_Comport = regGetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Comport"), m_csMC_Comport);
	m_csMC_Downloadpath = regGetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Downloadpath"), m_csMC_Downloadpath);
	m_csMC_Filepath = regGetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Filepath"), m_csMC_Filepath);

	// Digitech
	m_csD_Baudrate = regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Baudrate"), m_csD_Baudrate);
	m_csD_Comport = regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Comport"), m_csD_Comport);
	m_bD_Del0 = regGetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("\\Del0"), m_bD_Del0);
	m_bD_Height8 = regGetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Height8"), m_bD_Height8);
	m_bD_Two0 = regGetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Two0"), m_bD_Two0);
	m_bD_Three0 = regGetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Three0"), m_bD_Three0);
	m_csD_Downloadpath = regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Downloadpath"), m_csD_Downloadpath);
	m_csD_Filepath = regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Filepath"), m_csD_Filepath);
	m_nD_OutFormat = regGetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("OutFormat"), m_nD_OutFormat);
	m_caD_Species.SetAt(0, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie1"), m_caD_Species.GetAt(0)));
	m_caD_Species.SetAt(1, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie2"), m_caD_Species.GetAt(1)));
	m_caD_Species.SetAt(2, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie3"), m_caD_Species.GetAt(2)));
	m_caD_Species.SetAt(3, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie4"), m_caD_Species.GetAt(3)));
	m_caD_Species.SetAt(4, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie5"), m_caD_Species.GetAt(4)));
	m_caD_Species.SetAt(5, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie6"), m_caD_Species.GetAt(5)));
	m_caD_Species.SetAt(6, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie7"), m_caD_Species.GetAt(6)));
	m_caD_Species.SetAt(7, regGetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie8"), m_caD_Species.GetAt(7)));

	if(m_csDP_Downloadpath != _T(""))
		CreateDirectory(m_csDP_Downloadpath);
	if(m_csDP_Filepath != _T(""))
		CreatePublicDirectory(m_csDP_Filepath);
	if(m_csMC_Downloadpath != _T(""))
		CreateDirectory(m_csMC_Downloadpath);
	if(m_csMC_Filepath != _T(""))
		CreatePublicDirectory(m_csMC_Filepath);
	if(m_csD_Downloadpath != _T(""))
		CreateDirectory(m_csD_Downloadpath);
	if(m_csD_Filepath != _T(""))
		CreatePublicDirectory(m_csD_Filepath);

	if(m_csDP_Downloadpath.GetAt(m_csDP_Downloadpath.GetLength()-1) != '\\') m_csDP_Downloadpath += _T("\\");
	if(m_csDP_Filepath.GetAt(m_csDP_Filepath.GetLength()-1) != '\\') m_csDP_Filepath += _T("\\");
	if(m_csMC_Downloadpath.GetAt(m_csMC_Downloadpath.GetLength()-1) != '\\') m_csMC_Downloadpath += _T("\\");
	if(m_csMC_Filepath.GetAt(m_csMC_Filepath.GetLength()-1) != '\\') m_csMC_Filepath += _T("\\");
	if(m_csD_Downloadpath.GetAt(m_csD_Downloadpath.GetLength()-1) != '\\') m_csD_Downloadpath += _T("\\");
	if(m_csD_Filepath.GetAt(m_csD_Filepath.GetLength()-1) != '\\') m_csD_Filepath += _T("\\");
}

CSampleSuite::~CSampleSuite()
{
	// save some settings in the registry
	SaveRegSettings();

	if (m_hDll) 
		FreeLibrary(m_hDll); 
}

void CSampleSuite::SaveRegSettings()
{
	// Settings
	regSetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webpath"), m_csWebpath);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Webserver"), m_csWebserver);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Localpath"), m_csLocalpath);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Settings"), _T("Local"), m_bLocal);

	// DigitechPro
	regSetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Baudrate"), m_csDP_Baudrate);
	regSetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Comport"), m_csDP_Comport);
	regSetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Downloadpath"), m_csDP_Downloadpath);
	regSetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Filepath"), m_csDP_Filepath);
	regSetStr(REG_ROOT, _T("HMS_Communication\\DigitechPro"), _T("Languages"), m_csDP_Langs);

	// Mantax computer
	regSetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Baudrate"), m_csMC_Baudrate);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Comport"), m_csMC_Comport);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Downloadpath"), m_csMC_Downloadpath);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Mantax computer"), _T("Filepath"), m_csMC_Filepath);

	// Digitech
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Baudrate"), m_csD_Baudrate);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Comport"), m_csD_Comport);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Del0"), m_bD_Del0);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Height8"), m_bD_Height8);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Two0"), m_bD_Two0);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Three0"), m_bD_Three0);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Downloadpath"), m_csD_Downloadpath);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Filepath"), m_csD_Filepath);
	regSetInt(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("OutFormat"), m_nD_OutFormat);
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie1"), m_caD_Species.GetAt(0));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie2"), m_caD_Species.GetAt(1));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie3"), m_caD_Species.GetAt(2));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie4"), m_caD_Species.GetAt(3));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie5"), m_caD_Species.GetAt(4));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie6"), m_caD_Species.GetAt(5));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie7"), m_caD_Species.GetAt(6));
	regSetStr(REG_ROOT, _T("HMS_Communication\\Digitech"), _T("Specie8"), m_caD_Species.GetAt(7));
}

CSampleSuite theApp;

#include <setupapi.h>

#ifndef GUID_DEVINTERFACE_COMPORT
  DEFINE_GUID(GUID_DEVINTERFACE_COMPORT, 0x86E0D1E0L, 0x8089, 0x11D0, 0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73);
#endif


typedef HKEY (__stdcall SETUPDIOPENDEVREGKEY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, DWORD, DWORD, REGSAM);
typedef BOOL (__stdcall SETUPDIDESTROYDEVICEINFOLIST)(HDEVINFO);
typedef BOOL (__stdcall SETUPDIENUMDEVICEINFO)(HDEVINFO, DWORD, PSP_DEVINFO_DATA);
typedef HDEVINFO (__stdcall SETUPDIGETCLASSDEVS)(LPGUID, LPCTSTR, HWND, DWORD);
typedef BOOL (__stdcall SETUPDIGETDEVICEREGISTRYPROPERTY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, PDWORD, PBYTE, DWORD, PDWORD);

// get the available comports
int CSampleSuite::GetComPorts()
{
	m_caComports.RemoveAll();

	//Get the various function pointers we require from setupapi.dll
	HINSTANCE hSetupAPI = LoadLibrary(_T("SETUPAPI.DLL"));
	if (hSetupAPI == NULL)
		return FALSE;

	SETUPDIOPENDEVREGKEY* lpfnLPSETUPDIOPENDEVREGKEY = reinterpret_cast<SETUPDIOPENDEVREGKEY*>(GetProcAddress(hSetupAPI, "SetupDiOpenDevRegKey"));
	SETUPDIGETCLASSDEVS* lpfnSETUPDIGETCLASSDEVS = reinterpret_cast<SETUPDIGETCLASSDEVS*>(GetProcAddress(hSetupAPI, "SetupDiGetClassDevsW"));
	SETUPDIGETDEVICEREGISTRYPROPERTY* lpfnSETUPDIGETDEVICEREGISTRYPROPERTY = reinterpret_cast<SETUPDIGETDEVICEREGISTRYPROPERTY*>(GetProcAddress(hSetupAPI, "SetupDiGetDeviceRegistryPropertyW"));
	SETUPDIDESTROYDEVICEINFOLIST* lpfnSETUPDIDESTROYDEVICEINFOLIST = reinterpret_cast<SETUPDIDESTROYDEVICEINFOLIST*>(GetProcAddress(hSetupAPI, "SetupDiDestroyDeviceInfoList"));
	SETUPDIENUMDEVICEINFO* lpfnSETUPDIENUMDEVICEINFO = reinterpret_cast<SETUPDIENUMDEVICEINFO*>(GetProcAddress(hSetupAPI, "SetupDiEnumDeviceInfo"));

	if ((lpfnLPSETUPDIOPENDEVREGKEY == NULL) || (lpfnSETUPDIDESTROYDEVICEINFOLIST == NULL) ||
		(lpfnSETUPDIENUMDEVICEINFO == NULL) || (lpfnSETUPDIGETCLASSDEVS == NULL) || (lpfnSETUPDIGETDEVICEREGISTRYPROPERTY == NULL))
	{
		//Unload the setup dll
		FreeLibrary(hSetupAPI);

		SetLastError(ERROR_CALL_NOT_IMPLEMENTED);

		return FALSE;
	}

	//Now create a "device information set" which is required to enumerate all the ports
	GUID guid = GUID_DEVINTERFACE_COMPORT;
	HDEVINFO hDevInfoSet = lpfnSETUPDIGETCLASSDEVS(&guid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDevInfoSet == INVALID_HANDLE_VALUE)
	{
		//Unload the setup dll
		FreeLibrary(hSetupAPI);

		return FALSE;
	}

	//Finally do the enumeration
	BOOL bMoreItems = TRUE;
	int nIndex = 0;
	SP_DEVINFO_DATA devInfo;
	PORTS port;
	TCHAR pszPortName[256];

	while (bMoreItems)
	{
		//Enumerate the current device
		devInfo.cbSize = sizeof(SP_DEVINFO_DATA);
		bMoreItems = lpfnSETUPDIENUMDEVICEINFO(hDevInfoSet, nIndex, &devInfo);
		if (bMoreItems)
		{
			//Did we find a serial port for this device
			BOOL bAdded = FALSE;

			//Get the registry key which stores the ports settings
			HKEY hDeviceKey = lpfnLPSETUPDIOPENDEVREGKEY(hDevInfoSet, &devInfo, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_QUERY_VALUE);
			if (hDeviceKey)
			{
				//Read in the name of the port
				DWORD dwSize = sizeof(pszPortName);
				DWORD dwType = 0;
				if ((RegQueryValueEx(hDeviceKey, _T("PortName"), NULL, &dwType, reinterpret_cast<LPBYTE>(pszPortName), &dwSize) == ERROR_SUCCESS) && (dwType == REG_SZ))
				{
					//If it looks like "COMX" then
					//add it to the array which will be returned
					size_t nLen = _tcslen(pszPortName);
					if (nLen > 3)
					{
						if ((_tcsnicmp(pszPortName, _T("COM"), 3) == 0) && IsNumeric(&pszPortName[3], FALSE))
						{
							//Work out the port number
							bAdded = TRUE;
						}
					}
				}

				//Close the key now that we are finished with it
				RegCloseKey(hDeviceKey);
			}

			//If the port was a serial port, then also try to get its friendly name
			if (bAdded)
			{
				TCHAR pszDesc[256];
				DWORD dwSize = sizeof(pszDesc);
				DWORD dwType = 0;
				if (lpfnSETUPDIGETDEVICEREGISTRYPROPERTY(hDevInfoSet, &devInfo, /*SPDRP_DEVICEDESC*/SPDRP_HARDWAREID, &dwType, reinterpret_cast<PBYTE>(pszDesc), dwSize, &dwSize) && (dwType == /*REG_SZ*/REG_MULTI_SZ))
				{
					// HardwareID = FTDIBUS\COMPORT&VID_0403&PID_6001
					if( _tcscmp(pszDesc, _T("FTDIBUS\\COMPORT&VID_0403&PID_AFB8")) == 0)
						port.nDP = 1;
					else
						port.nDP = 0;
				}
				else
				{
					port.nDP = 0;
				}

				port.csPort.Format(_T("%s:"), pszPortName);
				m_caComports.Add(port);
			}
		}

		++nIndex;
	}

	//Free up the "device information set" now that we are finished with it
	lpfnSETUPDIDESTROYDEVICEINFOLIST(hDevInfoSet);

	//Unload the setup dll
	FreeLibrary(hSetupAPI);


	return 0;
}

BOOL CSampleSuite::IsNumeric(LPCTSTR pszString, BOOL bIgnoreColon)
{
  size_t nLen = _tcslen(pszString);
  if (nLen == 0)
    return FALSE;

  //Assume the best
  BOOL bNumeric = TRUE;

  for (size_t i=0; i<nLen && bNumeric; i++)
  {
    bNumeric = (_istdigit(pszString[i]) != 0);
    if (bIgnoreColon && (pszString[i] == _T(':')))
      bNumeric = TRUE;
  }

  return bNumeric;
}


////////////////////////////////////////////////////////////////////////////

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);
	
	hInst = hInstance;

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Communication.DLL Initializing!\n");
		
		g_pXML = new RLFReader();	// create the XML-parser class.
		g_pZIP = new CZipArchive();	// zip-handling class

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(SampleSuiteDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Communication.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(SampleSuiteDLL);

		delete g_pZIP;
		delete g_pXML;
	}

	hInst = hInstance;

	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(SampleSuiteDLL);

	CString csModuleFN = getModuleFN(hInst);
	m_vecHInstTable.clear();

	// Setup the language filename
	CString csLangFN;
	csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	CWinApp* pApp = AfxGetApp();

	// Programs
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_PROGRAMS, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CProgramsFrame),
			RUNTIME_CLASS(CProgramsDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_PROGRAMS, csModuleFN, csLangFN, TRUE));

	// Senddata
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SENDDATA, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSenddataFrame),
			RUNTIME_CLASS(CSenddataDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SENDDATA, csModuleFN, csLangFN, TRUE));

	// Recvdata
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_RECVDATA, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CRecvdataFrame),
			RUNTIME_CLASS(CRecvdataDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_RECVDATA, csModuleFN, csLangFN, TRUE));

	// ASCII receive
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_ASCII, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CASCIIFrame),
			RUNTIME_CLASS(CASCIIDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_ASCII, csModuleFN, csLangFN, TRUE));

	// Licenses
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_LICENSES, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CLicensesFrame),
			RUNTIME_CLASS(CLicensesDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_LICENSES, csModuleFN, csLangFN, TRUE));
	
	// Settings DigitechPro
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SETTINGSDP, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSettingsDPFrame),
			RUNTIME_CLASS(CSettingsDPDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SETTINGSDP, csModuleFN, csLangFN, TRUE));

	// Settings Mantax computer
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SETTINGSMC, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSettingsMCFrame),
			RUNTIME_CLASS(CSettingsMCDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SETTINGSMC, csModuleFN, csLangFN, TRUE));

	// Settings Digitech
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SETTINGSD, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSettingsDFrame),
			RUNTIME_CLASS(CSettingsDDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SETTINGSD, csModuleFN, csLangFN, TRUE));

	// Digitech Recvdata
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_RECVDATAD, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CRecvdataDFrame),
			RUNTIME_CLASS(CRecvdataDDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_RECVDATAD, csModuleFN, csLangFN, TRUE));

	// Settings
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SETTINGS, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSettingsFrame),
			RUNTIME_CLASS(CSettingsDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SETTINGS, csModuleFN, csLangFN, TRUE));

	// Get version information; 060803 p�d
	CString csVersion, csCopyright, csCompany;
	csVersion	= getVersionInfo(hInst, VER_NUMBER);
	csCopyright	= getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany	= getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		1, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		(csLangFN),
		(csVersion),
		(csCopyright),
		(csCompany)));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */
	typedef CRuntimeClass *(*Func)(CWinApp *, LPCTSTR suite, vecINDEX_TABLE &, vecINFO_TABLE &);
	Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"), getModulesDir(), user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], "InitModule" );
					if (proc != NULL)
					{
						// call the function
						proc(pApp, csModuleFN, vecIndex, vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)
}


void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	BOOL bFound = FALSE, bIsOneInst = FALSE;
	int nType=0;
	CString sDocTitle;


	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	if(_tcscmp(func, _T("DP")) == 0)
		nType = 0;	// DigitechPro
	else if(_tcscmp(func, _T("MC")) == 0)
		nType = 1;	// Mantax computer
	else if(_tcscmp(func, _T("D")) == 0)
		nType = 2;	// Digitech

	theApp.m_nType = nType;
	theApp.m_csExtraArg = _T("");
	theApp.m_bExtra = FALSE;
	theApp.m_bFinished = FALSE;
	theApp.m_csOptions = _T("");

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		bIsOneInst = vecIndex[i].bOneInstance;

		if (nTableIndex == idx && sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;
			bFound = TRUE;

			if (g_pXML->Load(sLangFN))
			{
				sCaption = g_pXML->str(nTableIndex);
			}
			else
			{
				AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
			}

			// Check if the document or module is in this SUITE; 051213 p�d
			if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			{
				POSITION pos = pApp->GetFirstDocTemplatePosition();
				while(pos != NULL)
				{
					pTemplate = pApp->GetNextDocTemplate(pos);
					pTemplate->GetDocString(sDocName, CDocTemplate::docName);
					ASSERT(pTemplate != NULL);
					// Need to add a linefeed, infront of the docName.
					// This is because, for some reason, the document title,
					// set in resource, must have a linefeed.
					// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
					sDocName = '\n' + sDocName;

					if (pTemplate && sDocName.Compare(sResStr) == 0)
					{
						if(bIsOneInst == TRUE)
						{
							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while(posDOC != NULL)
							{
								CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
								POSITION pos = pDocument->GetFirstViewPosition();
								if(pos != NULL && pDocument->m_nType == nType)
								{
									CView* pView = pDocument->GetNextView(pos);
									pView->GetParent()->BringWindowToTop();
									pView->GetParent()->SetFocus();
									posDOC = (POSITION)1;
									break;
								}
							}

							if(posDOC == NULL)
							{
								theApp.m_bExtra = FALSE;
								CDocument* pDocument = (CDocument*)pTemplate->CreateNewDocument(); //OpenDocumentFile(NULL);
								if(pDocument == NULL) return;
								pDocument->OnNewDocument();

								CFrameWnd * pFrame = pTemplate->CreateNewFrame(pDocument, NULL);
								if(pFrame == NULL) return;
								pTemplate->InitialUpdateFrame(pFrame, pDocument);

								CString sDocTitle;
								sDocTitle.Format(_T("%s"), sCaption);
								pDocument->SetTitle(sDocTitle);

								break;
							}
						}
						else
						{
							theApp.m_bExtra = FALSE;
							CDocument* pDocument = (CDocument*)pTemplate->OpenDocumentFile(NULL);	//CreateNewDocument();
							if(pDocument == 0) return;

							CString sDocTitle;
							sDocTitle.Format(_T("%s"), sCaption);
							pDocument->SetTitle(sDocTitle);
							
							break;
						}

						break;
					}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
				}	// while(pos != NULL)
			} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			*ret = 1;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
}

// Use this function, when calling from inside another Suite/User module; 060619 p�d
void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sLangSet;
	CString sFileToOpen;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	int nType = 0;	// DigitechPro
	
	ASSERT(pApp != NULL);

	sModuleFN = getModuleFN(hInst);

	if (vecIndex.size() == 0)
		return;

	sLangSet = getLangSet();

	theApp.m_csExtraArg = _T("");
	theApp.m_bExtra = FALSE;
	theApp.m_bFinished = FALSE;
	theApp.m_csOptions = _T("");
	theApp.m_nType = nType;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		if (nTableIndex == msg->getIndex())
		{
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			sLangFN = vecIndex[i].szLanguageFN;

			sFileToOpen = msg->getFileName();

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		sResStr.LoadString(nTableIndex);

		if (g_pXML->Load(sLangFN))
		{
			sCaption = g_pXML->str(nTableIndex);
		}

		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					if(msg->getVoidBuf() != 0)
						theApp.m_csExtraArg = CString((LPTSTR)msg->getVoidBuf());
					else
						theApp.m_csExtraArg = msg->getFileName();

					if(theApp.m_csExtraArg != _T(""))
						theApp.m_bExtra = TRUE;
					theApp.m_csOptions = msg->getName();	//get Caliper SN, License code, License level and Product ID	

					pTemplate->OpenDocumentFile(NULL);
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					nDocCounter = 1;

					while (posDOC != NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);

						CString sDocTitle;
						pDocument->SetTitle(sCaption);
						nDocCounter++;
					}

					// Don't return until operation is done
					CString csBuf = msg->getArgStr();
					if( csBuf != _T(""))	// is this a multi-transfer?
					{
						while( !theApp.m_bFinished )
						{
							doEvents();
						}
					}

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}

}

BOOL CSampleSuite::CreateDirectory( const CString& directory )
/* ============================================================
	Function :		CDiskObject::CreateDirectory
	Description :	Will recursively create the directory 
					"directory".
	Access :		Public

	Return :		BOOL				-	"TRUE" if OK. 
											"GetErrorMessage" 
											will get an 
											error string if 
											"FALSE"
	Parameters :	CString directory	-	directory to 
											create

	Usage :			Call to create a directory chain.

   ============================================================*/
{
	BOOL result = TRUE;
	CString indir( directory );
	if( indir.GetLength( ) )
	{
		_TCHAR drive[ _MAX_PATH ];
		_TCHAR dir[ _MAX_DIR ];
		_TCHAR fname[ _MAX_FNAME ];
		_TCHAR ext[ _MAX_EXT ];

		// Split directory into parts
		_tsplitpath( indir, drive, dir, fname, ext );

		TCHAR currentDirectory[ _MAX_PATH ];
		::GetCurrentDirectory( _MAX_PATH, currentDirectory );

		CStringArray directories;
		CString parts = dir;

		if( parts.GetLength( ) > 2 )
		{
			if( parts.Left( 2 ) == _T( "\\\\" ) )
			{
				// We have an UNC name
				CString strComputer;
				parts = parts.Right( parts.GetLength( ) - 2 );
				int findDir = parts.Find( _TCHAR( '\\' ) );
				if( findDir!=-1)
				{
					strComputer = _T( "\\\\" ) + parts.Left( findDir );
					parts = parts.Right( parts.GetLength( ) - ( findDir + 1 ) );
				}
				_tcscpy( drive, strComputer );
			}
		}

		CString strRoot( drive );

		// Strip leading \'s
		while( parts.GetLength( ) && parts[0] == _TCHAR( '\\' ) )
			parts = parts.Right( parts.GetLength( ) - 1 );

		// Cut into separate directories
		int find = parts.Find( _TCHAR( '\\' ) );
		while( find != -1 )
		{
			directories.Add( parts.Left( find ) );
			parts = parts.Right( parts.GetLength( ) - ( find + 1 ) );
			find = parts.Find( _TCHAR( '\\' ) );
		}

		if( parts.GetLength( ) )
			directories.Add( parts );

		if( fname )
			directories.Add( fname );

		// Loop directories one-by-one, creating as necessary
		int max = directories.GetSize( );
		CString strCurrentDirectory( strRoot );

		for( int t = 0 ; t < max ; t++ )
		{
			strCurrentDirectory += _TCHAR( '\\' ) + directories[ t ];
			if( !( result = ::SetCurrentDirectory( strCurrentDirectory ) ) )
			{
				if( !( result = ::CreateDirectory( strCurrentDirectory, NULL ) ) )
				{
					t = max;
				}
			}
		}

		::SetCurrentDirectory( currentDirectory );

	}
	else
	{
		result = FALSE;
	}
	return result;
}
