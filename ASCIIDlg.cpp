// ASCIIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "ASCIIDlg.h"
#include "Usefull.h"
#include ".\asciidlg.h"

IMPLEMENT_DYNCREATE(CASCIIFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CASCIIFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CASCIIFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CASCIIFrame construction/destruction

CASCIIFrame::CASCIIFrame()
{
	m_bOnce = TRUE;
}

CASCIIFrame::~CASCIIFrame()
{
}

LRESULT CASCIIFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
			((CASCIIDlg*)GetActiveView())->OnErase();
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
			((CASCIIDlg*)GetActiveView())->OnReport();
		break;

		case ID_SAVE_ITEM:
			((CASCIIDlg*)GetActiveView())->OnSave();
		break;

		case ID_DELETE_ITEM:
			((CASCIIDlg*)GetActiveView())->OnErase();
		break;
	};

	return 0;
}


void CASCIIFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\ASCII"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CASCIIFrame::OnClose()
{
	CASCIIDlg* pView = ((CASCIIDlg*)GetActiveView());
	if(pView != 0 && ((CASCIIDlg*)GetActiveView())->m_bChanged == TRUE)
	{
		int nRet = MessageBox(g_pXML->str(1113), g_pXML->str(403), MB_YESNOCANCEL|MB_ICONQUESTION);
		if(nRet == IDCANCEL) return;	
		else if(nRet == IDYES) pView->OnSave();
	}

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CASCIIFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\ASCII"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

BOOL CASCIIFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CASCIIFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient, WM_MDISETMENU, 0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CASCIIFrame diagnostics

#ifdef _DEBUG
void CASCIIFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CASCIIFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CASCIIFrame message handlers

int CASCIIFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CASCIIFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
}

void CASCIIFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
}


/*---------------------------------------------------------------------*/

// CASCIIDlg

IMPLEMENT_DYNCREATE(CASCIIDlg, CXTResizeFormView)

CASCIIDlg::CASCIIDlg()
	: CXTResizeFormView(CASCIIDlg::IDD)
{
	m_csText = _T("");
	m_csStatus = _T("");
	m_pCComThread = NULL;
	m_bChanged = FALSE;
	m_nComport = 0;
	m_nBaud = 0;
	m_hParentWnd = NULL;
	m_csDefaultpath = _T("");
}

CASCIIDlg::~CASCIIDlg()
{
	m_font.DeleteObject();
}

void CASCIIDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CASCIIDlg)
	DDX_Text(pDX, IDC_EDIT_ASCII, m_csText);
	DDX_Text(pDX, IDC_STATIC_ASCII_STATUS, m_csStatus);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CASCIIDlg, CXTResizeFormView) //CFormView)
	//{{AFX_MSG_MAP(CASCIIDlg)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_COM_PACKET, OnPacket)
	ON_MESSAGE(WM_COM_DONE, OnDone)
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_HELP, OnCommandHelp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

LRESULT CASCIIDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	int nPageID;

	if(pDoc->m_nType == 0)	// DigitechPro
	{
		nPageID = COMMRECEIVE_TEXT1;
	}
	else if(pDoc->m_nType == 1)	// Mantax computer
	{
		nPageID = COMMRECEIVE_TEXT1;
	}
	else if(pDoc->m_nType == 2)	// Digitech
	{
		nPageID = COMMRECEIVE_TEXT;
	}

	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, nPageID);
	return 0;
}

// CASCIIDlg diagnostics

#ifdef _DEBUG
void CASCIIDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CASCIIDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CASCIIDlg message handlers
void CASCIIDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();


	CClientDC dc(this);
	VERIFY(m_font.CreatePointFont(100, _T("Courier New"), &dc));
	GetDlgItem(IDC_EDIT_ASCII)->SetFont(&m_font, 0);

	int nPos = 0;
	if(pDoc->m_nType == 0)	// DP
	{
		m_nBaud = CStringToInt(theApp.m_csDP_Baudrate);
		m_nComport = CStringToInt(theApp.m_csDP_Comport.Tokenize(_T("COM:"), nPos));
	}
	else if(pDoc->m_nType == 1)	// MC
	{
		m_nBaud = CStringToInt(theApp.m_csMC_Baudrate);
		m_nComport = CStringToInt(theApp.m_csMC_Comport.Tokenize(_T("COM:"), nPos));
	}
	else if(pDoc->m_nType == 2)	// D
	{
		m_nBaud = 1200;
		m_nComport = CStringToInt(theApp.m_csD_Comport.Tokenize(_T("COM:"), nPos));
	}

	m_csStatus.Format(_T("%s (COM%d: %dbaud)"), g_pXML->str(1020), /*m_pCComThread->*/m_nComport, /*m_pCComThread->*/m_nBaud);

	PORTS port;
	for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
	{
		port = theApp.m_caComports.GetAt(nLoop);
		if(port.csPort == theApp.m_csDP_Comport && port.nDP == 1)
		{
			m_csStatus.Format(_T("%s (COM%d: (DP) %dbaud)"), g_pXML->str(1020), /*m_pCComThread->*/m_nComport, /*m_pCComThread->*/m_nBaud);
			break;
		}
	}


	// set the resizes
	SetResize(IDC_EDIT_ASCII, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_STATIC_ASCII_STATUS, SZ_BOTTOM_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}

BOOL CASCIIDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// we have recieved a packet.
LRESULT CASCIIDlg::OnPacket(WPARAM wParm, LPARAM lParm)
{
	m_bChanged = TRUE;

	GetParentFrame()->BringWindowToTop();
	SetFocus();
	if(m_csText.GetLength() == 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, TRUE);
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, TRUE);

	if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}
	else if((char)wParm == '�')
	{
		wParm = '�';
	}


	if((char)wParm == 13)
	{
		m_csText += '\r';
	}
	else if((char)wParm == 10)
	{
		m_csText += '\n';
		UpdateData(FALSE);
	}
	else
	{
		m_csBuf.Format(_T("%C"), (char)wParm);
		m_csText += m_csBuf;
	}

	return 0;
}

// the transfer is done
LRESULT CASCIIDlg::OnDone(WPARAM wParm, LPARAM lParm)
{
	UpdateData(FALSE);

	// transfer ended, tell why.
	switch(wParm)
	{
		case 1:
			MessageBox(g_pXML->str(1031), g_pXML->str(1021), MB_ICONINFORMATION);	// transfer done
			break;
		case 2:
			MessageBox(g_pXML->str(1032), g_pXML->str(1021), MB_ICONSTOP);	// transfer failed
			break;
		case 3:
			MessageBox(g_pXML->str(1033), g_pXML->str(1021), MB_ICONSTOP);	// unable to open file
			break;
		case 4:
			MessageBox(g_pXML->str(1034), g_pXML->str(1021), MB_ICONSTOP);	// unable to allocate memory
			break;
		case 5:
			MessageBox(g_pXML->str(1035), g_pXML->str(1021), MB_ICONSTOP);	// unable to open port
			break;
		case 6:
			MessageBox(g_pXML->str(1036), g_pXML->str(1021), MB_ICONSTOP);	// unable to save file
			break;
		case 7:
			MessageBox(g_pXML->str(1037), g_pXML->str(1021), MB_ICONSTOP);	// unable to receive file
			break;
	}

	((CASCIIFrame*)GetParentFrame())->OnClose();

	return 0;
}

void CASCIIDlg::OnDestroy()
{
	if(m_hParentWnd != NULL)	// signal to the calling window that we are leaving
	{
		::SendMessage(::GetParent(m_hParentWnd), WM_CLOSE, 0, 0);
	}

	CXTResizeFormView::OnDestroy();
}

// save the data in the editcontrol to a file
int CASCIIDlg::OnSave()
{
	CString csBuf;

	if(m_bChanged == FALSE) return FALSE;

	if(m_csFilepath == _T(""))
	{
		// pop a filedialog, asking for filename
		CFileDialog dlg(FALSE, _T("txt"), m_csFilepath, OFN_EXPLORER|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, _T("Text Files (*.txt)|*.txt|All Files (*.*)|*.*||"), this);
		dlg.m_ofn.lpstrTitle = g_pXML->str(1100);
		dlg.m_ofn.lpstrInitialDir = m_csDefaultpath;
		int nRet = dlg.DoModal();
		if(nRet == IDCANCEL)
		{
			return FALSE;
		}

		csBuf = dlg.GetPathName();
		m_csDefaultpath = dlg.GetPathName().Left(dlg.GetPathName().GetLength() - dlg.GetFileName().GetLength());
	}
	else
		csBuf = m_csFilepath;

	// save the contents of the editcontrol into the file
	UpdateData(FALSE);
	CFile fh;
	if(fh.Open(csBuf, CFile::modeCreate|CFile::modeWrite, 0) == 0)
	{
		MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
		return FALSE;
	}

	fh.Write(m_csText.GetBuffer(), m_csText.GetLength() * sizeof(TCHAR) );
	fh.Close();

	m_bChanged = FALSE;
	m_csFilepath = csBuf;
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, FALSE);
	m_csStatus.Format(_T("%s (COM%d: %dbaud)\r\n%s"), g_pXML->str(1020), m_nComport, m_nBaud, m_csFilepath);
	UpdateData(FALSE);

	return TRUE;
}

// show the received data in a report
void CASCIIDlg::OnReport()
{
	UpdateData(FALSE);

	// save the file into a temporary place
	CFile fh;
	CString csFilename;
	csFilename.Format(_T("%stemp.txt"), theApp.m_csTemppath);

	if(fh.Open(csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
	{
		MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
		return;
	}

	char *pszBuf;
	pszBuf = (char *)malloc(m_csText.GetLength());
	sprintf(pszBuf, "%S", m_csText);
	fh.Write(pszBuf, strlen(pszBuf));
	fh.Close();

	free(pszBuf);


	// open that file in the reportviewer
	BOOL bRet = AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(300, _T("OpenSuiteEx"),	
		_T("Reports.dll"),
		getProgDir() + SUBDIR_REPORTS + _T("\\showtext_report.fr3"),
		csFilename,
		_T("")));
}

// clear all text.
void CASCIIDlg::OnErase()
{
	if(m_bChanged == TRUE)
	{
		int nRet = MessageBox(g_pXML->str(1113), g_pXML->str(403), MB_YESNOCANCEL|MB_ICONQUESTION);
		if(nRet == IDCANCEL) return;	
		else if(nRet == IDYES) OnSave();
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);

	m_bChanged = FALSE;
	m_csFilepath = _T("");
	m_csText = _T("");

	m_csStatus.Format(_T("%s (COM%d: %dbaud)\r\n%s"), g_pXML->str(1020), m_nComport, m_nBaud, m_csFilepath);

	UpdateData(FALSE);
}

// create a new, and fresh, beginning...
void CASCIIDlg::OnNew()
{
	// ask if we should save the old data.
	if(m_csText.GetLength() > 0)
	{
		int nRet = AfxMessageBox(g_pXML->str(1113), MB_YESNOCANCEL);
		if(nRet == IDYES)
		{
			nRet = OnSave();
			if(nRet == FALSE) return;

			m_csFilepath = _T("");
		}
		else if(nRet == IDCANCEL)
			return;
	}

	m_csText = _T("");
	UpdateData(FALSE);;
}

void CASCIIDlg::OnSetFocus(CWnd* pOldWnd)
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_OPEN_ITEM, FALSE);

	if(m_csText.GetLength() != 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);
	}

	if(m_bChanged == TRUE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, FALSE);
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_END, FALSE);

	CXTResizeFormView::OnSetFocus(pOldWnd);
}
