#pragma once

#include "ComThread.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CRecvdataFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CRecvdataFrame)

	BOOL m_bOnce;
public:
	CRecvdataFrame();

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecvdataFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRecvdataFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CRecvdataFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};


// CRecvdataDlg form view
class CRecvdataDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CRecvdataDlg)

public:
	CString m_csFilename;
	CString m_csFilenameShort;

protected:
	CRecvdataDlg();   // standard constructor);
	virtual ~CRecvdataDlg();

	CMDISampleSuiteDoc* pDoc;

	CComThread* m_pCComThread;
	CProgressCtrl m_cProgress;

	// dialog text
	CString m_csFile;
	CString m_csComport;
	CString m_csBaudrate;
	CString m_csLicense;
	CString m_csStatus;
	CString m_csPath;
	int m_nPos;	// byte-counter

	BOOL m_bFOO;
	BOOL m_bFirst;

public:
	enum { IDD = IDD_RECVDATA };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	BOOL m_bQuit;

protected:
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	LRESULT OnPacket(WPARAM wParm, LPARAM lParm);
	LRESULT OnLength(WPARAM wParm, LPARAM lParm);
	LRESULT OnDone(WPARAM wParm, LPARAM lParm);
	LRESULT OnFile(WPARAM wParm, LPARAM lParm);
	LRESULT OnOpenAscii(WPARAM wParm, LPARAM lParm);
	afx_msg int OpenComThread();
	//{{AFX_MSG(CListCtrlDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedButtonTransfer();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
