#pragma once
#include "afxwin.h"
#include "SampleSuiteForms.h"


#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CSettingsDFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CSettingsDFrame)

	BOOL m_bOnce;
public:
	CSettingsDFrame();
	static XTPDockingPanePaintTheme m_themeCurrent;

private:
	int m_nSelProgram;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsDFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSettingsDFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CSettingsDFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
protected:
	int LoadWindowPlacement();
	int SaveWindowPlacement();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CSettingsDDlg form view

class CSettingsDDlg : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSettingsDDlg)

protected:
	CSettingsDDlg();           // protected constructor used by dynamic creation
	virtual ~CSettingsDDlg();

	CMDISampleSuiteDoc* pDoc;

	CXTPPropertyGridItem* m_pItemInterface;
	CXTPPropertyGridItem* m_pItemBaudrate;
	CXTPPropertyGridItem* m_pItemComport;
	CXTPPropertyGridItemBool* m_pItemBool1;
	CXTPPropertyGridItemBool* m_pItemBool2;
	CXTPPropertyGridItemBool* m_pItemBool3;
	CXTPPropertyGridItemBool* m_pItemBool4;
	CXTPPropertyGridItem* m_pItemPath1;
	CXTPPropertyGridItem* m_pItemPath2;

	CXTPPropertyGridItem* m_pOutput;
	CXTPPropertyGridItem* m_pSpc1;
	CXTPPropertyGridItem* m_pSpc2;
	CXTPPropertyGridItem* m_pSpc3;
	CXTPPropertyGridItem* m_pSpc4;
	CXTPPropertyGridItem* m_pSpc5;
	CXTPPropertyGridItem* m_pSpc6;
	CXTPPropertyGridItem* m_pSpc7;
	CXTPPropertyGridItem* m_pSpc8;

public:
	enum { IDD = IDD_SETTINGSD };
	//{{AFX_DATA(CPropertyGridDlg)
	CStatic m_wndPlaceHolder;
	//}}AFX_DATA
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

protected:
	CXTPPropertyGrid m_wndPropertyGrid;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
public:
	afx_msg LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
};


