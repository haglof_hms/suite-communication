#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "ComThread.h"

// CTransferDlg dialog

class CTransferDlg : public CDialog
{
	DECLARE_DYNAMIC(CTransferDlg)

protected:
	// dialog text
	CString m_csFile;
	CString m_csComport;
	CString m_csBaudrate;
	CString m_csLicense;
	CString m_csStatus;

public:
	CTransferDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTransferDlg();

	BOOL m_bQuit;
	BOOL m_bAbort;
	BOOL m_bError;

	int m_nLength;
	int m_nType;	// 0 = kermit, 1 = ascii, 2 = inet
	int m_nDirection;	// 0 = upload, 1 = download
	int m_nDevice;	// 0 = DigitechPro, 1 = Mantax computer, 2 = Digitech
	CString m_csFilename;
	CString m_csFilenameShort;
	CString m_csDestpath;	// local destinationpath

	int m_nSerial;
	int m_nProdID;
	int m_nLevel;
	int m_nCode;
	CString m_csLang;

// Dialog Data
	enum { IDD = IDD_TRANSFER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CComThread* m_pCComThread;
	LRESULT OnPacket(WPARAM wParm, LPARAM lParm);
	LRESULT OnLength(WPARAM wParm, LPARAM lParm);
	LRESULT OnDone(WPARAM wParm, LPARAM lParm);
	LRESULT OnFile(WPARAM wParm, LPARAM lParm);
	void OnCancel();
	DECLARE_MESSAGE_MAP()
public:
	CProgressCtrl m_cProgress;
	CString m_csStatic;
	afx_msg void OnBnClickedButtonTransfer();
	virtual BOOL OnInitDialog();
};
