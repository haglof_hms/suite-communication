// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SettingsDPDlg.h"
#include "MyLocale.h"
#include "SampleSuiteForms.h"
#include "CustomItems.h"
#include "xmlParser.h"
#include "w3c.h"
#include "usefull.h"


IMPLEMENT_DYNCREATE(CSettingsDPFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSettingsDPFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSettingsDPFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDPFrame construction/destruction

XTPDockingPanePaintTheme CSettingsDPFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CSettingsDPFrame::CSettingsDPFrame()
{
	m_bOnce = TRUE;
}

CSettingsDPFrame::~CSettingsDPFrame()
{
}

BOOL CSettingsDPFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsDPFrame diagnostics

#ifdef _DEBUG
void CSettingsDPFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSettingsDPFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSettingsDPFrame message handlers

int CSettingsDPFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CSettingsDPFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsDP"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CSettingsDPFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

LRESULT CSettingsDPFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CSettingsDPFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsDP"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

void CSettingsDPFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CSettingsDPDlg

IMPLEMENT_DYNCREATE(CSettingsDPDlg, CXTResizeFormView)

CSettingsDPDlg::CSettingsDPDlg()
	: CXTResizeFormView(CSettingsDPDlg::IDD)
{
}

CSettingsDPDlg::~CSettingsDPDlg()
{
}

void CSettingsDPDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PLACEHOLDER, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsDPDlg, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_HELP, OnCommandHelp)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
END_MESSAGE_MAP()


// CSettingsDPDlg diagnostics

#ifdef _DEBUG
void CSettingsDPDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSettingsDPDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CSettingsDPDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

LRESULT CSettingsDPDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMSETTINGS_DIGITECH_PRO);
	return 0;
}


void CSettingsDPDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	// get available comports and fill the combobox
	theApp.GetComPorts();
	
	// get the size of the placeholder, this will be used when creating the grid.
	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );

	CString csBuf;

	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

		// Serial category.
		CXTPPropertyGridItem* pSerial = m_wndPropertyGrid.AddCategory(g_pXML->str(1038));

		// add child items to category.
		m_pItemBaudrate = pSerial->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1039), theApp.m_csDP_Baudrate));
		CXTPPropertyGridItemConstraints* pList = m_pItemBaudrate->GetConstraints();
		pList->AddConstraint(_T("1200"));
		pList->AddConstraint(_T("2400"));
		pList->AddConstraint(_T("4800"));
		pList->AddConstraint(_T("9600"));
		pList->AddConstraint(_T("19200"));
		pList->AddConstraint(_T("38400"));
		pList->AddConstraint(_T("57600"));
		pList->AddConstraint(_T("115200"));
		m_pItemBaudrate->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);

		PORTS port;
		CString csPort = theApp.m_csDP_Comport;
		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.csPort == theApp.m_csDP_Comport && port.nDP == 1)
			{
				csPort.Format(_T("%s (DP)"), port.csPort);
				break;
			}
		}

		m_pItemComport = pSerial->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1040), csPort));
		pList = m_pItemComport->GetConstraints();

		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.nDP == 0)
			{
				pList->AddConstraint(port.csPort);
			}
			else
			{
				csBuf.Format(_T("%s (DP)"), port.csPort);
				pList->AddConstraint(csBuf);
			}
		}
		m_pItemComport->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);

		pSerial->Expand();
		m_pItemBaudrate->Select();


		// create global settings category.
		CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(g_pXML->str(1041));

		// add child items to category.
		m_pItemPath1 = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1042), theApp.m_csDP_Filepath));
		m_pItemPath1->SetDescription(g_pXML->str(1043));

		m_pItemPath2 = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1044), theApp.m_csDP_Downloadpath));
		m_pItemPath2->SetDescription(g_pXML->str(1045));

		pPaths->Expand();


		m_pLanguage = m_wndPropertyGrid.AddCategory(g_pXML->str(1125));
		m_pLanguage->SetDescription(g_pXML->str(1126));

		GetLangsFromXML();	// get all langauges that are in programs.xml

		// get an array containing which language that should be enabled
		CStringArray caLangs;
		GetLangSettings(theApp.m_csDP_Langs, _T(";"), &caLangs);

		LANGS lang;
		BOOL bFound;
			
		for(int nLoop=0; nLoop<m_caLangs.GetSize(); nLoop++)
		{
			lang = m_caLangs.GetAt(nLoop);

			bFound = FALSE;
			if(caLangs.GetSize() > 0)
			{
				for(int nLoop2=0; nLoop2<caLangs.GetSize(); nLoop2++)
				{
					if(lang.csAbb.CompareNoCase(caLangs.GetAt(nLoop2)) == 0)
					{
						bFound = TRUE;
						break;
					}
				}
			}
			else
			{
				bFound = TRUE;
			}

			m_pLanguage->AddChildItem(new CMyPropGridItemBool(g_pXML->str(900), g_pXML->str(901), lang.csName, bFound));
		}

		m_pLanguage->Expand();			
	}

	// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}


// CSettingsDPDlg message handlers

void CSettingsDPDlg::OnDestroy()
{
	CString csLangs;
	CXTPPropertyGridItemBool* pBool;
	CXTPPropertyGridItems* pChilds;
	LANGS lang;

	// get which languages to use/show
	pChilds = m_pLanguage->GetChilds();
	for(int nLoop=0; nLoop<pChilds->GetCount(); nLoop++)
	{
		pBool = (CXTPPropertyGridItemBool*)pChilds->GetAt(nLoop);
		if(pBool->GetBool())
		{
			lang = m_caLangs.GetAt(nLoop);
			csLangs += (lang.csAbb + _T(";"));
		}
	}
	theApp.m_csDP_Langs = csLangs;

	theApp.SaveRegSettings();

	CXTResizeFormView::OnDestroy();
}

void CSettingsDPDlg::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

LRESULT CSettingsDPDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_SORTORDER_CHANGED:
		{
		}
		break;

		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemComport)
			{
				int nFoo = 0;
				CString csBuf = m_pItemComport->GetValue().Tokenize(_T(" "), nFoo);
				if(csBuf == _T(""))
					theApp.m_csDP_Comport = m_pItemComport->GetValue();
				else
					theApp.m_csDP_Comport = csBuf;
			}
			else if(pItem == m_pItemBaudrate)
			{
				theApp.m_csDP_Baudrate = m_pItemBaudrate->GetValue();
			}
			else if(pItem == m_pItemPath1)
			{
				theApp.m_csDP_Filepath = m_pItemPath1->GetValue();
				if(theApp.m_csDP_Filepath.GetAt(theApp.m_csDP_Filepath.GetLength()-1) != '\\')
					theApp.m_csDP_Filepath += _T("\\");
			}
			else if(pItem == m_pItemPath2)
			{
				theApp.m_csDP_Downloadpath = m_pItemPath2->GetValue();
				if(theApp.m_csDP_Downloadpath.GetAt(theApp.m_csDP_Downloadpath.GetLength()-1) != '\\')
					theApp.m_csDP_Downloadpath += _T("\\");
			}
		}
		break;

		case XTP_PGN_SELECTION_CHANGED:
		{
		}
		break;
	}

	return FALSE;
}

BOOL CSettingsDPDlg::IsNumeric(LPCTSTR pszString, BOOL bIgnoreColon)
{
  size_t nLen = _tcslen(pszString);
  if (nLen == 0)
    return FALSE;

  BOOL bNumeric = TRUE;

  for (size_t i=0; i<nLen && bNumeric; i++)
  {
    bNumeric = (_istdigit(pszString[i]) != 0);
    if (bIgnoreColon && (pszString[i] == _T(':')))
      bNumeric = TRUE;
  }

  return bNumeric;
}

int CSettingsDPDlg::GetLangsFromXML()
{
	int i, j, nLangs, iterator=0, jterator=0;
	XMLNode xNode, xChild, xLang;
	CString csLang, csBuf;
	LANGS lang;

	m_caLangs.RemoveAll();

	CString csDestpath;
	if(pDoc->m_nType == 0) csDestpath = theApp.m_csDP_Filepath;	// digitechpro
	else if(pDoc->m_nType == 1) csDestpath = theApp.m_csMC_Filepath;	// mantax computer
	else if(pDoc->m_nType == 2) csDestpath = theApp.m_csD_Filepath;	// digitech

	xNode = XMLNode::openFileHelper(csDestpath + _T("Programs.xml"), _T("XML"));
	if(xNode.isEmpty() != 1)
	{
		xNode = xNode.getChildNode(_T("Programs"));	// get the first tag, "Programs"

		if(xNode.isEmpty() != 1)
		{
			int n = xNode.nChildNode(_T("Product"));
			for(i=0; i<n; i++)
			{
				xChild = xNode.getChildNode(_T("Product"), &iterator);
				nLangs = xChild.nChildNode(_T("Lang"));
				if(nLangs != 0)	// we have some langs
				{
					for(j=0; j<nLangs; j++)
					{
						xLang = xChild.getChildNode(_T("Lang"), &jterator);
						csLang = xLang.getChildNode(_T("ID")).getText();

						if(csLang != _T(""))
						{
							lang.csAbb = csLang;
							lang.csName = CLocale::GetLangString(csLang);
							m_caLangs.Add(lang);
						}

					}

					jterator = 0;
				}
			}
		}
	}

	// remove dupes from the array
	LANGS lang2;
	for(int nLoop=0; nLoop<m_caLangs.GetSize(); nLoop++)
	{
		lang = m_caLangs.GetAt(nLoop);

		for(int nLoop2=m_caLangs.GetSize()-1; nLoop2>=0; nLoop2--)
		{
			if(nLoop != nLoop2)
			{
				lang2 = m_caLangs.GetAt(nLoop2);
				if(lang.csAbb == lang2.csAbb)
				{
					m_caLangs.RemoveAt(nLoop2);
				}
			}
		}
	}

	// sort the array based on csName
	BOOL bSorted = TRUE;
	while(bSorted == TRUE)
	{
		bSorted = FALSE;
		for(int nLoop=0; nLoop<m_caLangs.GetSize()-1; nLoop++)
		{
			lang = m_caLangs.GetAt(nLoop);
			lang2 = m_caLangs.GetAt(nLoop+1);

			if(lang.csName.CompareNoCase(lang2.csName) > 0)
			{
				m_caLangs.SetAt(nLoop, lang2);
				m_caLangs.SetAt(nLoop+1, lang);
				bSorted = TRUE;
			}
		}
	}

	return TRUE;
}
