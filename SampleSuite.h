#pragma once

#ifndef _SampleSuite_H_
#define _SampleSuite_H_

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include "stdafx.h"
#include "ZipArchive.h"
#include <vector>

#define COLUMN_FLAG		  0
#define COLUMN_PROGRAM    1
#define COLUMN_VERSION    2
#define COLUMN_NEWVERSION 3
#define COLUMN_LANGUAGE   4
#define COLUMN_TLA        5

typedef struct _stand
{
	TCHAR szName[100];		// name of the stand
	unsigned int nNumPlots;	// number of plots totaly
	unsigned int nNumTrees;	// number of trees totaly
	float fPlotRadius;		// radius of the plots
	unsigned int nPart;		// part of the stand
	unsigned int nMethod;	// measuringmethod
} STAND;

typedef struct _tree
{
	unsigned int nPlot;	// which plot?
	
	unsigned int nSpc;		// specie
	unsigned int nDia;		// diameter
	unsigned int nHeight;	// height
	unsigned int nHeight2;	// height
} TREE;


// struct for receiveing data
typedef struct _tagDEVICEDATA
{
	int nDevice;	// 0=Digitech, 1=VertexIII, 2=VertexI, 3=Mantax
	int nId;		// ID/Serial of device

	int nCommmand;	// I.e Digicopy
	int nNumVars;	// Bitfield telling which variables available

	int nData1;		// Datavariables
	int nData2;		//
	int nData3;		//
	int nData4;		//
	int nData5;		//
} DD;

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

// Initialize the DLL, register the classes etc
extern "C" void DLL_BUILD InitSuite(CStringArray *, vecINDEX_TABLE &, vecINFO_TABLE &);

// Open a document view
extern "C" void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);

//extern SampleSuite theApp;
extern RLFReader* g_pXML;

// zip
extern CZipArchive* g_pZIP;


typedef struct _tagPORTS
{
	int nDP;
	CString csPort;
} PORTS;


class CSampleSuite //: public CWinApp
{
public:
	CSampleSuite();
	~CSampleSuite();

//	CZipArchive m_zip;

	CArray<PROGRAM, PROGRAM> m_caPrograms;
	CArray<LANGUAGE, LANGUAGE> m_caLanguages;
	CArray<KEY, KEY> m_caKeys;
	
	CArray<STAND, STAND> m_caStands;	// stands
	CArray<TREE, TREE> m_caTrees;		// trees

	CString m_csSelProgramDP;
	int m_nSelProgramDP;
	int m_nSelLangDP;
	CString m_csSelProgramMC;
	int m_nSelProgramMC;
	int m_nSelLangMC;

	int m_nType;	// ugly hack to know what called a document
	CString m_csFilepath;	// ^^
	CString m_csDownloadpath;
	CString m_csTemppath;	// where to store temporary stuff

	CString m_csWebserver;	// where to download files
	CString m_csWebpath;	// where to download files
	CString m_csLocalpath;	//
	BOOL m_bLocal;			// local (TRUE) / web (FALSE)

	// DigitechPro settings
	CString m_csDP_Comport;
	CString m_csDP_Baudrate;
	CString m_csDP_Filepath;
	CString m_csDP_Downloadpath;
	CString m_csDP_Langs;

	// Mantax computer settings
	CString m_csMC_Comport;
	CString m_csMC_Baudrate;
	CString m_csMC_Filepath;
	CString m_csMC_Downloadpath;

	// Digitech settings
	CString m_csD_Comport;
	CString m_csD_Baudrate;
	BOOL m_bD_Del0;
	BOOL m_bD_Height8;
	BOOL m_bD_Two0;
	BOOL m_bD_Three0;
	CString m_csD_Filepath;
	CString m_csD_Downloadpath;
	int m_nD_OutFormat;
	CArray<CString, CString> m_caD_Species;
/*	CString m_csD_Spc1;
	CString m_csD_Spc2;
	CString m_csD_Spc3;
	CString m_csD_Spc4;
	CString m_csD_Spc5;
	CString m_csD_Spc6;
	CString m_csD_Spc7;
	CString m_csD_Spc8;*/
	
	bool m_bExtra;
	bool m_bFinished;
	CString m_csExtraArg;

	CString m_csOptions;	//added by magnus 100811 


	void SaveRegSettings();	// save settings to the registry

	CArray<PORTS, PORTS> m_caComports;
	int GetComPorts();
	BOOL IsNumeric(LPCTSTR pszString, BOOL bIgnoreColon);
	BOOL CreateDirectory( const CString& directory );

private:
	HINSTANCE m_hDll;
	HRESULT (WINAPI *m_pSHGetFolderPath)(HWND hwndOwner, int nFolder, HANDLE hToken, DWORD dwFlags, LPTSTR pszPath);

};

extern CSampleSuite theApp;

#endif
