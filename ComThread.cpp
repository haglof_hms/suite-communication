#include "stdafx.h"
#include "ComThread.h"
#include "w3c.h"
#include "SampleSuite.h"
#include "Kermit.h"
#include "DP_Code.h"
#include "SampleSuiteForms.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CComThread
UINT CComThread::tm_CComThreadOnDoContinuous = ::RegisterWindowMessage(_T("CComThread OnDoContinuous"));

IMPLEMENT_DYNCREATE( CComThread, CWinThread )

CComThread::CComThread( void )
{
	m_hStopEvent = ::CreateEvent( NULL, TRUE, FALSE, NULL );
	m_bQuit = FALSE;
	m_csFilename = _T("");
	m_nType = 0;	// kermit
	m_nDirection = 0;	// up

	m_nBaud = 9600;

	m_nSerial = -1;
	m_nProdID = -1;
	m_nLevel = -1;
	m_nCode = -1;
	m_csLang = _T("");

	m_bDC = 0;

	m_hWnd2 = NULL;

	ck = NULL;

	m_bCanIQuit = FALSE;
}

CComThread::~CComThread( void )
{
}

BOOL CComThread::InitInstance( void )
{
	return TRUE;
}

int CComThread::ExitInstance( void )
{
	return CWinThread::ExitInstance( );
}

BEGIN_MESSAGE_MAP( CComThread, CWinThread )
	//{{AFX_MSG_MAP( CComThread )
		ON_REGISTERED_THREAD_MESSAGE( tm_CComThreadOnDoContinuous, OnDoContinuous )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP( )

/////////////////////////////////////////////////////////////////////////////
// CComThread message handlers

void CComThread::OnDoContinuous(WPARAM wParam, LPARAM lParam)
{
	m_bQuit = FALSE;

	if(m_nDirection == 0)	// Upload
	{
		Up();
	}
	else if(m_nDirection == 1)	// Download
	{
		Down();
	}

	::ResetEvent( m_hStopEvent );

	m_bCanIQuit = TRUE;
}

void CComThread::Abort()
{
	if(m_nType == 0 ||	// kermit
		m_nType == 1 || // ascii
		m_nType == 4 || m_nType == 5)	// digitech copy & digitech pc
	{
		if(ck != NULL)
		{
			while(m_bCanIQuit == FALSE)
			{
				Sleep(500);
			};

			ck->ker_cancel();
		}
	}

	::SetEvent( m_hStopEvent );
	m_bQuit = TRUE;
}

int CComThread::Up()
{
	char szFilename[512];
	CFile fh;
	char *szFileBuf;

	if(m_nType == 0)	// Kermit
	{
		ck = new CKermit;
		ck->m_hWnd = m_hWnd;

		if(m_csFilename.Find(_T(".512")) != -1 || m_csFilename.Find(_T(".128")) != -1)
		{
			// mantax binary
			int nRet = ck->Open(m_nComport, m_nBaud);
			m_bCanIQuit = TRUE;
			if(nRet != 0)	// could not open port!
			{
				if(!fh.Open(m_csDestpath + m_csFilename, CFile::modeRead))
				{
					::PostMessage(m_hWnd, WM_COM_DONE, 3, 0);
					return FALSE;
				}
				::PostMessage(m_hWnd, WM_COM_LENGTH, fh.GetLength(), 0);
				fh.Close();

				sprintf(szFilename, "%S", m_csFilenameShort);
				nRet = ck->start(szFilename);

				if(nRet == 1)
				{
					ck->Close();
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// error: transfer done
					return FALSE;
				}
				else	// error
				{
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// error: transfer aborted
					return FALSE;
				}
			}
			else
			{
				delete ck; ck = NULL;
				::PostMessage(m_hWnd, WM_COM_DONE, 5, 0);	// error: comport
				return FALSE;
			}
		}
		else	// not mantax binary
		{
			// open the serialport.
			int nOpenPort = ck->Open(m_nComport, m_nBaud);
			if(nOpenPort == 0)	// could not open port!
			{
				delete ck; ck = NULL;
				::PostMessage(m_hWnd, WM_COM_DONE, 5, 0);	// error: comport
				return FALSE;
			}

			// transfer one file at a time
			// filenames are separated with a %-sign
			CString csTmp;
			int nCurPos = 0;
			csTmp = m_csFilename.Tokenize(_T("%"), nCurPos);
			while(csTmp != _T(""))
			{
				m_csFilenameShort = csTmp.Right(csTmp.GetLength() - csTmp.ReverseFind('\\') -1 );

				// open the file and read in the data.
				if(!fh.Open(m_csDestpath + csTmp/*m_csFilename*/, CFile::modeRead))
				{
					fh.Close();
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 3, 0);
					return FALSE;
				}

				ULONG dwFilesize = fh.GetLength();
				szFileBuf = (char *)malloc(dwFilesize+1);
				if(szFileBuf == NULL )	// check that the malloc was sucessfull.
				{
					fh.Close();
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 4, 0);
					return FALSE;
				}

				// send the file size
				::PostMessage(m_hWnd, WM_COM_LENGTH, dwFilesize, 0);

				// send the filename
				::PostMessage(m_hWnd, WM_COM_FILE, 0, (LPARAM)((LPTSTR)m_csFilenameShort.GetBuffer()) );

				// read the file into a temporar buffer
				fh.Read(szFileBuf, dwFilesize);
				fh.Close();

				// fix the code into the buffer.
				if(m_nSerial != -1 && m_nCode != -1)
				{
					m_nLevel--;

					if(CheckCode(m_nProdID, m_nSerial, m_nLevel, m_nCode))
					{
						prg_struct_type prg;
						memset(&prg, 0, sizeof(prg_struct_type));
						memcpy(&prg, szFileBuf, sizeof(prg_struct_type));

						prg.productcode = prg.productcode + (m_nLevel << 28);	// put the level into the productcode
						prg.licenstart = m_nSerial;
						prg.licensend = m_nSerial;
						prg.checksum_ = crc16_typ2((unsigned char*)&prg, sizeof(prg_struct_type)-4);

						memcpy(szFileBuf, &prg, sizeof(prg_struct_type));
					}
				}


				// get the filename out of the path.
				sprintf(szFilename, "%S", m_csFilenameShort);

				m_bCanIQuit = TRUE;

				// send the data with kermit
				int nRet = ck->ker_send_file(szFilename, (unsigned char*)szFileBuf, dwFilesize);

				free(szFileBuf);

				if(nRet == -1 && strstr(ck->k.ker_last_error, "QUIT") != NULL)
				{
					ck->Close();
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// error: transfer aborted
					return FALSE;
				}

				// parse next filename
				csTmp = m_csFilename.Tokenize(_T("%"), nCurPos);
			}
		}

		ck->Close();
		delete ck; ck = NULL;
		::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// error: transfer done
	}
	else if(m_nType == 1)	// Ascii
	{
	}
	else if(m_nType == 2)	// Inet
	{
	}

	return TRUE;
}

int CComThread::Down()
{
	char szBuf[1024];
	int nRet, nLoop;
	char *szFilename;
	unsigned char *szArea;

	if(m_nType == 0 || m_nType == 1)	// ASCII or kermit
	{
		szFilename = (char*)malloc(2048);
		szArea = (unsigned char*)malloc(8192);

		ck = new CKermit;
		ck->ker_filepath = m_csDestpath;

		// open the serialport.
		int nRet = ck->Open(m_nComport, m_nBaud);
		m_bCanIQuit = TRUE;
		if(nRet != FALSE)	// could not open port!
		{
			snoop:

			// snoop into the first couple of packets,
			// to determine if it is ASCII or kermit.
			for(nLoop=0; nLoop<11; nLoop++)
			{
				nRet = ck->read_ker_char_();

				if(nRet == -1) goto quit;
				szBuf[nLoop] = (char)nRet;
			}
			szBuf[nLoop] = '\0';

			// set type
			if(strstr(szBuf, "% @-#Y") != NULL ||
				strstr(szBuf, "% -#Y") != NULL)
			{
				m_nType = 0;	// Kermit
				ck->m_hWnd = m_hWnd;

				// send a NAK to the other side
				char str[7]={1,35,32,78,51,13,0}; //NACK
				ck->SendData(str, strlen(str));
			}
			else	// ascii
			{
				// we have to open the ASCII-window
				// and push the data we got so far into it.
				::SendMessage(m_hWnd, WM_COM_ASCII, 0, 0);

				if(m_hWnd2 != NULL)
				{
					m_nType = 1;	// ASCII
					ck->m_hWnd = m_hWnd2;

					for(nLoop=0; nLoop<11; nLoop++)
					{
						::SendMessage(m_hWnd2, WM_COM_PACKET, (int)szBuf[nLoop], 0);
					}
				}
				else	// things got fucked up
				{
				quit:
					m_nType = -1;
					free(szFilename);
					free(szArea);

					ck->Close();
					delete ck; ck = NULL;
					::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// transfer aborted
					return FALSE;
				}
			}
		}
		else	// kermit->open()
		{
			free(szFilename);
			free(szArea);

			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 5, 0);	// error: comport
			return FALSE;
		}
	}

	if(m_nType == 0)	// Kermit
	{
		szFilename[0] = 0;
		nRet = ck->ker_server(szFilename, szArea, 8191);

		// check if the file is a textfile.
		// if so, open it in notepad.
		if(nRet != -1)
		{
			::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done

			int nSize = strlen(szFilename);
			char *pExt = (szFilename + nSize - 4);
			if(_stricmp(pExt, ".TXT") == 0 || _stricmp(pExt, ".PRN") == 0)
			{
				STARTUPINFO si;
				PROCESS_INFORMATION pi;

				ZeroMemory( &si, sizeof(si) );
				si.cb = sizeof(si);
				ZeroMemory( &pi, sizeof(pi) );

				sprintf(szBuf, "notepad.exe %S%s", m_csDestpath, szFilename);
				USES_CONVERSION;
				if( !CreateProcess( NULL, // No module name (use command line). 
					A2W(szBuf), // Command line. 
					NULL,             // Process handle not inheritable. 
					NULL,             // Thread handle not inheritable. 
					FALSE,            // Set handle inheritance to FALSE. 
					0,                // No creation flags. 
					NULL,             // Use parent's environment block. 
					NULL,             // Use parent's starting directory. 
					&si,              // Pointer to STARTUPINFO structure.
					&pi )             // Pointer to PROCESS_INFORMATION structure.
					) 
				{
					AfxMessageBox(_T("Error opening notepad.exe!"), MB_ICONSTOP);
				}
			}

			Sleep(500);
			PurgeComm(ck->m_hIDComDev, PURGE_RXCLEAR|PURGE_TXCLEAR);
			Sleep(500);

			goto snoop;
		}


		free(szFilename);
		free(szArea);

		if(nRet == -1 && strstr(ck->k.ker_last_error, "ERR: GETDATA") != NULL)	// QUIT
		{
			ck->Close();
			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// transfer aborted
			return FALSE;
		}

		ck->Close();
		delete ck; ck = NULL;
		::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done
	}
	else if(m_nType == 1)	// Ascii
	{
		do
		{
			nRet = ck->read_ker_char_();

			// fill the buffer with the received char.
			if(nRet != -1)
			{
				::SendMessage(ck->m_hWnd, WM_COM_PACKET, nRet, 0);
			}
		}
		while(nRet != -1);

		ck->Close();
		delete ck; ck = NULL;
	}
	else if(m_nType == 2)	// INET
	{
		W3Client w3;
		int nRet;
		m_nPos = 0;
		CString csReq;

		// retrieve the file from the net.
		if(w3.Connect(theApp.m_csWebserver))
		{
			if(m_nProdID != -1 && m_nCode != -1 && m_nSerial != -1 && m_nLevel != -1)
			{
				csReq.Format(_T("%sdm.php?pid=%d&lang=%s&serial=%d&level=%d&key=%d"),
					theApp.m_csWebpath, m_nProdID, m_csLang, m_nSerial, m_nLevel-1, m_nCode);
			}
			else
			{
				csReq.Format(_T("%sdm.php?pid=%d&lang=%s"),
					theApp.m_csWebpath, m_nProdID, m_csLang);
			}

			if(w3.Request(csReq))
			{
				if((nRet = w3.QueryResult()) == 200)
				{
					char buf[1024]="\0";

					int nLen = w3.QueryContentLength();
					SendMessage(m_hWnd, WM_COM_LENGTH, nLen, 0);


					CFile fh;
					if(fh.Open(m_csDestpath + m_csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
					{
						w3.Close();
						SendMessage(m_hWnd, WM_COM_DONE, 6, 0);	// unable to create file
						return FALSE;
					}
					int nSize = 0;
					while((nSize = w3.Response(reinterpret_cast<unsigned char *>(buf), 1024)) && m_bQuit == FALSE)
					{
						fh.Write(buf, nSize);
						m_nPos += nSize;

						SendMessage(m_hWnd, WM_COM_PACKET, m_nPos, 0);
					};

					fh.Close();
				}
			}
			w3.Close();

			if(nRet == 403)	// forbidden = not a valid license
			{
				SendMessage(m_hWnd, WM_COM_DONE, 8, 0);
				return FALSE;
			}
			else if(nRet != 200)	// we were unable to fetch the file.
			{
				SendMessage(m_hWnd, WM_COM_DONE, 7, 0);	// could not get file!
				return FALSE;
			}
			
			if(m_bQuit == FALSE)
				SendMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done
			else
				SendMessage(m_hWnd, WM_COM_DONE, 0, 0);	// transfer aborted

		}
	}
	else if(m_nType == 3)	// local
	{
		CFile fhSource, fhDest;
		char *pszBuf;

		if(!(fhSource.Open(theApp.m_csLocalpath + m_csFilename, CFile::modeRead|CFile::typeBinary, 0)))
		{
			SendMessage(m_hWnd, WM_COM_DONE, 7, 0);	// could not get file!
			return FALSE;
		}

		int nLen = fhSource.GetLength();
		pszBuf = (char*)malloc(fhSource.GetLength()+1);
		fhSource.Read(pszBuf, fhSource.GetLength());
		fhSource.Close();

		if(fhDest.Open(m_csDestpath + m_csFilename, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary, 0) == 0)
		{
			free(pszBuf);
			SendMessage(m_hWnd, WM_COM_DONE, 6, 0);	// unable to create file
			return FALSE;
		}

		fhDest.Write(pszBuf, nLen);
		fhDest.Close();

		free(pszBuf);
		SendMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done
	}
	else if(m_nType == 4)	// digitech copy
	{
		int nBytes=0, send=0;
		char cByte, szBuf[20];

		ck = new CKermit;
		ck->m_hWnd = m_hWnd;

		// open the serialport.
		int nRet = ck->Open(m_nComport, m_nBaud);
		m_bCanIQuit = TRUE;
		if(nRet != FALSE)
		{
			do
			{
				nRet = ck->read_ker_char_();

				// fill the buffer with the received char.
				if(nRet != -1)
				{
					cByte = (char)nRet;

					// is the cByte a digit, linefeed or a ; ?
 					if(((cByte >= '0' && cByte <= '9') || ((cByte == '\n' || cByte == '\r') && nBytes>3) || cByte == ';'))
					{
						szBuf[nBytes] = (char)nRet;
						cByte = 0;

						if(szBuf[nBytes]=='\n' || szBuf[nBytes]=='\r')
						{
							szBuf[nBytes]=48+(nBytes);	// put the size last in the packet
							szBuf[nBytes+1]='\0';

							send = GetIntFromString(szBuf);

							if(szBuf[0]==';')	// we have a copy going on
							{
								ParseData(6666);
								ParseData(send);
							}
							else
							{
								ParseData(send);
							}	

							nBytes = 0;
						}
						else nBytes++;
					} // digit?
				}
			}
			while(nRet != -1 && nRet != -2);

			if(nRet == -1 && strstr(ck->k.ker_last_error, "ERR: GETDATA") != NULL)	// QUIT
			{
				ck->Close();
				delete ck; ck = NULL;
				::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// transfer aborted
				return FALSE;
			}

			ck->Close();
			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done
		}
		else
		{
			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 5, 0);	// error: comport
			return FALSE;
		}
	}
	else if(m_nType == 5)	// digitech pc	(binary format)
	{
		int nBytes=0, send=0, nCheck=0;
		int nRet, nCount = 0, nSpecie=0, nAmount=0, nPackets=0;
		unsigned __int16 nDiam=0;
		BOOL bLast = FALSE, bFirst = FALSE;

		COPYDATASTRUCT cds;
		DD dd;
		dd.nDevice = 0;		// digitech
		dd.nId = 0;			// id


		ck = new CKermit;
		ck->m_hWnd = m_hWnd;

		// open the serialport.
		nRet = ck->Open(m_nComport, m_nBaud);
		m_bCanIQuit = TRUE;
		if(nRet != FALSE)
		{
			do
			{
				nRet = ck->read_ker_char_();

				// fill the buffer with the received char.
				if(nRet != -1)
				{
					nPackets++;

					if(bLast == TRUE)
					{
						// do we have the correct checksum?
						bLast = FALSE;
						bFirst = FALSE;
						nPackets = 0;
						nCount = 0;
						nCheck = 0;
						nAmount = 0;
						break;
					}
					else
						nCheck ^= nRet;

					if(nPackets == 1 && nRet == 2)	// STX
					{
						nCount = 0;
					}
					else if(nPackets == 2 && nRet == 68)	// Data
					{
						bFirst = TRUE;
					}
					else if(nPackets == nAmount+5 && nRet == 3)	// ETX
					{
						bLast = TRUE;
					}
					else if(nPackets == 3 || nPackets == 4)	// size 2 bytes
					{
							if(nPackets == 3)
							{
								nAmount = _lrotl(nRet, 8);
							}
							else if(nPackets == 4)
							{
								nAmount += nRet;

								// send the packet to the main window
								dd.nData1 = nAmount / 2;	// total number of diameters to send
								dd.nData2 = 0;
								dd.nData3 = 0;
								dd.nData4 = 0;
								dd.nData5 = 0;
								dd.nCommmand = 1;	// digicopy

								cds.dwData = 1;				// function identifier
								cds.cbData = sizeof(dd);	// size of data
								cds.lpData = &dd;           // data structure
								::SendMessage(m_hWnd, WM_COPYDATA, (WPARAM)(HWND)0, (LPARAM)(LPVOID)&cds);
							}
					}
					else
					{
						if(nCount == 0)
						{
							nCount++;
							nDiam = _lrotl(nRet, 8);
							nSpecie = ( nRet &= 240 );
							nSpecie = _lrotr(nSpecie, 4);	// get the specie
						}
						else
						{
							nCount = 0;
							nDiam += nRet;
							nDiam = ( nDiam &= 4095 );	// get the diameter


							dd.nCommmand = 0;		//
							dd.nNumVars = 7;		// number of variables
							dd.nData1 = nSpecie;	// specie
							dd.nData2 = nDiam;		// diameter
							dd.nData3 = 0;			// id
							dd.nData4 = 0;
							dd.nData5 = 0;

							cds.dwData = 1;				// function identifier
							cds.cbData = sizeof(dd);	// size of data
							cds.lpData = &dd;           // data structure
							::SendMessage(m_hWnd, WM_COPYDATA, (WPARAM)(HWND)0, (LPARAM)(LPVOID)&cds);
						}
					}
				}
			}
			while(nRet != -1 && nRet != -2);

			if(nRet == -1 && strstr(ck->k.ker_last_error, "ERR: GETDATA") != NULL)	// QUIT
			{
				ck->Close();
				delete ck; ck = NULL;
				::PostMessage(m_hWnd, WM_COM_DONE, 0, 0);	// transfer aborted
				return FALSE;
			}

			ck->Close();
			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 1, 0);	// transfer done
		}
		else
		{
			delete ck; ck = NULL;
			::PostMessage(m_hWnd, WM_COM_DONE, 5, 0);	// error: comport
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CComThread::ParseData(int nData)
{
	CString csData;
	int treetype=-1,diameter=-1,id=-1, nSum=0;
    int pack_len, amount_max=0, amount=0;
	pack_len = nData%10;

	if(pack_len == 4)	// (   1 218   ) + LENGTH
	{
		nData = nData/10;
		treetype = nData/1000;
		diameter = nData%1000;
	}
	else if(pack_len == 5)	// (   2 536 1 ) + LENGTH
	{
		nData = nData/10;
		treetype = nData/10000;
		diameter = (nData%10000)/10;
		id = nData%10;
	}
	else if(pack_len == 6)	// ( 035 031   ) + LENGTH
	{
		nData = nData/10;
		treetype = nData/1000;
		diameter = nData%1000;
	}
	else if(pack_len == 7)	// ( 103 457 4 ) + LENGTH
	{
		nData = nData/10;
		treetype = nData/10000;
		diameter = (nData%10000)/10;
		id = nData%10;
	}

	if(m_bDC == TRUE) // nData contains how much data that will follow...
	{
		amount_max = nData%1000;
		amount = 0;
		nData = 0;
		m_bDC = FALSE;
	}
	else if(nData == 666)	// we've received a copy
	{
		m_bDC = TRUE;
	}

	if(m_bDC == FALSE)
	{
		COPYDATASTRUCT cds;
		DD dd;

		dd.nDevice = 0;		// digitech
		dd.nId = id;		// id

		if(amount_max != 0)
		{
			dd.nData1 = amount_max;	// total number of diameters to send
			dd.nData2 = diameter;	//
			dd.nData3 = id;
			dd.nData4 = 0;
			dd.nData5 = 0;

			amount++;

			dd.nCommmand = 1;	// digicopy
		}
		else
		{
			if(treetype != -1)
				nSum += 1;
			if(diameter != -1)
				nSum += 2;
			if(id != -1)
				nSum += 4;

			dd.nCommmand = 0;		//
			dd.nNumVars = nSum;		// number of variables
			dd.nData1 = treetype;	//
			dd.nData2 = diameter;	//
			dd.nData3 = id;
			dd.nData4 = 0;
			dd.nData5 = 0;
		}

		cds.dwData = 1;				// function identifier
		cds.cbData = sizeof(dd);	// size of data
		cds.lpData = &dd;           // data structure

		::SendMessage(m_hWnd, WM_COPYDATA, (WPARAM)(HWND)0, (LPARAM)(LPVOID)&cds);
	}

	return 0;
}

/***********************************************************
 Turn an string into an integer.
 ***********************************************************/
unsigned int CComThread::GetIntFromString(char *buf)
{
  unsigned int sum=0,ch,loop=0;

	do
	{
		ch=buf[loop]-'0';
		sum*=10;
	    sum+=ch;
	    loop++;
	}
	while(buf[loop]!='\0');

  return sum;
}
