#pragma once

#include "ComThread.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CSenddataFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CSenddataFrame)

	BOOL m_bOnce;
public:
	CSenddataFrame();

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSenddataFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSenddataFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CSenddataFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CSenddataDlg form view
class CSenddataDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CSenddataDlg)

public:
	CString m_csFilename;
	CString m_csFilenameShort;

	CString m_csOptions;	//added by magnus 100811 

protected:
	CSenddataDlg();   // standard constructor);
	virtual ~CSenddataDlg();

	CMDISampleSuiteDoc* pDoc;
	CComThread* m_pCComThread;
	CProgressCtrl m_cProgress;
	BOOL m_bFile;
	int m_nLength;

	// dialog text
	CString m_csFile;
	CString m_csComport;
	CString m_csBaudrate;
	CString m_csLicense;
	CString m_csStatus;

	bool m_bBLA;
	bool m_bFOO;
	bool m_bFirst;

public:
	enum { IDD = IDD_SENDDATA };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	BOOL m_bQuit;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	LRESULT OnPacket(WPARAM wParm, LPARAM lParm);
	LRESULT OnLength(WPARAM wParm, LPARAM lParm);
	LRESULT OnDone(WPARAM wParm, LPARAM lParm);
	LRESULT OnFile(WPARAM wParm, LPARAM lParm);
	//{{AFX_MSG(CListCtrlDlg)
	//}}AFX_MSG
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedButtonTransfer();
	afx_msg void OnBnClickedButtonFilechoose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void SendFile(CString csFilename, CString csOptions = _T(""));
protected:
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
};
