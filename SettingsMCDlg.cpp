// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SettingsMCDlg.h"
#include ".\settingsmcdlg.h"
#include "CustomItems.h"

IMPLEMENT_DYNCREATE(CSettingsMCFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSettingsMCFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSettingsMCFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsMCFrame construction/destruction

XTPDockingPanePaintTheme CSettingsMCFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CSettingsMCFrame::CSettingsMCFrame()
{
	m_bOnce = TRUE;
}

CSettingsMCFrame::~CSettingsMCFrame()
{
}

BOOL CSettingsMCFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsMCFrame diagnostics

#ifdef _DEBUG
void CSettingsMCFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSettingsMCFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSettingsMCFrame message handlers

int CSettingsMCFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CSettingsMCFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsMC"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CSettingsMCFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

LRESULT CSettingsMCFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CSettingsMCFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	theApp.SaveRegSettings();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsMC"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

void CSettingsMCFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CSettingsMCDlg

IMPLEMENT_DYNCREATE(CSettingsMCDlg, CXTResizeFormView)

CSettingsMCDlg::CSettingsMCDlg()
	: CXTResizeFormView(CSettingsMCDlg::IDD)
{
}

CSettingsMCDlg::~CSettingsMCDlg()
{
}

void CSettingsMCDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PLACEHOLDER, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsMCDlg, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_HELP, OnCommandHelp)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
END_MESSAGE_MAP()


// CSettingsMCDlg diagnostics

#ifdef _DEBUG
void CSettingsMCDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSettingsMCDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CSettingsMCDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

LRESULT CSettingsMCDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMSETTINGS_MANTAX_COMPUTER);
	return 0;
}

void CSettingsMCDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	// get available comports and fill the combobox
	theApp.GetComPorts();

	// get the size of the placeholder, this will be used when creating the grid.
	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );

	CString csBuf;

	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

		// Serial category.
		CXTPPropertyGridItem* pSerial = m_wndPropertyGrid.AddCategory(g_pXML->str(1038));

		// add child items to category.
		m_pItemBaudrate = pSerial->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1039), theApp.m_csMC_Baudrate));
		CXTPPropertyGridItemConstraints* pList = m_pItemBaudrate->GetConstraints();
		pList->AddConstraint(_T("1200"));
		pList->AddConstraint(_T("2400"));
		pList->AddConstraint(_T("4800"));
		pList->AddConstraint(_T("9600"));
		pList->AddConstraint(_T("19200"));
		pList->AddConstraint(_T("38400"));
		pList->AddConstraint(_T("57600"));
		pList->AddConstraint(_T("115200"));
		m_pItemBaudrate->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);

		m_pItemComport = pSerial->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1040), theApp.m_csMC_Comport));
		pList = m_pItemComport->GetConstraints();
		PORTS port;

		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.nDP == 0)
			{
				pList->AddConstraint(port.csPort);
			}
			else
			{
				csBuf.Format(_T("%s (DP)"), port.csPort);
				pList->AddConstraint(csBuf);
			}
		}
		m_pItemComport->SetFlags(xtpGridItemHasComboButton /*| xtpGridItemHasEdit*/);

		pSerial->Expand();
		m_pItemBaudrate->Select();


		// create global settings category.
		CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(g_pXML->str(1041));

		// add child items to category.
		m_pItemPath1 = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1042), theApp.m_csMC_Filepath));
		m_pItemPath1->SetDescription(g_pXML->str(1043));

		m_pItemPath2 = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1044), theApp.m_csMC_Downloadpath));
		m_pItemPath2->SetDescription(g_pXML->str(1045));

		pPaths->Expand();
	}

	// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}


// CSettingsMCDlg message handlers

void CSettingsMCDlg::OnDestroy()
{
	UpdateData(TRUE);

	CXTResizeFormView::OnDestroy();
}

void CSettingsMCDlg::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

LRESULT CSettingsMCDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_SORTORDER_CHANGED:
		{
		}
		break;

		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemComport)
			{
				theApp.m_csMC_Comport = m_pItemComport->GetValue();
			}
			else if(pItem == m_pItemBaudrate)
			{
				theApp.m_csMC_Baudrate = m_pItemBaudrate->GetValue();
			}
			else if(pItem == m_pItemPath1)
			{
				theApp.m_csMC_Filepath = m_pItemPath1->GetValue();
				if(theApp.m_csMC_Filepath.GetAt(theApp.m_csMC_Filepath.GetLength()-1) != '\\')
					theApp.m_csMC_Filepath += _T("\\");
			}
			else if(pItem == m_pItemPath2)
			{
				theApp.m_csMC_Downloadpath = m_pItemPath2->GetValue();
				if(theApp.m_csMC_Downloadpath.GetAt(theApp.m_csMC_Downloadpath.GetLength()-1) != '\\')
					theApp.m_csMC_Downloadpath += _T("\\");
			}
		}
		break;

		case XTP_PGN_SELECTION_CHANGED:
		{
		}
		break;
	}

	return FALSE;
}
