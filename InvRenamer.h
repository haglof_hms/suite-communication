#pragma once

#include "Resource.h"

// CInvRenamer dialog

class CInvRenamer : public CDialog
{
	DECLARE_DYNAMIC(CInvRenamer)

public:
	CInvRenamer(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInvRenamer();

// Dialog Data
	enum { IDD = IDD_INV };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void OnOK();

	BOOL m_bSub;

	DECLARE_MESSAGE_MAP()
public:
	CString m_csOrigName;
	CString m_csNewName;
	virtual BOOL OnInitDialog();
	CString m_csSubOrig;
	CString m_csSubNew;
};
