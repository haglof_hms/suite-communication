#include "stdafx.h"
#include "ProgramsForms.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/*=========================================================================================*/
/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame

IMPLEMENT_DYNCREATE(CProgramsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CProgramsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProgramsFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame construction/destruction

CProgramsFrame::CProgramsFrame()
{
}

CProgramsFrame::~CProgramsFrame()
{
}

BOOL CProgramsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame diagnostics

#ifdef _DEBUG
void CProgramsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CProgramsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame message handlers

int CProgramsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = _T("C:\\Program\\HMS\\HMSToolBarIcones32.dll"); //getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					hResModule = LoadLibrary(_T(sTBResFN));
					if (hResModule)
					{
						pCtrl = p->GetAt(0);
//						pCtrl->SetTooltip(sToolTip1);
						hIcon = LoadIcon(hResModule, _T("QUEST"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(1);
//						pCtrl->SetTooltip(sToolTip2);
						hIcon = LoadIcon(hResModule, _T("QUEST"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(2);
//						pCtrl->SetTooltip(sToolTip3);
						hIcon = LoadIcon(hResModule, _T("QUEST"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						FreeLibrary(hResModule);
					}	// if (hResModule)

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	CenterWindow();
	UpdateWindow();

	return 0;
}

void CProgramsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

LRESULT CProgramsFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		return TRUE;

	}
	return FALSE;
}
