// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SettingsDDlg.h"
#include "CustomItems.h"
#include <setupapi.h>

#ifndef GUID_DEVINTERFACE_COMPORT
  DEFINE_GUID(GUID_DEVINTERFACE_COMPORT, 0x86E0D1E0L, 0x8089, 0x11D0, 0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73);
#endif


typedef HKEY (__stdcall SETUPDIOPENDEVREGKEY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, DWORD, DWORD, REGSAM);
typedef BOOL (__stdcall SETUPDIDESTROYDEVICEINFOLIST)(HDEVINFO);
typedef BOOL (__stdcall SETUPDIENUMDEVICEINFO)(HDEVINFO, DWORD, PSP_DEVINFO_DATA);
typedef HDEVINFO (__stdcall SETUPDIGETCLASSDEVS)(LPGUID, LPCTSTR, HWND, DWORD);
typedef BOOL (__stdcall SETUPDIGETDEVICEREGISTRYPROPERTY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, PDWORD, PBYTE, DWORD, PDWORD);


IMPLEMENT_DYNCREATE(CSettingsDFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSettingsDFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSettingsDFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDFrame construction/destruction

XTPDockingPanePaintTheme CSettingsDFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CSettingsDFrame::CSettingsDFrame()
{
	m_bOnce = TRUE;
}

CSettingsDFrame::~CSettingsDFrame()
{
}

BOOL CSettingsDFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsDFrame diagnostics

#ifdef _DEBUG
void CSettingsDFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSettingsDFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSettingsDFrame message handlers

int CSettingsDFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CSettingsDFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsD"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CSettingsDFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

LRESULT CSettingsDFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CSettingsDFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	theApp.SaveRegSettings();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\SettingsD"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

void CSettingsDFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CSettingsDDlg

IMPLEMENT_DYNCREATE(CSettingsDDlg, CXTResizeFormView)

CSettingsDDlg::CSettingsDDlg()
	: CXTResizeFormView(CSettingsDDlg::IDD)
{
}

CSettingsDDlg::~CSettingsDDlg()
{
}

void CSettingsDDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PLACEHOLDER, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsDDlg, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()


// CSettingsDDlg diagnostics

#ifdef _DEBUG
void CSettingsDDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSettingsDDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CSettingsDDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

LRESULT CSettingsDDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMSETTINGS_DIGITECH);
	return 0;
}

void CSettingsDDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	// get available comports and fill the combobox
	theApp.GetComPorts();

	// get the size of the placeholder, this will be used when creating the grid.
	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );

	CString csBuf;

	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

		// Serial category.
		CXTPPropertyGridItem* pSerial = m_wndPropertyGrid.AddCategory(g_pXML->str(1038));

		// add child items to category.
		m_pItemComport = pSerial->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1040), theApp.m_csD_Comport));
		CXTPPropertyGridItemConstraints* pList = m_pItemComport->GetConstraints();
		PORTS port;

		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.nDP == 0)
			{
				pList->AddConstraint(port.csPort);
			}
			else
			{
				csBuf.Format(_T("%s (DP)"), port.csPort);
				pList->AddConstraint(csBuf);
			}
		}
		m_pItemComport->SetFlags(xtpGridItemHasComboButton);

		pSerial->Expand();


		// Conversion category
		CXTPPropertyGridItem* pConversion = m_wndPropertyGrid.AddCategory(g_pXML->str(1046));

		// add child items to category.
		m_pItemBool1 = (CXTPPropertyGridItemBool*)pConversion->AddChildItem(new CMyPropGridItemBool(g_pXML->str(900), g_pXML->str(901), g_pXML->str(1047), theApp.m_bD_Del0));
		m_pItemBool2 = (CXTPPropertyGridItemBool*)pConversion->AddChildItem(new CMyPropGridItemBool(g_pXML->str(900), g_pXML->str(901), g_pXML->str(1048), theApp.m_bD_Two0));
		m_pItemBool3 = (CXTPPropertyGridItemBool*)pConversion->AddChildItem(new CMyPropGridItemBool(g_pXML->str(900), g_pXML->str(901), g_pXML->str(1049), theApp.m_bD_Three0));
		m_pItemBool4 = (CXTPPropertyGridItemBool*)pConversion->AddChildItem(new CMyPropGridItemBool(g_pXML->str(900), g_pXML->str(901), g_pXML->str(1050), theApp.m_bD_Height8));

		if(theApp.m_nD_OutFormat == 1)
			csBuf = g_pXML->str(1081);
		else if(theApp.m_nD_OutFormat == 2)
			csBuf = g_pXML->str(1082);
		else if(theApp.m_nD_OutFormat == 3)
			csBuf = g_pXML->str(1083);
		else
			csBuf = g_pXML->str(1080);

		m_pOutput = pConversion->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1079), csBuf));
		pList = m_pOutput->GetConstraints();
		pList->AddConstraint(g_pXML->str(1080));
		pList->AddConstraint(g_pXML->str(1081));
		pList->AddConstraint(g_pXML->str(1082));
		pList->AddConstraint(g_pXML->str(1083));
		m_pOutput->SetFlags(xtpGridItemHasComboButton | xtpGridItemHasEdit);

		pConversion->Expand();


		// Species category
		CXTPPropertyGridItem* pSpecies = m_wndPropertyGrid.AddCategory(g_pXML->str(1084));
		pSpecies->SetDescription(g_pXML->str(1085));

		m_pSpc1 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1086), theApp.m_caD_Species.GetAt(0)));
		m_pSpc2 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1087), theApp.m_caD_Species.GetAt(1)));
		m_pSpc3 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1088), theApp.m_caD_Species.GetAt(2)));
		m_pSpc4 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1089), theApp.m_caD_Species.GetAt(3)));
		m_pSpc5 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1090), theApp.m_caD_Species.GetAt(4)));
		m_pSpc6 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1091), theApp.m_caD_Species.GetAt(5)));
		m_pSpc7 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1092), theApp.m_caD_Species.GetAt(6)));
		m_pSpc8 = pSpecies->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1093), theApp.m_caD_Species.GetAt(7)));
		if(theApp.m_bD_Height8 == TRUE) m_pSpc8->SetReadOnly();

		pSpecies->Expand();


		// Settings category.
		CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(g_pXML->str(1041));

		// add child items to category.
		m_pItemPath2 = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1044), theApp.m_csD_Downloadpath));
		m_pItemPath2->SetDescription(g_pXML->str(1045));

		pPaths->Expand();
	}

	// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}


// CSettingsDDlg message handlers

void CSettingsDDlg::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

void CSettingsDDlg::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

LRESULT CSettingsDDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_SORTORDER_CHANGED:
		{
		}
		break;

		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemBool4)
			{
				CXTPPropertyGridItemConstraints* pList = m_pItemBool4->GetConstraints();
				int nRet = pList->GetCurrent();

				// 8 = height?
				if(nRet == 0)	// yes
				{
					m_pSpc8->SetReadOnly(TRUE);
				}
				else	// no
				{
					m_pSpc8->SetReadOnly(FALSE);
				}

				theApp.m_bD_Height8 = m_pItemBool4->GetBool();
			}
			else if(pItem == m_pItemComport)
			{
				theApp.m_csD_Comport = m_pItemComport->GetValue();
			}
			else if(pItem == m_pItemBool1)
			{
				theApp.m_bD_Del0 = m_pItemBool1->GetBool();
			}
			else if(pItem == m_pItemBool2)
			{
				theApp.m_bD_Two0 = m_pItemBool2->GetBool();
			}
			else if(pItem == m_pItemBool3)
			{
				theApp.m_bD_Three0 = m_pItemBool3->GetBool();
			}
			else if(pItem == m_pItemPath2)
			{
				theApp.m_csD_Downloadpath = m_pItemPath2->GetValue();
				if(theApp.m_csD_Downloadpath.GetAt(theApp.m_csD_Downloadpath.GetLength()-1) != '\\')
					theApp.m_csD_Downloadpath += _T("\\");
			}
			else if(pItem == m_pOutput)
			{
				CXTPPropertyGridItemConstraints* pList = m_pOutput->GetConstraints();
				theApp.m_nD_OutFormat = pList->GetCurrent();
				if(theApp.m_nD_OutFormat < 0 || theApp.m_nD_OutFormat > 3) theApp.m_nD_OutFormat = 0;
			}
			else if(pItem == m_pSpc1)
			{
				theApp.m_caD_Species.SetAt(0, m_pSpc1->GetValue());
			}
			else if(pItem == m_pSpc2)
			{
				theApp.m_caD_Species.SetAt(1, m_pSpc2->GetValue());
			}
			else if(pItem == m_pSpc3)
			{
				theApp.m_caD_Species.SetAt(2, m_pSpc3->GetValue());
			}
			else if(pItem == m_pSpc4)
			{
				theApp.m_caD_Species.SetAt(3, m_pSpc4->GetValue());
			}
			else if(pItem == m_pSpc5)
			{
				theApp.m_caD_Species.SetAt(4, m_pSpc5->GetValue());
			}
			else if(pItem == m_pSpc6)
			{
				theApp.m_caD_Species.SetAt(5, m_pSpc6->GetValue());
			}
			else if(pItem == m_pSpc7)
			{
				theApp.m_caD_Species.SetAt(6, m_pSpc7->GetValue());
			}
			else if(pItem == m_pSpc8)
			{
				theApp.m_caD_Species.SetAt(7, m_pSpc8->GetValue());
			}
		}
		break;

		case XTP_PGN_SELECTION_CHANGED:
		{
		}
		break;
	}

	return FALSE;
}
