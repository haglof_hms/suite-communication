#pragma once


// CPrgDlg dialog

class CPrgDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrgDlg)

public:
	CPrgDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPrgDlg();

// Dialog Data
	enum { IDD = IDD_PROGRAMSINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
