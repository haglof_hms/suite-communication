#pragma once

#include "SampleSuiteForms.h"
//#include "c:\program\codejock software\mfc\xtreme toolkitpro v9.81\source\reportcontrol\xtpreportcontrol.h"
#include <reportcontrol\xtpreportcontrol.h>

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CLicensesFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CLicensesFrame)

	BOOL m_bOnce;
public:
	CLicensesFrame();

private:
	int m_nSelProgram;

// Attributes
public:
	// Toolbar
	CXTPToolBar m_wndToolBar;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicensesFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLicensesFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CLicensesFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
protected:
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CLicensesDlg form view

class CLicensesDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CLicensesDlg)

protected:
	CLicensesDlg();           // protected constructor used by dynamic creation
	virtual ~CLicensesDlg();

	CString m_csProgram;	// name of the program
	CString m_csVersion;	// version of the program
	int m_nSelProgram;		// currently selected program
	int m_nSelLang;			// currently selected language
	int m_nID;				// program-id

	CMDISampleSuiteDoc* pDoc;
	
	BOOL m_bModified;

public:
// Dialog Data
	//{{AFX_DATA(CTreeViewDlg)
	enum { IDD = IDD_LICENSES };
	//}}AFX_DATA

	CXTPReportControl* m_pwndReport;
	CXTPReportControl m_wndReport;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
// Generated message map functions
	//{{AFX_MSG(CLicensesFrame)
	afx_msg void OnInitialUpdate();
	afx_msg void OnBnClickedButtonLicenseAddkey();
	afx_msg void OnBnClickedButtonLicenseDelkey();
	afx_msg void OnBnClickedButtonAttach();
	afx_msg void OnBnClickedSend();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRowSelect(NMHDR* nm, LRESULT* lr);
	afx_msg void SaveKeys();
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnNavigate(int nID);
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
private:
	CString m_csDesc;
};
