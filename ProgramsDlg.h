#pragma once
#include "Resource.h"
#include "afxcmn.h"
#include "xmlParser.h"
#include "w3c.h"
#include "afxwin.h"
#include "SampleSuiteForms.h"
#include "MyLocale.h"

//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d



class CProgramsFrame;
class CContactsReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CContactsReportFilterEditControl)
public:
	CContactsReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

	CProgramsFrame* pWnd;
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};


#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CProgramsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CProgramsFrame)

	BOOL m_bOnce;
	BOOL m_bEnableTBBTNFilterOff;
public:
	CProgramsFrame();

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

	// Toolbar
	CXTPToolBar m_wndToolBar;
	CStatusBar  m_wndStatusBar;

	CDialogBar m_wndFieldChooser;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

	void setFilterWndCap(LPCTSTR cap)
	{
		m_wndFilterEdit.SetWindowText(cap);
	}


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CProgramsFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


class CProgramsDlg : public CXTPReportView
{
protected: // create from serialization only
	CProgramsDlg();
	DECLARE_DYNCREATE(CProgramsDlg)

// Attributes
public:
	CMDISampleSuiteDoc* pDoc;

	CXTPReportSubListControl m_wndSubList;
//	CXTPReportFilterEditControl m_wndFilterEdit;
	CContactsReportFilterEditControl m_wndFilterEdit;

	CImageList m_ilIcons;

	BOOL m_bAutomaticFormating;
	BOOL m_bMultilineSample;

private:
	int m_nVersionXML;
	CString m_csDateXML;
	CLocale m_cLocale;



// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsDlg)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
public:
	int SaveXML();
protected:
	// XML
	int GetLocalXML();
	int GetWebXML();
	int ParseXML();

	void AddSampleRecords();
	void LoadReportState();
	void SaveReportState();

	//{{AFX_MSG(CProgramsDlg)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedLicense();

	afx_msg void OnGridHorizontal(UINT);
	afx_msg void OnUpdateGridHorizontal(CCmdUI* pCmdUI);

	afx_msg void OnGridVertical(UINT);
	afx_msg void OnUpdateGridVertical(CCmdUI* pCmdUI);

	afx_msg void OnReportSelChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNavigate(int nID);
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
};

