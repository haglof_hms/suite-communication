#pragma once
#include "afxwin.h"
#include "SampleSuiteForms.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CSettingsDPFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CSettingsDPFrame)

	BOOL m_bOnce;
public:
	CSettingsDPFrame();
	static XTPDockingPanePaintTheme m_themeCurrent;

private:
	int m_nSelProgram;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsDPFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSettingsDPFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CSettingsDPFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
protected:
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};

// CSettingsDPDlg form view

typedef struct _tagLANGS
{
	CString csName;
	CString csAbb;
} LANGS;

class CSettingsDPDlg : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSettingsDPDlg)

protected:
	CSettingsDPDlg();           // protected constructor used by dynamic creation
	virtual ~CSettingsDPDlg();

//	CString m_csComport;
	int m_nBaudrate;
	CMDISampleSuiteDoc* pDoc;

	CXTPPropertyGridItem* m_pItemBaudrate;
	CXTPPropertyGridItem* m_pItemComport;
	CXTPPropertyGridItem* m_pItemPath1;
	CXTPPropertyGridItem* m_pItemPath2;
	CXTPPropertyGridItem* m_pLanguage;

//	CArray<CString, CString> m_caLangs;
	CArray<LANGS, LANGS> m_caLangs;

public:
	enum { IDD = IDD_SETTINGSDP };
	//{{AFX_DATA(CPropertyGridDlg)
	CStatic m_wndPlaceHolder;
	//}}AFX_DATA
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

protected:
	CXTPPropertyGrid m_wndPropertyGrid;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);
	BOOL IsNumeric(LPCTSTR pszString, BOOL bIgnoreColon);
	DECLARE_MESSAGE_MAP()
	int GetLangsFromXML();
public:
	afx_msg LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
	CString m_csBaudrate;
	CString m_csComport;
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
};
