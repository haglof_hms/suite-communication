// TransferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "RecvdataDDlg.h"
#include "Usefull.h"
#include "DigiExportDlg.h"

IMPLEMENT_DYNCREATE(CRecvdataDFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CRecvdataDFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CRecvdataDFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecvdataDFrame construction/destruction

CRecvdataDFrame::CRecvdataDFrame()
{
	m_bOnce = TRUE;
}

CRecvdataDFrame::~CRecvdataDFrame()
{
}

BOOL CRecvdataDFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CRecvdataDFrame diagnostics

#ifdef _DEBUG
void CRecvdataDFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CRecvdataDFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRecvdataDFrame message handlers

int CRecvdataDFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

LRESULT CRecvdataDFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
			((CRecvdataDDlg*)GetActiveView())->OnPreview();
		break;

		case ID_SAVE_ITEM:
			((CRecvdataDDlg*)GetActiveView())->OnSave();
		break;

		case ID_DELETE_ITEM:
			((CRecvdataDDlg*)GetActiveView())->OnDelete();
		break;
	};

	return 0;
}


void CRecvdataDFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\RecvdataD"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CRecvdataDFrame::OnClose()
{
	CRecvdataDDlg* pView = ((CRecvdataDDlg*)GetActiveView());
	if(pView != 0)
	{
		if( ((CRecvdataDDlg*)GetActiveView())->m_bDone == TRUE)
		{
			int nRet = MessageBox(g_pXML->str(1113), g_pXML->str(IDR_RECVDATAD), MB_YESNOCANCEL|MB_ICONQUESTION);
			if(nRet == IDCANCEL) return;	
			else if(nRet == IDYES)
			{
				nRet = pView->OnSave(TRUE);
			}
		}
	}

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CRecvdataDFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\RecvdataD"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CRecvdataDFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}






// CRecvdataDDlg

IMPLEMENT_DYNCREATE(CRecvdataDDlg, CXTResizeFormView)

CRecvdataDDlg::CRecvdataDDlg()
	: CXTResizeFormView(CRecvdataDDlg::IDD)
{
	//{{AFX_DATA_INIT(CRecvdataDDlg)
	//}}AFX_DATA_INIT

	m_csFilename = _T("");
	m_csFilenameShort = _T("");

	// dialog text
	m_nStand = 0;
	m_nPlots = 0;
	m_nTrees = 0;
	m_csComport = _T("");
	m_csBaudrate = _T("");
	m_csStatus = _T("");

	m_nZeros = 0;
	m_nCopyAmount = 0;
	m_nCopyReceived = 0;
	m_nTimer = 0;

	m_bReport = FALSE;	// report?

	m_bChanged = FALSE;

	m_bQuit = FALSE;
}

CRecvdataDDlg::~CRecvdataDDlg()
{
}

void CRecvdataDDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PROGRESS_TRANSFER, m_cProgress);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_STAND2, m_nStand);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_PLOTS2, m_nPlots);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_TREES2, m_nTrees);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_COMPORT2, m_csComport);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_BAUDRATE2, m_csBaudrate);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_STATUS2, m_csStatus);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CRecvdataDDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramsView)
	ON_BN_CLICKED(IDC_BUTTON_TRANSFER, OnBnClickedButtonTransfer)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_COM_PACKET, OnPacket)
	ON_MESSAGE(WM_COM_LENGTH, OnLength)
	ON_MESSAGE(WM_COM_DONE, OnDone)
	ON_MESSAGE(WM_COM_FILE, OnFile)
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_TIMER()
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()

LRESULT CRecvdataDDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMRECEIVE_FILE);
	return 0;
}

// CRecvdataDDlg diagnostics
#ifdef _DEBUG
void CRecvdataDDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CRecvdataDDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CRecvdataDDlg message handlers
BOOL CRecvdataDDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRecvdataDDlg::OnDestroy()
{
	// Abort any filetransfer and close the dialog.
	if(m_bQuit == FALSE && m_pCComThread != NULL)
	{
		m_pCComThread->Abort();
		m_pCComThread = NULL;
	}

	CView::OnDestroy();
}

void CRecvdataDDlg::OnInitialUpdate()
{
	m_bDone = FALSE;	// done with the wizard
	m_bQuit = FALSE;
	m_pCComThread = NULL;


	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();
	if(pDoc == NULL) return;

	if(pDoc->m_nType == 0)	// DigitechPro
	{
		m_csFilename = theApp.m_csDP_Downloadpath;	// digitechpro
		m_csComport = theApp.m_csDP_Comport;
	}
	else if(pDoc->m_nType == 1)	// Mantax computer
	{
		m_csFilename = theApp.m_csMC_Downloadpath;	// mantax computer
		m_csComport = theApp.m_csMC_Comport;
	}
	else if(pDoc->m_nType == 2)	// Digitech
	{
		m_csFilename = theApp.m_csD_Downloadpath;	// digitech
		m_csComport = theApp.m_csD_Comport;
	}

	m_csBaudrate = _T("9600");
	m_csStatus.Format(_T("%s"), g_pXML->str(1029));

	// clear the arrays
	theApp.m_caStands.RemoveAll();
	theApp.m_caTrees.RemoveAll();

	CXTResizeFormView::OnInitialUpdate();

	// change caption on items
	SetDlgItemText(IDC_BUTTON_TRANSFER, g_pXML->str(1023));
	SetDlgItemText(IDC_STATIC_TRANSFER_STAND, g_pXML->str(1073));
	SetDlgItemText(IDC_STATIC_TRANSFER_PLOTS, g_pXML->str(1074));
	SetDlgItemText(IDC_STATIC_TRANSFER_TREES, g_pXML->str(1075));
	SetDlgItemText(IDC_STATIC_TRANSFER_COMPORT, g_pXML->str(1068));
	SetDlgItemText(IDC_STATIC_TRANSFER_BAUDRATE, g_pXML->str(1069));
	SetDlgItemText(IDC_STATIC_TRANSFER_LICENSE, g_pXML->str(1070));
	SetDlgItemText(IDC_STATIC_TRANSFER_STATUS, g_pXML->str(1071));

	m_cProgress.SetRange32(0, 1);
	m_cProgress.SetPos(0);

	ResizeParentToFit();

	int pos = 0;
	CString csBuf = m_csComport.Tokenize(_T("COM:"), pos);
 	OpenComThread();
}

void CRecvdataDDlg::OnSetFocus(CWnd* pOldWnd)
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);

	if(m_bDone == FALSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
	}
	
	if(m_bReport == FALSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, TRUE);
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTResizeFormView::OnSetFocus(pOldWnd);
}

// user has clicked the Transfer!-button
void CRecvdataDDlg::OnBnClickedButtonTransfer()
{
	if(m_bQuit == TRUE) return;
	m_bQuit = TRUE;

	// Abort the filetransfer and close the dialog.
	m_pCComThread->Abort();
	m_pCComThread = NULL;

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);
	GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

// we have recieved a packet.
LRESULT CRecvdataDDlg::OnPacket(WPARAM wParm, LPARAM lParm)
{
	GetParentFrame()->BringWindowToTop();
	SetFocus();

	UpdateData(FALSE);
	
	return 0;
}

// we have recieved a length.
LRESULT CRecvdataDDlg::OnLength(WPARAM wParm, LPARAM lParm)
{
	m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1030), (int)wParm);

	m_cProgress.SetRange32(0, (int)wParm);
	m_cProgress.SetPos(0);

	UpdateData(FALSE);

	return 0;
}

// the transfer is done
LRESULT CRecvdataDDlg::OnDone(WPARAM wParm, LPARAM lParm)
{
	BOOL bError = FALSE;

	// transfer ended, tell why.
	switch(wParm)
	{
		case 0:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1066));
			break;
		case 1:
			break;
		case 2:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1032));
			bError = TRUE;
			break;
		case 3:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1033));
			bError = TRUE;
			break;
		case 4:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1034));
			bError = TRUE;
			break;
		case 5:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1035));
			bError = TRUE;
			break;
		case 6:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1036));
			bError = TRUE;
			break;
		case 7:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1037));
			bError = TRUE;
			break;
	}

	if(bError == TRUE && m_bQuit == FALSE)
	{
		return 0;
	}

	m_pCComThread = NULL;
	if(m_pCComThread == NULL) OpenComThread();


	UpdateData(FALSE);

	return 0;
}

// we got the filename we are receiving
LRESULT CRecvdataDDlg::OnFile(WPARAM wParm, LPARAM lParm)
{
	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(TRUE);

	m_csStatus.Format(_T("%s (0 Bytes)"), g_pXML->str(1030));

	UpdateData(FALSE);

	return 0;
}

BOOL CRecvdataDDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	DD* dd = (DD*)pCopyDataStruct->lpData;
	STAND stand;
	TREE tree;

	if(dd->nDevice == 0)	// digitech
	{
		if(dd->nCommmand == 1)	// digicopy
		{
			m_nCopyAmount = dd->nData1;	// num of diameters
			m_nCopyReceived = 0;
			m_nZeros = 0;

			// set the progress max
			m_cProgress.SetRange32(0, m_nCopyAmount);
			m_cProgress.SetStep(1);
			m_cProgress.SetPos(0);

			if(m_nStand == 0)
			{
				// add the new stand
				stand.fPlotRadius = 0.0;
				stand.nNumPlots = 1;
				stand.nNumTrees = 0;
				_tcscpy(stand.szName, _T(""));
				stand.nMethod = 0;
				stand.nPart = 0;
				theApp.m_caStands.Add(stand);

				m_nStand++;
			}
			if(m_nPlots == 0) m_nPlots++;


			m_csStatus.Format(_T("Digitech copy (%d packets)"), m_nCopyAmount);

			// start a timeout timer for the copy.
			m_nTimer = SetTimer(1, 1000, 0);

			GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
		}
		else	// normal diameter
		{
			if(dd->nData2 == 0 && (theApp.m_bD_Three0 == TRUE || theApp.m_bD_Two0 == TRUE))
			{
				m_nZeros++;

				if(theApp.m_bD_Del0 == FALSE)	// add the 0-tree
				{
					stand = theApp.m_caStands.GetAt(m_nStand-1);

					tree.nSpc = dd->nData1;
					tree.nDia = dd->nData2;
					tree.nHeight = 0;
					tree.nHeight2 = 0;
					tree.nPlot = stand.nNumPlots;
					theApp.m_caTrees.Add(tree);

					m_nTrees++;
					stand.nNumTrees++;
					theApp.m_caStands.SetAt(m_nStand-1, stand);
				}
			}
			else
			{
				if(m_nZeros > 0)
				{
					if(m_nZeros == 2 && theApp.m_bD_Two0 == TRUE)	// new plot
					{
						stand = theApp.m_caStands.GetAt(m_nStand-1);
						stand.nNumPlots++;
						theApp.m_caStands.SetAt(m_nStand-1, stand);

						m_nPlots++;
					}
					else if(m_nZeros == 3 && theApp.m_bD_Three0 == TRUE)	// new stand
					{
						stand.nNumPlots = 1;
						stand.nNumTrees = 0;
						stand.fPlotRadius = 0.0;
						_tcscpy(stand.szName, _T(""));
						stand.nMethod = 0;
						stand.nPart = 0;
						theApp.m_caStands.Add(stand);

						m_nPlots++;
						m_nStand++;
					}

					m_nZeros = 0;
				}
				else
				{
					if(dd->nData1 == 8 && theApp.m_bD_Height8 == TRUE)	// height
					{
						// set the height on the previous tree
						tree = theApp.m_caTrees.GetAt(m_nTrees-1);

						if(tree.nHeight == 0)
							tree.nHeight = dd->nData2;
						else if(tree.nHeight2 == 0)
							tree.nHeight2 = dd->nData2;

						theApp.m_caTrees.SetAt(m_nTrees-1, tree);
					}
					else
					{
						// add a new tree
						stand = theApp.m_caStands.GetAt(m_nStand-1);

						tree.nSpc = dd->nData1;
						tree.nDia = dd->nData2;
						tree.nHeight = 0;
						tree.nHeight2 = 0;
						tree.nPlot = stand.nNumPlots;
						theApp.m_caTrees.Add(tree);

						m_nTrees++;
						stand.nNumTrees++;
						theApp.m_caStands.SetAt(m_nStand-1, stand);
					}
				}
			}

			if(m_nCopyAmount != 0)
			{
				// poke on the timeout timer so it won't stop
				if(m_nTimer != 0) KillTimer(m_nTimer);
				m_nTimer = SetTimer(1, 1000, 0);

				m_cProgress.StepIt();
				m_nCopyReceived++;
			}
		}
	}

	UpdateData(FALSE);

	return TRUE;
}

// the timeout timer
void CRecvdataDDlg::OnTimer(UINT nIDEvent) 
{
	if(m_nTimer != 0) KillTimer(m_nTimer);

	if(m_nCopyAmount > 0)
	{
		if(m_nCopyAmount != m_nCopyReceived)
		{
			m_nCopyAmount = 0;
			m_nCopyReceived = 0;
			m_nStand = 0;
			m_nPlots = 0;
			m_nTrees = 0;
			m_nZeros = 0;

			theApp.m_caStands.RemoveAll();
			theApp.m_caTrees.RemoveAll();

			m_cProgress.SetPos(0);

			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);

			m_bDone = FALSE;
			m_bReport = FALSE;

			m_pCComThread->Abort();
			::WaitForSingleObject( m_pCComThread->m_hStopEvent, INFINITE );
			m_pCComThread = NULL;

			m_csStatus.Format(_T("%s"), g_pXML->str(1077));
		}
		else
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);

			m_csStatus.Format(_T("%s"), g_pXML->str(1076));
			GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);
			m_bDone = TRUE;
		}
	}

	UpdateData(FALSE);

	CXTResizeFormView::OnTimer(nIDEvent);
}

// save the data collected
int CRecvdataDDlg::OnSave(BOOL bQuit)
{
	// show the summary-dialog
	CDigiExportDlg dlg;
	int nRet = dlg.DoModal();
	if(nRet != IDOK)
	{
		if(bQuit == FALSE && m_pCComThread == NULL) OpenComThread();	// restart communication-thread
		return FALSE;
	}


	// show a filechooser
	TCHAR tzFilename[1024], tzDir[1024];
	STAND stand;
	if(theApp.m_caStands.GetSize() > 0)
	{
		stand = theApp.m_caStands.GetAt(0);
		_stprintf(tzFilename, stand.szName);
	}
	else
	{
		_stprintf(tzFilename, _T(""));
	}
	_stprintf(tzDir, theApp.m_csD_Downloadpath);

	CFileDialog dlgF(TRUE);
	dlgF.m_ofn.lStructSize = sizeof(OPENFILENAME);
	dlgF.m_ofn.lpstrTitle = g_pXML->str(1100);
	dlgF.m_ofn.hwndOwner = this->GetSafeHwnd();
	dlgF.m_ofn.nMaxFile = 511;
	dlgF.m_ofn.lpstrFile = tzFilename;
	dlgF.m_ofn.lpstrInitialDir = tzDir;
	dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;


	if(theApp.m_nD_OutFormat == 0)	// XML
	{
		dlgF.m_ofn.lpstrDefExt = (LPTSTR)_T("xml");
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("XML (*.xml)\0*.xml\0\0");
	}
	else if(theApp.m_nD_OutFormat == 1)	// INV
	{
		dlgF.m_ofn.lpstrDefExt = (LPTSTR)_T("inv");
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("INV (*.inv)\0*.inv\0\0");
	}
	else if(theApp.m_nD_OutFormat == 2 || theApp.m_nD_OutFormat == 3)	// Tabseparated text & 2cm standtable
	{
		dlgF.m_ofn.lpstrDefExt = (LPTSTR)_T("txt");
		dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("TXT (*.txt)\0*.txt\0\0");
	}

	nRet = dlgF.DoModal();
	if(nRet != IDOK)
	{
		if(bQuit == FALSE && m_pCComThread == NULL) OpenComThread();	// restart communication-thread
		return FALSE;
	}
	m_csFilename = dlgF.GetPathName();
	m_nExportType = theApp.m_nD_OutFormat;


	// take care of the export
	BOOL bSave = FALSE;

	if(m_nExportType == 0)	// XML
	{
		bSave = SaveXML();
	}
	else if(m_nExportType == 1)	// INV
	{
		bSave = SaveINV();
	}
	else if(m_nExportType == 2)	// Tabseparated text
	{
		bSave = SaveTXT();
	}
	else if(m_nExportType == 3)	// 2cm standtable
	{
		bSave = SaveStand();
	}


	if(bSave == FALSE)
		m_csStatus.Format(_T("%s"), g_pXML->str(1036));	// data NOT saved
	else
	{
		OnDelete(bQuit);
		m_csStatus.Format(_T("%s"), g_pXML->str(1099));	// data saved
	}

	UpdateData(FALSE);
	if(bQuit == FALSE && m_pCComThread == NULL) OpenComThread();	// restart communication-thread

	return bSave;
}

// delete all data
int CRecvdataDDlg::OnDelete(BOOL bQuit)
{
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);

	m_nCopyAmount = 0;
	m_nCopyReceived = 0;
	m_nStand = 0;
	m_nPlots = 0;
	m_nTrees = 0;
	m_nZeros = 0;

	theApp.m_caStands.RemoveAll();
	theApp.m_caTrees.RemoveAll();

	m_cProgress.SetPos(0);

	m_csStatus.Format(_T("%s"), g_pXML->str(1078));

	m_bDone = FALSE;
	if(bQuit == FALSE && m_pCComThread == NULL) OpenComThread();	// restart communication-thread

	UpdateData(FALSE);

	return TRUE;
}

// start the receive-thread
int CRecvdataDDlg::OpenComThread()
{
	if(m_pCComThread != NULL) return FALSE;

	int pos = 0;
	CString csBuf = m_csComport.Tokenize(_T("COM:"), pos);

	m_pCComThread = (CComThread*)::AfxBeginThread( RUNTIME_CLASS( CComThread ), 0 /*THREAD_PRIORITY_ABOVE_NORMAL*/ );
	m_pCComThread->m_hWnd = this->m_hWnd;
	m_pCComThread->m_csFilename = m_csFilename;
	m_pCComThread->m_csFilenameShort = m_csFilenameShort;
	m_pCComThread->m_nDirection = 1;	// down
	m_pCComThread->m_csDestpath = m_csFilename;
	m_pCComThread->m_nType = 5;	// digitech pc
	m_pCComThread->m_hWnd = GetSafeHwnd();
	m_pCComThread->m_nBaud = CStringToInt(m_csBaudrate);
	m_pCComThread->m_nComport = CStringToInt(csBuf);
	m_pCComThread->DoContinuous();

	return TRUE;
}

// show the outputfiles
void CRecvdataDDlg::OnPreview()
{
	if(m_nExportType == 0)	// XML
	{
	}
	else if(m_nExportType == 1)	// INV
	{
	}
	else if(m_nExportType == 2)	// Tabseparated text
	{
		OnReport(m_csFilename);
	}
	else if(m_nExportType == 3)	// 2cm standtable
	{
		OnReport(m_csFilename);
	}
}

// show the received data in a report
void CRecvdataDDlg::OnReport(CString csFilename)
{
	// open that file in the reportviewer
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(300,
		_T("OpenSuiteEx"),	
		_T("Reports.dll"),
		_T("showtext_report.fr3"),
		(csFilename),
		_T("")));
}

// save data in text format
int CRecvdataDDlg::SaveTXT()
{
	CFile fh;
	STAND stand;
	TREE tree;

	if(fh.Open(m_csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
	{
		MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
		return FALSE;
	}

	int nLoop, nLoop2, nPlot, nTreeIndex=0;
	TCHAR szBuf[1024];

	_stprintf(szBuf, _T("Digitech data\r\n\r\n"));
	fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

	for(nLoop=0; nLoop<theApp.m_caStands.GetSize(); nLoop++)
	{
		stand = theApp.m_caStands.GetAt(nLoop);

		_stprintf(szBuf, _T("%s %s\r\n%s %d\r\n%s %d\r\n%s %.2f\r\n"),
			g_pXML->str(1106), stand.szName,
			g_pXML->str(1096), stand.nPart,
			g_pXML->str(1110), stand.nNumPlots,
			g_pXML->str(1108), stand.fPlotRadius);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		nPlot = -1;

		for(nLoop2=nTreeIndex; nLoop2<(nTreeIndex+stand.nNumTrees); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);

			if(nPlot != tree.nPlot)
			{
				_stprintf(szBuf, _T("\r\n%s %d\r\n%s\t%s\t%s\t%s2\r\n"),
					g_pXML->str(1105), tree.nPlot, g_pXML->str(1102), g_pXML->str(1103), g_pXML->str(1104), g_pXML->str(1104));
				fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

				nPlot = tree.nPlot;
			}

			_stprintf(szBuf, _T("%s\t%d\t%d\t%d\r\n"), theApp.m_caD_Species.GetAt(tree.nSpc-1), tree.nDia, tree.nHeight, tree.nHeight2);
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		}

		nTreeIndex += stand.nNumTrees;

		_stprintf(szBuf, _T("\r\n\r\n"));
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
	}

	fh.Close();

	// enable the preview-button
	m_bReport = TRUE;
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, TRUE);

	return TRUE;
}

// save data in INV format
int CRecvdataDDlg::SaveINV()
{
	CFile fh;
	STAND stand;
	TREE tree;
	CString csFilename, csBuf;
	char szBuf[1024], szBufTmp[512], szBuf2[128];
	int nLoop, nLoop2, nTreeIndex=0, nSpcExist[10], nCount;
	int nPlot=0, nTrees=0, nArea=0, nRet;
	TCHAR tzFilename[1024], tzDir[1024];

	// save every stand in seperate files
	for(nLoop=0; nLoop<theApp.m_caStands.GetSize(); nLoop++)
	{
		stand = theApp.m_caStands.GetAt(nLoop);

		if(nLoop > 0)
		{
			// show a filechooser
			_stprintf(tzFilename, stand.szName);
			_stprintf(tzDir, theApp.m_csD_Downloadpath);

			CFileDialog dlgF(TRUE);
			dlgF.m_ofn.lStructSize = sizeof(OPENFILENAME);
			dlgF.m_ofn.lpstrTitle = g_pXML->str(1100);
			dlgF.m_ofn.hwndOwner = this->GetSafeHwnd();
			dlgF.m_ofn.nMaxFile = 511;
			dlgF.m_ofn.lpstrFile = tzFilename;
			dlgF.m_ofn.lpstrInitialDir = tzDir;
			dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;
			dlgF.m_ofn.lpstrDefExt = (LPTSTR)_T("inv");
			dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("INV (*.inv)\0*.inv\0\0");

			nRet = dlgF.DoModal();
			if(nRet == IDCANCEL)
			{
				return FALSE;
			}

			m_csFilename = dlgF.GetPathName();
		}

		csFilename = m_csFilename;

		// create the file
		if(fh.Open(csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
		{
			MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
			return FALSE;
		}

		// do some calculations
		nSpcExist[0] = 0;
		nSpcExist[1] = 0;
		nSpcExist[2] = 0;
		nSpcExist[3] = 0;
		nSpcExist[4] = 0;
		nSpcExist[5] = 0;
		nSpcExist[6] = 0;
		nSpcExist[7] = 0;
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);

			nSpcExist[tree.nSpc-1] = 1;
		}


		// write the data
		CTime m_ctDate = CTime::GetCurrentTime();
		int nYear = m_ctDate.GetYear(),
			nMonth = m_ctDate.GetMonth(),
			nDay = m_ctDate.GetDay(),
			nHour = m_ctDate.GetHour(),
			nMinute = m_ctDate.GetMinute(),
			nSecond = m_ctDate.GetSecond();

		sprintf(szBuf, "1 2\nINV HMS~1 3 \nISO 8859-1~2 1 \n%S#%d~" \
			"12 4 \n%04d%02d%02d%02d%02d%02d~111 1 4~",
			stand.szName, stand.nPart,
			nYear, nMonth, nDay, nHour, nMinute, nSecond);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 111 1 - Number of species
		nCount = 0;
		for(nLoop2=0; nLoop2<8; nLoop2++)
		{
			if(nSpcExist[nLoop2] == 1)
			{
				nCount++;
			}
		}
		sprintf(szBuf, "111 1 %d~", nCount);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 120 1 - Specienames
		sprintf(szBuf, "120 1 ");
		for(nLoop2=0; nLoop2<8; nLoop2++)
		{
			if(nSpcExist[nLoop2] == 1)
			{
				sprintf(szBufTmp, "\n%S", theApp.m_caD_Species.GetAt(nLoop2));
				strcat(szBuf, szBufTmp);
			}
		}
		sprintf(szBufTmp, "~");
		strcat(szBuf, szBufTmp);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 120 3 - Specienumbers
		sprintf(szBuf, "120 3");
		for(nLoop2=0; nLoop2<8; nLoop2++)
		{
			if(nSpcExist[nLoop2] == 1)
			{
				sprintf(szBufTmp, " %d", nLoop2+1);
				strcat(szBuf, szBufTmp);
			}
		}
		sprintf(szBufTmp, "~");
		strcat(szBuf, szBufTmp);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 651 1 - Number of plots
		sprintf(szBuf, "651 1 %d~", stand.nNumPlots);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 222 3 - Trees/plot
		sprintf(szBuf, "222 3"); 
		fh.Write(szBuf, (int)strlen(szBuf));

		nPlot=1; nTrees=0;
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);

			if(tree.nPlot != nPlot)
			{
				sprintf(szBuf, " %d", nTrees);
				fh.Write(szBuf, (int)strlen(szBuf));

				nTrees=1;
				nPlot = tree.nPlot;
			}
			else
			{
				nTrees++;
			}
		}
		sprintf(szBuf, " %d~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 671 1 - Area of plots
		sprintf(szBuf, "671 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));

		nArea = stand.fPlotRadius*stand.fPlotRadius*3.1415;
		for(nLoop2=0; nLoop2<nPlot; nLoop2++)
		{
			sprintf(szBuf, " %d", nArea);
			fh.Write(szBuf, (int)strlen(szBuf));
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 652 1 - Species codes for every tree
		sprintf(szBuf, "652 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));

		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);
			sprintf(szBuf, " %d", tree.nSpc);
			fh.Write(szBuf, (int)strlen(szBuf));
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 653 1 - Diameters for every tree
		sprintf(szBuf, "653 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));

		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);
			sprintf(szBuf, " %d", tree.nDia);
			fh.Write(szBuf, (int)strlen(szBuf));
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 654 1 - Number of heights/tree
		sprintf(szBuf, "654 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));
		
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);
			
			if(tree.nHeight > 0)
			{
				sprintf(szBuf, " 1");

				if(tree.nHeight2 > 0)	// we got a second height aswell?
				{
					sprintf(szBuf, " 2");
				}
			}
			else
			{
				sprintf(szBuf, " 0");
			}
			fh.Write(szBuf, (int)strlen(szBuf));
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 655 1 - Heightcode/height	(50 first height, 51 second)
		sprintf(szBuf, "655 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));
		
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);
			
			if(tree.nHeight > 0)
			{
				sprintf(szBuf, " 50");

				if(tree.nHeight2 > 0)	// we got a second height aswell?
				{
					strcat(szBuf, " 51");
				}

				fh.Write(szBuf, (int)strlen(szBuf));
			}
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 656 1 - Heights cm
		sprintf(szBuf, "656 1"); 
		fh.Write(szBuf, (int)strlen(szBuf));
		
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);
			
			if(tree.nHeight > 0)
			{
				sprintf(szBuf, " %d", tree.nHeight * 10);	// cm

				if(tree.nHeight2 > 0)	// we got a second height aswell?
				{
					sprintf(szBuf2, " %d", tree.nHeight2 * 10);	// cm
					strcat(szBuf, szBuf2);
				}

				fh.Write(szBuf, (int)strlen(szBuf));
			}
		}
		sprintf(szBuf, "~", nTrees);
		fh.Write(szBuf, (int)strlen(szBuf));


		// 2030 1 - Measuringmethod	(1 - circle, 2 - total, 3 - quick)
		sprintf(szBuf, "2030 1 %d~", stand.nMethod+1); 
		fh.Write(szBuf, (int)strlen(szBuf));



		fh.Close();

		nTreeIndex += stand.nNumTrees;
	}

	return TRUE;
}

// save data in 2cm standtable format
int CRecvdataDDlg::SaveStand()
{
	CFile fh;
	STAND stand;
	TREE tree;
	int nDklass[44] = {8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64, 66,68,70,72,74,76,78,80,82,84,86,88,90, 0};
	int nStandtable[NUM_SPECIES+1][MAX_DIA-MIN_DIA];
	int nI, nJ, nDiaCls;
	int nLoop, nLoop2, nTreeIndex=0;
	TCHAR szBuf[1024];

	if(fh.Open(m_csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
	{
		MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
		return FALSE;
	}


	_stprintf(szBuf, _T("Digitech data\r\n\r\n"));
	fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

	for(nLoop=0; nLoop<theApp.m_caStands.GetSize(); nLoop++)
	{
		for(nI=0; nI<NUM_SPECIES; nI++)
		{
			for(nJ=0; nJ<((MAX_DIA-MIN_DIA)/DIA_INT); nJ++)
			{
				nStandtable[nI][nJ] = 0;
			}
		}

		stand = theApp.m_caStands.GetAt(nLoop);

		_stprintf(szBuf, _T("%s %s\r\n%s %d\r\n%s %d\r\n%s %.2f\r\n"),
			g_pXML->str(1106), stand.szName,
			g_pXML->str(1096), stand.nPart,
			g_pXML->str(1110), stand.nNumPlots,
			g_pXML->str(1108), stand.fPlotRadius);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));


		// calculate standtable
		_stprintf(szBuf, _T("\r\n%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\r\n"),
			g_pXML->str(1112), theApp.m_caD_Species.GetAt(0), theApp.m_caD_Species.GetAt(1),
			theApp.m_caD_Species.GetAt(2), theApp.m_caD_Species.GetAt(3), theApp.m_caD_Species.GetAt(4),
			theApp.m_caD_Species.GetAt(5), theApp.m_caD_Species.GetAt(6), theApp.m_caD_Species.GetAt(7));
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);

			nDiaCls = (tree.nDia / 20) - 4;
			if(nDiaCls < 0) nDiaCls = 0;

			nStandtable[tree.nSpc-1][nDiaCls]++;
			nStandtable[tree.nSpc-1][44]++;
		}


		// print the diameters
		for(nJ=0; nJ<((MAX_DIA-MIN_DIA)/DIA_INT); nJ++)
		{
			_stprintf(szBuf, _T("%d"), nDklass[nJ]);
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

			for(nI=0; nI<NUM_SPECIES; nI++)
			{
				_stprintf(szBuf, _T("\t%d"), nStandtable[nI][nJ]);
				fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
			}
	
			_stprintf(szBuf, _T("\r\n"));
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		}

		_stprintf(szBuf, _T("\r\n\r\n"));
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
				
		nTreeIndex += stand.nNumTrees;
	}

	fh.Close();

	// enable the preview-button
	m_bReport = TRUE;
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, TRUE);

	return TRUE;
}

// save data in XML format
int CRecvdataDDlg::SaveXML()
{
	CFile fh;
	STAND stand;
	TREE tree;
	TCHAR szBuf[1024];
	int nTreeIndex=0, nLoop, nLoop2, nPlot;

	if(fh.Open(m_csFilename, CFile::modeCreate|CFile::modeWrite, 0) == 0)
	{
		MessageBox(g_pXML->str(1036), g_pXML->str(1101), MB_ICONSTOP);
		return FALSE;
	}

	// write the data
	_stprintf(szBuf, _T("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<HMS_Com>\r\n"));
	fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

	for(nLoop=0; nLoop<theApp.m_caStands.GetSize(); nLoop++)
	{
		stand = theApp.m_caStands.GetAt(nLoop);

		_stprintf(szBuf, _T("\t<STAND>\r\n\t\t<NAME>%s</NAME>\r\n"), stand.szName);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		_stprintf(szBuf, _T("\t\t<PART>%d</PART>\r\n"), stand.nPart);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		_stprintf(szBuf, _T("\t\t<TYPE>%d</TYPE>\r\n"), stand.nMethod);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		// loop through all trees/plots
		nPlot = -1;
		for(nLoop2=nTreeIndex; nLoop2<(stand.nNumTrees+nTreeIndex); nLoop2++)
		{
			tree = theApp.m_caTrees.GetAt(nLoop2);

			if(nPlot == -1)	// the first plot
			{
				_stprintf(szBuf, _T("\t\t<PLOT>\r\n\t\t\t<RADIE>%.2f</RADIE>\r\n"), stand.fPlotRadius);
				fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

				nPlot = tree.nPlot;
			}
			else if(tree.nPlot != nPlot)	// plot changed
			{
				_stprintf(szBuf, _T("\t\t</PLOT>\r\n\t\t<PLOT>\r\n\t\t\t<RADIE>%.2f</RADIE>\r\n"), stand.fPlotRadius);
				fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

				nPlot = tree.nPlot;
			}

			_stprintf(szBuf, _T("\t\t\t<TREE spc=\"%d\" d1=\"%d\" h1=\"%d\" h2=\"%d\"/>\r\n"), tree.nSpc, tree.nDia, tree.nHeight, tree.nHeight2);
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		}

		if(nPlot != -1)	// terminate the PLOT-tag
		{
			_stprintf(szBuf, _T("\t\t</PLOT>\r\n"));
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		}

		_stprintf(szBuf, _T("\t</STAND>\r\n"));
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		nTreeIndex += stand.nNumTrees;
	}


	_stprintf(szBuf, _T("</HMS_Com>\r\n"));
	fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

	fh.Close();

	return TRUE;
}
