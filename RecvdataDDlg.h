#pragma once

#define MAX_DIA 90
#define MIN_DIA 6
#define DIA_INT 2
#define NUM_SPECIES 8


#include "ComThread.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CRecvdataDFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CRecvdataDFrame)

	BOOL m_bOnce;
public:
	CRecvdataDFrame();

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecvdataDFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRecvdataDFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CRecvdataDFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CRecvdataDDlg form view

class CRecvdataDDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CRecvdataDDlg)
public:
	CString m_csFilename;
	CString m_csFilenameShort;

	CComThread* m_pCComThread;
protected:
	CRecvdataDDlg();           // protected constructor used by dynamic creation
	virtual ~CRecvdataDDlg();

	CMDISampleSuiteDoc* pDoc;

	CProgressCtrl m_cProgress;

	// dialog text
	int m_nStand;
	int m_nPlots;
	int m_nTrees;
	CString m_csComport;
	CString m_csBaudrate;
	CString m_csStatus;

	int m_nZeros;
	int m_nCopyAmount;
	int m_nCopyReceived;
	int m_nTimer;	// timeout timer

	bool m_bChanged;


public:
	enum { IDD = IDD_RECVDATAD };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	BOOL m_bQuit;
	BOOL m_bDone;
	BOOL m_bReport;
	int m_nExportType;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	LRESULT OnPacket(WPARAM wParm, LPARAM lParm);
	LRESULT OnLength(WPARAM wParm, LPARAM lParm);
	LRESULT OnDone(WPARAM wParm, LPARAM lParm);
	LRESULT OnFile(WPARAM wParm, LPARAM lParm);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg int OpenComThread();
	afx_msg void OnReport(CString csFilename);

	afx_msg int SaveTXT();
	afx_msg int SaveXML();
	afx_msg int SaveINV();
	afx_msg int SaveStand();
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnSave(BOOL bQuit = FALSE);
	afx_msg int OnDelete(BOOL bQuit = FALSE);
	afx_msg void OnPreview();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedButtonTransfer();
};


