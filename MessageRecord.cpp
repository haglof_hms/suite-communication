// MessageRecord.cpp: implementation of the CMessageRecord class.
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MessageRecord.h"
#include "samplesuite.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_SERIAL(CMessageRecordItemCheck, CXTPReportRecordItem, 0)
IMPLEMENT_SERIAL(CMessageRecordItemCombo, CXTPReportRecordItem, 0)
IMPLEMENT_SERIAL(CMessageRecordItemIcon, CXTPReportRecordItem, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemIcon

CMessageRecordItemIcon::CMessageRecordItemIcon(BOOL bRead)
	: m_bRead(bRead)
{
	UpdateReadIcon();
}

void CMessageRecordItemIcon::UpdateReadIcon()
{
	SetIconIndex(m_bRead ? 6 : 0);

}

int CMessageRecordItemIcon::Compare(CXTPReportColumn* /*pColumn*/, CXTPReportRecordItem* pItem)
{
	return int(m_bRead) - int(((CMessageRecordItemIcon*)pItem)->m_bRead);
}


CString CMessageRecordItemIcon::GetGroupCaption(CXTPReportColumn* /*pColumn*/)
{
	if (m_bRead)
		return g_pXML->str(1117); //_T("New versions");
	else
		return g_pXML->str(1116); //_T("No new versions");
}

int CMessageRecordItemIcon::CompareGroupCaption(CXTPReportColumn* pColumn, CXTPReportRecordItem* pItem)
{
	return GetGroupCaption(pColumn).Compare(pItem->GetGroupCaption(pColumn));
}

void CMessageRecordItemIcon::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecordItem::DoPropExchange(pPX);

	PX_Bool(pPX, _T("Version"), m_bRead);
}


//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCheck

CMessageRecordItemCheck::CMessageRecordItemCheck(BOOL bCheck)
{
	HasCheckbox(TRUE);
	SetChecked(bCheck);
}

int CMessageRecordItemCheck::GetGroupCaptionID(CXTPReportColumn* /*pColumn*/)
{
//	return IsChecked()? IDS_GROUP_CHECKED_TRUE: IDS_GROUP_CHECKED_FALSE;
	return FALSE;
}

int CMessageRecordItemCheck::Compare(CXTPReportColumn* /*pColumn*/, CXTPReportRecordItem* pItem)
{
	return int(IsChecked()) - int(pItem->IsChecked());
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CMessageRecord, CXTPReportRecord, 0)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessageRecord::CMessageRecord()
{
	m_pItemIcon = NULL;
	CreateItems();
}

CMessageRecord::CMessageRecord(
		BOOL bLocal,
		int nProgID,
		CString strName,
		CString strVersion,
		CString strNewVersion,
		CString strLanguage,
		CString strTLA
		)
{
	m_nProgID = nProgID;

	bool bRead = FALSE;
	if(strVersion != strNewVersion && strVersion != "" && strNewVersion != "") bRead = TRUE;

	m_pItemIcon = (CMessageRecordItemIcon*)AddItem(new CMessageRecordItemIcon(bRead));
	AddItem(new CXTPReportRecordItemText(strName));
	AddItem(new CXTPReportRecordItemText(strVersion));
	AddItem(new CXTPReportRecordItemText(strNewVersion));
	AddItem(new CXTPReportRecordItemText(strLanguage));
	AddItem(new CXTPReportRecordItemText(strTLA));
}

void CMessageRecord::CreateItems()
{
	// Initialize record items with empty values
	m_pItemIcon = (CMessageRecordItemIcon*)AddItem(new CMessageRecordItemIcon(TRUE));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
}

CMessageRecord::~CMessageRecord()
{

}

BOOL CMessageRecord::SetRead()
{
	ASSERT(m_pItemIcon);
	if (!m_pItemIcon->m_bRead)
		return FALSE;

	m_pItemIcon->m_bRead = FALSE;
	m_pItemIcon->UpdateReadIcon();

	return TRUE;
}


void CMessageRecord::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
	if (m_pItemIcon && m_pItemIcon->m_bRead && !pDrawArgs->pItem->IsPreviewItem())
		pItemMetrics->pFont = &pDrawArgs->pControl->GetPaintManager()->m_fontBoldText;
/*
	CReportSampleView* pView = DYNAMIC_DOWNCAST(CReportSampleView, pDrawArgs->pControl->GetParent());

	// If automatic formatting option is enabled, sample code below will be executed.
	// There you can see an example of "late" customization for colors, fonts, etc.
	if (pView && pView->m_bAutomaticFormating)
	{
		if ((pDrawArgs->pRow->GetIndex() % 2) && !pDrawArgs->pItem->IsPreviewItem())
		{
			pItemMetrics->clrBackground = RGB(245, 245, 245);
		}
		if (pDrawArgs->pItem->GetCaption(pDrawArgs->pColumn).Find(_T("Undeliverable")) >= 0)
		{
			pItemMetrics->clrForeground = RGB(0xFF, 0, 0);

		}
	}
*/
}

void CMessageRecord::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecord::DoPropExchange(pPX);

	if (pPX->IsLoading())
	{
		// 1 - m_pItemIcon = (CMessageRecordItemIcon*)AddItem(new CMessageRecordItemIcon(TRUE));
		ASSERT_KINDOF(CMessageRecordItemIcon, GetItem(COLUMN_FLAG));
		m_pItemIcon = DYNAMIC_DOWNCAST(CMessageRecordItemIcon, GetItem(COLUMN_FLAG));
		ASSERT(m_pItemIcon);
/*		
		// 5 - m_pItemReceived = (CMessageRecordItemDate*)AddItem(new CMessageRecordItemDate(dtNow));
		ASSERT_KINDOF(CMessageRecordItemDate, GetItem(5));
		m_pItemReceived = DYNAMIC_DOWNCAST(CMessageRecordItemDate, GetItem(5));
		ASSERT(m_pItemReceived);
		
		// 6 - m_pItemSize = AddItem(new CXTPReportRecordItemNumber(0));
		ASSERT_KINDOF(CXTPReportRecordItemNumber, GetItem(6));
		m_pItemSize = DYNAMIC_DOWNCAST(CXTPReportRecordItemNumber, GetItem(6));
		ASSERT(m_pItemSize);		
*/
	}
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CKeyRecord, CXTPReportRecord, 0)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeyRecord::CKeyRecord()
{
	CreateItems();
}

CKeyRecord::CKeyRecord(
		BOOL bSend,
		int nSerial,
		int nKey,
		int nLevel,
		CString csComment
		)
{
	CXTPReportRecordItem* pItem = AddItem(new CMessageRecordItemCheck(bSend));
	pItem->SetItemData(0);

	pItem = AddItem(new CXTPReportRecordItemNumber(nSerial));
	pItem->SetItemData(1);

	pItem = AddItem(new CMessageRecordItemCombo(nLevel));
	pItem->SetItemData(2);

	pItem = AddItem(new CXTPReportRecordItemNumber(nKey));
	pItem->SetItemData(3);

	pItem = AddItem(new CXTPReportRecordItemText(csComment));
	pItem->SetItemData(4);
}

void CKeyRecord::CreateItems()
{
	// Initialize record items with empty values
}

CKeyRecord::~CKeyRecord()
{
}

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCombo

CMessageRecordItemCombo::CMessageRecordItemCombo(int nLevel)
{
	m_nValue = nLevel;

	GetEditOptions(NULL)->AddConstraint(_T("1"), 1);
	GetEditOptions(NULL)->AddConstraint(_T("2"), 2);
	GetEditOptions(NULL)->AddConstraint(_T("3"), 3);
	GetEditOptions(NULL)->AddConstraint(_T("4"), 4);
	GetEditOptions(NULL)->AddConstraint(_T("5"), 5);
	GetEditOptions(NULL)->AddConstraint(_T("6"), 6);
	GetEditOptions(NULL)->AddConstraint(_T("7"), 7);
	GetEditOptions(NULL)->AddConstraint(_T("8"), 8);
	GetEditOptions(NULL)->AddConstraint(_T("9"), 9);
	GetEditOptions(NULL)->AddConstraint(_T("10"), 10);
	GetEditOptions(NULL)->m_bConstraintEdit = TRUE;
	GetEditOptions(NULL)->AddComboButton();
//	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
//	pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
//	pCol->GetEditOptions()->m_nMaxLength = 10;
}

CString CMessageRecordItemCombo::GetCaption(CXTPReportColumn* /*pColumn*/)
{
	CXTPReportRecordItemConstraint* pConstraint = GetEditOptions(NULL)->FindConstraint(m_nValue);
	ASSERT(pConstraint);
	return pConstraint->m_strConstraint;
}

void CMessageRecordItemCombo::OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS*, CXTPReportRecordItemConstraint* pConstraint)
{
	m_nValue = (int)pConstraint->m_dwData;
}
