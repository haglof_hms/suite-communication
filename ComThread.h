#if !defined(AFX_TimerThread_H__FFA0F7FF_6927_11D2_ACA8_00C04F81B6B0__INCLUDED_)
#define AFX_TimerThread_H__FFA0F7FF_6927_11D2_ACA8_00C04F81B6B0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Kermit.h"

/////////////////////////////////////////////////////////////////////////////
// CComThread thread
class CComThread : public CWinThread
{
	DECLARE_DYNCREATE( CComThread )

	void Abort();

private:
	CKermit* ck;

protected:
	CComThread( void );           // protected constructor used by dynamic creation
	int Up();
	int Down();

	BOOL ParseData(int nData);
	int m_bDC;
	unsigned int GetIntFromString(char *buf);

// Attributes
public:
	CString m_csFilename;	// filename used for up-/downloads.
	CString m_csFilenameShort;
	CString m_csDestpath;	// destination filepath for downloads

	HWND m_hWnd;			// handle to the gui
	HWND m_hWnd2;			// handle to a second gui

	int m_nType;			// 0 = Kermit, 1 = Ascii, 2 = INET
	int m_nDirection;		// 0 = Up, 1 = Down
	int m_nPos;				// current position
	int m_nBaud;
	int m_nComport;

	int m_nProdID;
	int m_nSerial;
	int m_nLevel;
	int m_nCode;
	CString m_csLang;

// Operations
public:
	bool DoContinuous( void ) { return( CWinThread::PostThreadMessage( tm_CComThreadOnDoContinuous, NULL, NULL ) ? true : false ); }
	bool StopContinuous( void ) { return( ::SetEvent( m_hStopEvent ) ? true : false ); }
	bool Quit( void ) {Abort(); return( StopContinuous( ) && CWinThread::PostThreadMessage( WM_QUIT, NULL, NULL ) ? true : false ); }
	bool IsFinished() { return m_bCanIQuit || m_bQuit; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComThread)
	public:
	virtual BOOL InitInstance( void );
	virtual int ExitInstance( void );
	//}}AFX_VIRTUAL

	BOOL m_bQuit;
	BOOL m_bCanIQuit;

// Implementation
	HANDLE m_hStopEvent;
private:
	virtual ~CComThread( void );
	static UINT tm_CComThreadOnDoContinuous;


	// Generated message map functions
	//{{AFX_MSG(CComThread)
	// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg void OnDoContinuous( WPARAM wParam, LPARAM lParam );
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TimerThread_H__FFA0F7FF_6927_11D2_ACA8_00C04F81B6B0__INCLUDED_)
