// LicensesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "LicensesDlg.h"
#include ".\licensesdlg.h"
#include "SampleSuiteForms.h"
#include "TransferDlg.h"
#include "MessageRecord.h"

IMPLEMENT_DYNCREATE(CLicensesFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CLicensesFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CLicensesFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicensesFrame construction/destruction

CLicensesFrame::CLicensesFrame()
{
	m_bOnce = TRUE;
}

CLicensesFrame::~CLicensesFrame()
{
}

LRESULT CLicensesFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;

		case ID_DBNAVIG_START:
			((CLicensesDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_NEXT:
			((CLicensesDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_PREV:
			((CLicensesDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_END:
			((CLicensesDlg*)GetActiveView())->OnNavigate(wParm);
		break;
	};

	return 0;
}

BOOL CLicensesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLicensesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CLicensesFrame diagnostics

#ifdef _DEBUG
void CLicensesFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLicensesFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLicensesFrame message handlers

int CLicensesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_LICENSE);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR_LICENSE)
				{		
						// load/send program
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(1025));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 5);	// comto
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(1026));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 0);	// add
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(1027));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 28);	// minus
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(g_pXML->str(1028));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 13);	// gem
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	UpdateWindow();

	return 0;
}

void CLicensesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\License"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CLicensesFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CLicensesFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\License"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CLicensesFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

void CLicensesFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

/*---------------------------------------------------------------------*/
#define IDC_REPORT 9998
#define COLUMN_SERIAL 0
#define COLUMN_KEY 1

// CLicensesDlg

IMPLEMENT_DYNCREATE(CLicensesDlg, CXTResizeFormView)

CLicensesDlg::CLicensesDlg()
	: CXTResizeFormView(CLicensesDlg::IDD)
	, m_csDesc(_T(""))
{
	m_nSelProgram = -1;
	m_nSelLang = -1;
	m_nID = 0;
	m_bModified = FALSE;
}

CLicensesDlg::~CLicensesDlg()
{
	// save the keys.
	SaveKeys();    
}

void CLicensesDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Text(pDX, IDC_STATIC_LICENSE_PROGRAM2, m_csProgram);
	DDX_Text(pDX, IDC_STATIC_LICENSE_VERSION2, m_csVersion);
	DDX_Text(pDX, IDC_STATIC_LICENSE_ID2, m_nID);
	DDX_Text(pDX, IDC_STATIC_LICENSE_DESCRIPTION2, m_csDesc);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_REPORT, m_wndReport);
}

BEGIN_MESSAGE_MAP(CLicensesDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CLicensesDlg)
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_BUTTON_LICENSE_SEND, OnBnClickedSend)
	ON_BN_CLICKED(ID_BUTTON_LICENSE_ADDKEY, OnBnClickedButtonLicenseAddkey)
	ON_BN_CLICKED(ID_BUTTON_LICENSE_DELKEY, OnBnClickedButtonLicenseDelkey)
	ON_BN_CLICKED(ID_BUTTON_LICENSE_ATTACH, OnBnClickedButtonAttach)
	ON_WM_SETFOCUS()
	ON_WM_CREATE()
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_REPORT, OnRowSelect)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnReportValueChanged)
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnReportItemClick)
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()


// CLicensesDlg diagnostics

#ifdef _DEBUG
void CLicensesDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CLicensesDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

LRESULT CLicensesDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMMANAGE_PROGRAMS_PROGRAMINFO);

	return 0;
}

BOOL CLicensesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CLicensesDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the reportcontrol
	if (!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL, CRect(10, 158, 247, 334), this, IDC_REPORT))
	{
		TRACE(_T("Failed to create reportcontrol"));
		return -1;
	}

	m_wndReport.SetGridStyle(FALSE, xtpReportGridNoLines);
	m_wndReport.GetPaintManager()->SetColumnStyle(xtpReportColumnExplorer);

	return 0;
}

// CLicensesDlg message handlers
void CLicensesDlg::OnInitialUpdate()
{
	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	CXTResizeFormView::OnInitialUpdate();

	SetDlgItemText(IDC_STATIC_LICENSE_PROGRAM, g_pXML->str(1007));
	SetDlgItemText(IDC_STATIC_LICENSE_VERSION, g_pXML->str(1008));
	SetDlgItemText(IDC_STATIC_LICENSE_ID, g_pXML->str(1009));
	SetDlgItemText(IDC_STATIC_LICENSE_DESCRIPTION, g_pXML->str(1010));
	SetDlgItemText(IDC_STATIC_LICENSE_KEYS, g_pXML->str(1011));

	// setup the reportcontrol
	m_wndReport.ModifyStyle(0, WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP);

	m_wndReport.AddColumn(new CXTPReportColumn(0, g_pXML->str(1012), 20, TRUE, XTP_REPORT_NOICON, FALSE));
	m_wndReport.AddColumn(new CXTPReportColumn(1, g_pXML->str(1013), 40));
	m_wndReport.AddColumn(new CXTPReportColumn(2, g_pXML->str(1014), 20));
	m_wndReport.AddColumn(new CXTPReportColumn(3, g_pXML->str(1015), 40));
	m_wndReport.AddColumn(new CXTPReportColumn(4, g_pXML->str(1063), 40));

	m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);

	m_wndReport.AllowEdit(TRUE);
	m_wndReport.FocusSubItems(TRUE);


	// Set the resize
	SetResize(IDC_STATIC_LICENSE_PROGRAM2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_LICENSE_VERSION2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_LICENSE_ID2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_LICENSE_DESCRIPTION2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_PROGRAMS_KEYS, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_STATIC_LICENSE_KEYS, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_REPORT, SZ_TOP_LEFT , SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}

// add a new key
void CLicensesDlg::OnBnClickedButtonLicenseAddkey()
{
	PROGRAM prg;
	KEY key;
	int nKeyStart=0;

	for(int nLoop=0; nLoop<m_nSelProgram; nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);
		nKeyStart += prg.nKeys;
	}
	prg = theApp.m_caPrograms.GetAt(m_nSelProgram);

	key.nCode = 0;
	key.nKey = 0;
	key.nSerial = 0;
	key.nLevel = 1;
	key.csComment = _T("");
	theApp.m_caKeys.InsertAt(nKeyStart+prg.nKeys, key);

	prg.nKeys++;
	theApp.m_caPrograms.SetAt(m_nSelProgram, prg);

	// add the new record
	m_wndReport.AddRecord(new CKeyRecord(FALSE, key.nSerial, key.nCode, key.nLevel, key.csComment));
	m_wndReport.Populate();
}

// user has choosen to delete a key
void CLicensesDlg::OnBnClickedButtonLicenseDelkey()
{
	CXTPReportRow* pRow = m_wndReport.GetFocusedRow();
	if(pRow == 0) return;

	int nRet = MessageBox(g_pXML->str(1062), g_pXML->str(1000), MB_YESNO|MB_ICONQUESTION);
	if(nRet == IDNO) return;


	int nSelKey = pRow->GetIndex();
	m_wndReport.GetRecords()->RemoveAt(nSelKey);
	m_wndReport.Populate();

	if(m_wndReport.GetRecords()->GetCount() == 0)
		((CLicensesFrame*)GetParent())->SetButtonEnabled(2, FALSE);	// delkey


	PROGRAM prg;
	KEY key;
	int nKeyStart=0;

	for(int nLoop=0; nLoop<m_nSelProgram; nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);
		nKeyStart += prg.nKeys;
	}

	// update the number of keys of this program
	prg = theApp.m_caPrograms.GetAt(m_nSelProgram);
	prg.nKeys--;
	theApp.m_caPrograms.SetAt(m_nSelProgram, prg);

	// remove the selected index
	theApp.m_caKeys.RemoveAt(nKeyStart + nSelKey);

//	pDoc->SetModifiedFlag(TRUE);
}

void CLicensesDlg::OnSetFocus(CWnd* pOldWnd)
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTResizeFormView::OnSetFocus(pOldWnd);

	// has the current selected program changed?
	int nTmp, nTmp2;
	CString csPath;

	if(pDoc->m_nType == 0)	// digitechpro
	{
		nTmp = theApp.m_nSelProgramDP;
		nTmp2 = theApp.m_nSelLangDP;
		csPath = theApp.m_csDP_Filepath;
	}
	else if(pDoc->m_nType == 1)	// mantax computer
	{
		nTmp = theApp.m_nSelProgramMC;
		nTmp2 = theApp.m_nSelLangMC;
		csPath = theApp.m_csMC_Filepath;
	}


	if(m_nSelProgram != nTmp || m_nSelLang != nTmp2)
	{
		if(m_nSelProgram != -1 && m_nSelLang != -1)
			SaveKeys();	// save the current keys first.

		// insert the keys for the selected program
		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		int nKeyStart=0, nItem=0, nLangStart=0;

		// get the starting key.
		BOOL bFound = FALSE;

		for(int nLoop=0; nLoop<nTmp; nLoop++)
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);
			nKeyStart += prg.nKeys;
		}

		m_nSelProgram = nTmp;
		m_nSelLang = nTmp2;
		prg = theApp.m_caPrograms.GetAt(m_nSelProgram);
		lng = theApp.m_caLanguages.GetAt(/*nLangStart +*/ nTmp2);

		m_csProgram = prg.csName;
		m_nID = prg.nID;
		if(lng.csVersion != _T(""))
			m_csVersion = lng.csVersion;
		else
			m_csVersion = _T("");
		m_csDesc = lng.csDesc;

		// set the toolbaricons
		if(lng.bLocal == TRUE)
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(0, TRUE);	// send
		}
		else
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(0, FALSE);	// send
		}

		((CLicensesFrame*)GetParent())->SetButtonEnabled(1, TRUE);	// addkey
		if(prg.nKeys > 0)
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(2, TRUE);	// delkey
		}
		else
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(2, FALSE);	// delkey
		}

		// check if there is documentation (pdf) aswell
		if(lng.csDoc == "")
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(3, FALSE);	// doc
		}
		else
		{
			((CLicensesFrame*)GetParent())->SetButtonEnabled(3, TRUE);	// doc
		}


		// add the keys to the reportcontrol
		CString csBuf;

		m_wndReport.GetRecords()->RemoveAll();	// remove the old ones first

		for(int nKey=nKeyStart; nKey<(nKeyStart+prg.nKeys); nKey++)
		{
			key = theApp.m_caKeys.GetAt(nKey);

			m_wndReport.AddRecord(new CKeyRecord(FALSE, key.nSerial, key.nCode, key.nLevel, key.csComment));
		}

		m_wndReport.Populate();
		UpdateData(FALSE);
	}
}

void CLicensesDlg::OnRowSelect(NMHDR* nm, LRESULT* lr)
{
	((CLicensesFrame*)GetParent())->SetButtonEnabled(2, TRUE);	// delkey
}

// save the keys
void CLicensesDlg::SaveKeys()
{
	PROGRAM prg;
	KEY key;
	int nKeyStart=0, nLoop;

	for(nLoop=0; nLoop<m_nSelProgram; nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);
		nKeyStart += prg.nKeys;
	}
	prg = theApp.m_caPrograms.GetAt(m_nSelProgram);


	CXTPReportRecords* pRecords = m_wndReport.GetRecords();
	CString csBuf;

	for(nLoop=0; nLoop<prg.nKeys; nLoop++)
	{
		key = theApp.m_caKeys.GetAt(nKeyStart + nLoop);

		// serial
		csBuf = pRecords->GetAt(nLoop)->GetItem(1)->GetCaption(0);
		key.nSerial = _tstoi(csBuf);

		// level
		csBuf = pRecords->GetAt(nLoop)->GetItem(2)->GetCaption(0);
		key.nLevel = _tstoi(csBuf);

		// code
		csBuf = pRecords->GetAt(nLoop)->GetItem(3)->GetCaption(0);
		key.nCode = _tstoi(csBuf);

		// comment
		csBuf = pRecords->GetAt(nLoop)->GetItem(4)->GetCaption(0);
		key.csComment = csBuf;

		theApp.m_caKeys.SetAt(nKeyStart + nLoop, key);
	}
}

// user has changed a value
void CLicensesDlg::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify != NULL);

	m_bModified = TRUE;
}

// user wants to send a selected program.
void CLicensesDlg::OnBnClickedSend()
{
//	CXTPReportRow* pRow = m_wndReport.GetFocusedRow();

	int nItem = 0, nTmp=0, nTmp2=0;
	CString csBuf;

	if(pDoc->m_nType == 0)
	{
		nTmp = theApp.m_nSelProgramDP;
		nTmp2 = theApp.m_nSelLangDP;
	}
	else if(pDoc->m_nType == 1)
	{
		nTmp = theApp.m_nSelProgramMC;
		nTmp2 = theApp.m_nSelLangMC;
	}

	PROGRAM prg = theApp.m_caPrograms.GetAt(nTmp);
	LANGUAGE lng = theApp.m_caLanguages.GetAt(nTmp2);
	KEY key;

	// check which keys, if any, to send with the program.
	BOOL bSel=FALSE;
	int nKey = -1, nSerial = -1, nLevel = -1;

	CXTPReportRecords* pRecords = m_wndReport.GetRecords();
	for(int nLoop=0; nLoop<pRecords->GetCount(); nLoop++)
	{
		CXTPReportRecord* pRecord = pRecords->GetAt(nLoop);
		CXTPReportRecordItem* pItem = pRecord->GetItem(0);

		if(pItem->IsChecked() == TRUE)
		{
			bSel = TRUE;

			// get the key at this index and send it with the file.
			csBuf = pRecord->GetItem(1)->GetCaption(0);
			nSerial = _tstoi(csBuf);

			csBuf = pRecord->GetItem(2)->GetCaption(0);
			nLevel = _tstoi(csBuf);

			csBuf = pRecord->GetItem(3)->GetCaption(0);
			nKey = _tstoi(csBuf);

			// create the transfer-thread and show the GUI.
			CTransferDlg dlg;
			dlg.m_csFilename = lng.csPath;
			dlg.m_csFilenameShort = lng.csPath; //prg.csName;
			dlg.m_nLength = 0;
			dlg.m_nDevice = pDoc->m_nType;	// which device?
			dlg.m_nType = 0;				// kermit
			dlg.m_nDirection = 0;			// upload
			
			dlg.m_nCode = nKey;
			dlg.m_nSerial = nSerial;
			dlg.m_nProdID = prg.nID;
			dlg.m_nLevel = nLevel;

			if(pDoc->m_nType == 0) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csDP_Filepath, prg.nID);	// digitechpro
			else if(pDoc->m_nType == 1) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csMC_Filepath, prg.nID);	// mantax computer
			else if(pDoc->m_nType == 2) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csD_Filepath, prg.nID);	// digitech
			int nRet = dlg.DoModal();
		}
	}

	// create the transfer-thread and show the GUI.
	if(bSel == FALSE)
	{
		CTransferDlg dlg;
		dlg.m_csFilename = lng.csPath;
		dlg.m_csFilenameShort = lng.csPath; //prg.csName;
		dlg.m_nLength = 0;
		dlg.m_nDevice = pDoc->m_nType;	// which device?
		dlg.m_nType = 0;		// kermit
		dlg.m_nDirection = 0;	// upload
		dlg.m_nCode = -1;
		dlg.m_nSerial = -1;
		if(pDoc->m_nType == 0) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csDP_Filepath, prg.nID);	// digitechpro
		else if(pDoc->m_nType == 1) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csMC_Filepath, prg.nID);	// mantax computer
		else if(pDoc->m_nType == 2) dlg.m_csDestpath.Format(_T("%s%d\\"), theApp.m_csD_Filepath, prg.nID);	// digitech
		int nRet = dlg.DoModal();
	}
}

void CLicensesDlg::OnBnClickedButtonAttach()
{
	int nItem = 0, nTmp=0, nTmp2=0;
	CString csBuf, csPath;

	if(pDoc->m_nType == 0)	// digitechpro
	{
		nTmp = theApp.m_nSelProgramDP;
		nTmp2 = theApp.m_nSelLangDP;
		csPath = theApp.m_csDP_Filepath;
	}
	else if(pDoc->m_nType == 1)	// mantax computer
	{
		nTmp = theApp.m_nSelProgramMC;
		nTmp2 = theApp.m_nSelLangMC;
		csPath = theApp.m_csDP_Filepath;
	}

	PROGRAM prg = theApp.m_caPrograms.GetAt(nTmp);
	LANGUAGE lng = theApp.m_caLanguages.GetAt(nTmp2);

	csPath.Format(_T("%s%d\\"), csPath, prg.nID);

	
	// get documents out of the doc-string
	CArray<CString, CString> caDocFiles;
	int nStart = 0;
	int nFound = 0;
	while( (nFound = lng.csDoc.Find(_T(";"), nStart)) != -1)
	{
		csBuf = lng.csDoc.Mid(nStart, nFound - nStart);
		caDocFiles.Add(csBuf);

		nStart = nFound + 1;
	};

	// show a chooser
	CString csDocFile;
	if(caDocFiles.GetSize() > 1)
	{
		// ask about which document to show
		// display a popup-menu with the different alternatives (documents).
		POINT point;
		CMenu menuPopup;
		menuPopup.CreatePopupMenu();

		// add the files to the popup-menu
		for(int nLoop=0; nLoop<caDocFiles.GetSize(); nLoop++)
		{
			menuPopup.AppendMenu(MF_STRING, nLoop+1, (LPCTSTR)caDocFiles.GetAt(nLoop));
		}

		GetCursorPos(&point);
		int nRet = ::TrackPopupMenu(menuPopup.GetSafeHmenu(), TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, NULL, this->GetSafeHwnd(), NULL);
		menuPopup.DestroyMenu();

		if(nRet == 0) return;
		
		csDocFile = caDocFiles.GetAt(nRet-1);
	}
	else
	{
		csDocFile = caDocFiles.GetAt(0);
	}


	CString csFile;
	csFile.Format(_T("explorer.exe \"%s%s\""), csPath, csDocFile); //lng.csDoc);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	if( !CreateProcess( NULL, // No module name (use command line). 
		csFile.GetBuffer(0), // Command line. 
		NULL,             // Process handle not inheritable. 
		NULL,             // Thread handle not inheritable. 
		FALSE,            // Set handle inheritance to FALSE. 
		0,                // No creation flags. 
		NULL,             // Use parent's environment block. 
		NULL,             // Use parent's starting directory. 
		&si,              // Pointer to STARTUPINFO structure.
		&pi )             // Pointer to PROCESS_INFORMATION structure.
		) 
	{
		MessageBox(_T("Error opening explorer.exe!"), _T("Error"), MB_ICONSTOP);
	}

}

void CLicensesDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
    XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (!pItemNotify->pRow || !pItemNotify->pColumn)
		return;

	if (pItemNotify->pColumn->GetItemIndex() == 0)	// is it the checkbox?
	{
		m_wndReport.Populate();
	}
} 

void CLicensesDlg::OnDestroy()
{
	if(m_bModified == TRUE)
	{
		SaveKeys();
	}

	CView::OnDestroy();
}

// navigation
void CLicensesDlg::OnNavigate(int nID)
{
	int nTmp, nTmp2;
	CString csPath;
	PROGRAM prg;
	LANGUAGE lng;

	if(pDoc->m_nType == 0)	// digitechpro
	{
		nTmp = theApp.m_nSelProgramDP;
		nTmp2 = theApp.m_nSelLangDP;
		csPath = theApp.m_csDP_Filepath;
	}
	else if(pDoc->m_nType == 1)	// mantax computer
	{
		nTmp = theApp.m_nSelProgramMC;
		nTmp2 = theApp.m_nSelLangMC;
		csPath = theApp.m_csMC_Filepath;
	}

	prg = theApp.m_caPrograms.GetAt(nTmp);
	lng = theApp.m_caLanguages.GetAt(nTmp2);

	switch(nID)
	{
		case ID_DBNAVIG_START:
			nTmp2 = 0;
		break;

		case ID_DBNAVIG_NEXT:
			nTmp2++;
		break;

		case ID_DBNAVIG_PREV:
			nTmp2--;
		break;

		case ID_DBNAVIG_END:
			nTmp2 = theApp.m_caLanguages.GetSize()-1;
		break;
	};

	// find which program that has this language
	int nLang=0;
	for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);
		nLang += prg.nLangs;

		if(nTmp2 < nLang)
		{
			nTmp = nLoop;
			break;
		}
	}

	if(pDoc->m_nType == 0)	// digitechpro
	{
		theApp.m_nSelProgramDP = nTmp;
		theApp.m_nSelLangDP = nTmp2;
	}
	else if(pDoc->m_nType == 1)	// mantax computer
	{
		theApp.m_nSelProgramMC = nTmp;
		theApp.m_nSelLangMC = nTmp2;
	}
	
	OnSetFocus(this);
}
