// TransferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "RecvdataDlg.h"
#include "TransferDlg.h"
#include "XBrowseForFolder.h"
#include "Usefull.h"
#include "ASCIIDlg.h"

IMPLEMENT_DYNCREATE(CRecvdataFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CRecvdataFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CRecvdataFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecvdataFrame construction/destruction

CRecvdataFrame::CRecvdataFrame()
{
	m_bOnce = TRUE;
}

CRecvdataFrame::~CRecvdataFrame()
{
}

BOOL CRecvdataFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CRecvdataFrame diagnostics

#ifdef _DEBUG
void CRecvdataFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CRecvdataFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRecvdataFrame message handlers

int CRecvdataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CRecvdataFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Recvdata"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

LRESULT CRecvdataFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CRecvdataFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Recvdata"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CRecvdataFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CRecvdataDlg

IMPLEMENT_DYNCREATE(CRecvdataDlg, CXTResizeFormView)

CRecvdataDlg::CRecvdataDlg()
	: CXTResizeFormView(CRecvdataDlg::IDD)
{
	//{{AFX_DATA_INIT(CRecvdataDlg)
	//}}AFX_DATA_INIT
	
	m_csFilename = _T("");
	m_csFilenameShort = _T("");
	m_bQuit = FALSE;

	// dialog text
	m_csFile = _T("");
	m_csComport = _T("");
	m_csBaudrate = _T("");
	m_csLicense = _T("");
	m_csStatus = _T("");
	m_csPath = _T("");
	m_nPos = 0;

	m_bFOO = FALSE;
	m_bFirst = TRUE;
}

CRecvdataDlg::~CRecvdataDlg()
{
}

void CRecvdataDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PROGRESS_TRANSFER, m_cProgress);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_FILE2, m_csFile);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_COMPORT2, m_csComport);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_BAUDRATE2, m_csBaudrate);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_LICENSE2, m_csLicense);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_STATUS2, m_csStatus);
	DDX_Text(pDX, IDC_STATIC_TRANSFER_PATH2, m_csPath);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CRecvdataDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramsView)
	//}}AFX_MSG_MAP
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(IDC_BUTTON_TRANSFER, OnBnClickedButtonTransfer)

	ON_MESSAGE(WM_COM_PACKET, OnPacket)
	ON_MESSAGE(WM_COM_LENGTH, OnLength)
	ON_MESSAGE(WM_COM_DONE, OnDone)
	ON_MESSAGE(WM_COM_FILE, OnFile)
	ON_MESSAGE(WM_COM_ASCII, OnOpenAscii)
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()

LRESULT CRecvdataDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	int nPageID;

	if(pDoc->m_nType == 0)	// DigitechPro
	{
		nPageID = COMMRECEIVE_FILE;
	}
	else if(pDoc->m_nType == 1)	// Mantax computer
	{
		nPageID = COMMRECEIVE_FILE;
	}
	else if(pDoc->m_nType == 2)	// Digitech
	{
	}

	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, nPageID);
	return 0;
}

// CRecvdataDlg diagnostics
#ifdef _DEBUG
void CRecvdataDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CRecvdataDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CRecvdataDlg message handlers
BOOL CRecvdataDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRecvdataDlg::OnDestroy()
{
	// Abort the filetransfer and close the dialog.
	if(m_bQuit == FALSE && m_pCComThread != NULL)
	{
		m_pCComThread->Quit();
		m_pCComThread = NULL;
	}

	CView::OnDestroy();
}

void CRecvdataDlg::OnInitialUpdate()
{
	m_pCComThread = NULL;

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	if(pDoc->m_nType == 0)	// DigitechPro
	{
		m_csFilename = theApp.m_csDP_Downloadpath;	// digitechpro
		m_csBaudrate = theApp.m_csDP_Baudrate;
		m_csComport = theApp.m_csDP_Comport;

		PORTS port;
		CString csPort = theApp.m_csDP_Comport;
		for(int nLoop=0; nLoop<theApp.m_caComports.GetSize(); nLoop++)
		{
			port = theApp.m_caComports.GetAt(nLoop);
			if(port.csPort == m_csComport && port.nDP == 1)
			{
				m_csComport += _T(" (DP)");
				break;
			}
		}
	}
	else if(pDoc->m_nType == 1)	// Mantax computer
	{
		m_csFilename = theApp.m_csMC_Downloadpath;	// mantax computer
		m_csBaudrate = theApp.m_csMC_Baudrate;
		m_csComport = theApp.m_csMC_Comport;
	}
	else if(pDoc->m_nType == 2)	// Digitech
	{
		m_csFilename = theApp.m_csD_Downloadpath;	// digitech
		m_csBaudrate = theApp.m_csD_Baudrate;
		m_csComport = theApp.m_csD_Comport;
	}

	if(m_csFilename.GetAt(m_csFilename.GetLength()-1) != '\\')
		m_csFilename += _T("\\");


	m_csStatus.Format(_T("%s"), g_pXML->str(1029));


	CXTResizeFormView::OnInitialUpdate();


	SetDlgItemText(IDC_BUTTON_TRANSFER, g_pXML->str(1023));
	SetDlgItemText(IDC_STATIC_TRANSFER_FILE, g_pXML->str(1067));
	SetDlgItemText(IDC_STATIC_TRANSFER_COMPORT, g_pXML->str(1068));
	SetDlgItemText(IDC_STATIC_TRANSFER_BAUDRATE, g_pXML->str(1069));
	SetDlgItemText(IDC_STATIC_TRANSFER_LICENSE, g_pXML->str(1070));
	SetDlgItemText(IDC_STATIC_TRANSFER_STATUS, g_pXML->str(1071));
	SetDlgItemText(IDC_STATIC_TRANSFER_PATH, g_pXML->str(1124));


	SetResize(IDC_PROGRESS_TRANSFER, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_FILE2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_COMPORT2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_BAUDRATE2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_LICENSE2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_STATUS2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_STATIC_TRANSFER_PATH2, SZ_TOP_LEFT, SZ_TOP_RIGHT);
	SetResize(IDC_BUTTON_TRANSFER, SZ_TOP_LEFT, SZ_TOP_RIGHT);

	m_cProgress.SetRange32(0, 1);
	m_cProgress.SetPos(0);

	ResizeParentToFit();
}

void CRecvdataDlg::OnSetFocus(CWnd* pOldWnd)
{
	if(m_bFirst == TRUE)
	{
		m_bFirst = FALSE;

		if(pDoc->m_csExtraArg == _T(""))
		{
			TCHAR szFolder[MAX_PATH*2];
			szFolder[0] = _T('\0');
			BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
				m_csFilename,
				szFolder,
				sizeof(szFolder)/sizeof(TCHAR)-2);

			if(bRet == TRUE)
			{
				m_csFilename = szFolder;
				if(m_csFilename.GetAt(m_csFilename.GetLength()-1) != '\\')
					m_csFilename += _T("\\");

				if(pDoc->m_nType == 0)	// DigitechPro
				{
					theApp.m_csDP_Downloadpath = m_csFilename;	// digitechpro
				}
				else if(pDoc->m_nType == 1)	// Mantax computer
				{
					theApp.m_csMC_Downloadpath = m_csFilename;	// mantax computer
				}
				else if(pDoc->m_nType == 2)	// Digitech
				{
					theApp.m_csD_Downloadpath = m_csFilename;	// digitech
				}

				m_csPath = m_csFilename;
				UpdateData(FALSE);

				OpenComThread();
			}
			else
			{
				m_bFOO = TRUE;
			}
		}
		else
		{
			m_csFilename = pDoc->m_csExtraArg;
			if(m_csFilename.GetAt(m_csFilename.GetLength()-1) != '\\')
				m_csFilename += _T("\\");

			m_csPath = m_csFilename;
			UpdateData(FALSE);

			OpenComThread();
		}
	}

	if(m_bFOO == TRUE)
	{
		GetParentFrame()->PostMessage(WM_CLOSE);
		return;
	}


	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

// user has clicked the Abort-button
void CRecvdataDlg::OnBnClickedButtonTransfer()
{
	if(m_bQuit == TRUE) return;
	m_bQuit = TRUE;

	// Abort the filetransfer and close the dialog.
	m_pCComThread->Quit();
	m_pCComThread = NULL;

	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(FALSE);
	GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

// we have recieved a packet.
LRESULT CRecvdataDlg::OnPacket(WPARAM wParm, LPARAM lParm)
{
	m_nPos = (int)wParm;

	m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1030), m_nPos);

	UpdateData(FALSE);
	
	return 0;
}

// we have recieved a length.
LRESULT CRecvdataDlg::OnLength(WPARAM wParm, LPARAM lParm)
{
	m_csStatus.Format(_T("%s (%d Bytes)"), g_pXML->str(1030), (int)wParm);

	m_cProgress.SetRange32(0, (int)wParm);
	m_cProgress.SetPos(0);

	UpdateData(FALSE);

	return 0;
}

// the transfer is done
LRESULT CRecvdataDlg::OnDone(WPARAM wParm, LPARAM lParm)
{
	BOOL bError = FALSE;

	// transfer ended, tell why.
	switch(wParm)
	{
		case 0:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1066));
			break;
		case 1:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1031));
			break;
		case 2:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1032));
			bError = TRUE;
			break;
		case 3:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1033));
			bError = TRUE;
			break;
		case 4:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1034));
			bError = TRUE;
			break;
		case 5:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1035));
			break;
		case 6:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1036));
			bError = TRUE;
			break;
		case 7:
			m_csStatus.Format(_T("%s"),	g_pXML->str(1037));
			bError = TRUE;
			break;
	}

	if(bError == TRUE && m_bQuit == FALSE)
	{
		OnBnClickedButtonTransfer();
	}

	UpdateData(FALSE);

	return 0;
}

// we got the filename we are receiving
LRESULT CRecvdataDlg::OnFile(WPARAM wParm, LPARAM lParm)
{
	GetDlgItem(IDC_BUTTON_TRANSFER)->EnableWindow(TRUE);

	m_csFilenameShort.Format(_T("%S"), lParm);
	m_csFile.Format(_T("%s"), m_csFilenameShort);
	m_csStatus.Format(_T("%s (0 Bytes)"), g_pXML->str(1030));

	UpdateData(FALSE);

	return 0;
}

LRESULT CRecvdataDlg::OnOpenAscii(WPARAM wParm, LPARAM lParm)
{
	// open the ASCII-dlg
	CDocTemplate *pTemplate;
	CMDISampleSuiteDoc *pDocument;
	CWinApp* pApp = AfxGetApp();
	CString csDocName, csResStr, csDocTitle;

	csResStr.LoadString(IDR_ASCII);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(csDocName, CDocTemplate::docName);
		ASSERT(pTemplate != NULL);
		csDocName = '\n' + csDocName;

		if (pTemplate && csDocName.Compare(csResStr) == 0)
		{
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			while(posDOC != NULL)
			{
				pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
				POSITION pos = pDocument->GetFirstViewPosition();
				if(pos != NULL)
				{
					CView* pView = pDocument->GetNextView(pos);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					posDOC = (POSITION)1;

					GetParentFrame()->ShowWindow(SW_HIDE);

					break;
				}
			}

			if(posDOC == NULL)
			{
				pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
				CString sDocTitle;
				pDocument->SetTitle(g_pXML->str(IDR_ASCII));

				POSITION pos = pDocument->GetFirstViewPosition();
				CView* pView = pDocument->GetNextView(pos);
				((CASCIIDlg*)pView)->m_hParentWnd = GetSafeHwnd();
				m_pCComThread->m_hWnd2 = pView->GetSafeHwnd();
				((CASCIIDlg*)pView)->m_csDefaultpath = m_csPath;
				GetParentFrame()->ShowWindow(SW_HIDE);
			}
		}
	}


	return 0;
}

// start the receive-thread
int CRecvdataDlg::OpenComThread()
{
	if(m_pCComThread != NULL) return FALSE;

	int pos = 0;
	CString csBuf = m_csComport.Tokenize(_T("COM:"), pos);

 	m_pCComThread = (CComThread*) ::AfxBeginThread( RUNTIME_CLASS( CComThread ), 0);
	m_pCComThread->m_hWnd = this->m_hWnd;
	m_pCComThread->m_csFilename = m_csFilename;
	m_pCComThread->m_csFilenameShort = m_csFilenameShort;
	m_pCComThread->m_nDirection = 1;	// down
	m_pCComThread->m_csDestpath = m_csFilename;
	m_pCComThread->m_nType = 0;	// kermit
	m_pCComThread->m_hWnd = GetSafeHwnd();
	m_pCComThread->m_nBaud = CStringToInt(m_csBaudrate);
	m_pCComThread->m_nComport = CStringToInt(csBuf);
	m_pCComThread->DoContinuous();

	return TRUE;
}

void CRecvdataDlg::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}