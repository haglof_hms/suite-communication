// ProgramsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "ProgramsDlg.h"
#include "LicensesDlg.h"
#include "SampleSuiteForms.h"
#include "Usefull.h"
#include "TransferDlg.h"
#include "MessageRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CContactsReportFilterEditControl

IMPLEMENT_DYNCREATE(CContactsReportFilterEditControl, CXTPReportFilterEditControl)
BEGIN_MESSAGE_MAP(CContactsReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CContactsReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	if (pWnd != NULL)
	{
		CString S;
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != _T(""));
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}


IMPLEMENT_DYNCREATE(CProgramsFrame, CMDIChildWnd)
BEGIN_MESSAGE_MAP(CProgramsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProgramsFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
END_MESSAGE_MAP()


static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
};

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame construction/destruction

CProgramsFrame::CProgramsFrame()
{
	m_bOnce = TRUE;
	m_bEnableTBBTNFilterOff = FALSE;
}

void CProgramsFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

CProgramsFrame::~CProgramsFrame()
{
}

LRESULT CProgramsFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;

		case ID_DBNAVIG_START:
			((CProgramsDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_NEXT:
			((CProgramsDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_PREV:
			((CProgramsDlg*)GetActiveView())->OnNavigate(wParm);
		break;

		case ID_DBNAVIG_END:
			((CProgramsDlg*)GetActiveView())->OnNavigate(wParm);
		break;
	};

	return 0;
}

void CProgramsFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CProgramsFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Programs"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CProgramsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Programs"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

BOOL CProgramsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CProgramsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame diagnostics

#ifdef _DEBUG
void CProgramsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CProgramsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame message handlers

int CProgramsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_PROGRAMS); //, FALSE);


	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	m_wndFilterEdit.SetWindowText(g_pXML->str(1127));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	// can not debug this one!!!!
	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));



	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;

		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR_PROGRAMS)
				{
						// view licenses/info
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(1000));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 20);	// info
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_PROGRAM_OPEN);

						// download program
						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(1002));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 41);	// udaterawww
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_PROGRAM_DOWNLOAD);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(1127));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 39);	// tabellfilter
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(TRUE);
						pCtrl->SetID(ID_TBBTN_FILTER);
						
						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(g_pXML->str(1128));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 8);	// endfilter
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetID(ID_TBBTN_FILTER_OFF);

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	if (!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	UpdateWindow();

	return 0;
}

void CProgramsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

void CProgramsFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

/*---------------------------------------------------------------------*/


/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg

IMPLEMENT_DYNCREATE(CProgramsDlg, CXTPReportView)

BEGIN_MESSAGE_MAP(CProgramsDlg, CXTPReportView)
	//{{AFX_MSG_MAP(CProgramsDlg)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_OPEN, OnBnClickedLicense)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_DOWNLOAD, OnBnClickedDownload)
	ON_COMMAND(ID_TBBTN_FILTER, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	//}}AFX_MSG_MAP
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_MESSAGE(WM_HELP, OnCommandHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg construction/destruction

LRESULT CProgramsDlg::OnCommandHelp(WPARAM wParam, LPARAM lParam)
{
	CString csBuf;
	csBuf.Format(_T("%s\\Help\\CommSuite%s.chm"), getProgDir(), getLangSet());
	::HtmlHelp(GetDesktopWindow()->m_hWnd, csBuf, HH_HELP_CONTEXT, COMMMANAGE_PROGRAMS);
	return 0;
}

CProgramsDlg::CProgramsDlg()
{
	m_nVersionXML = 0;
	m_csDateXML = _T("");
}

CProgramsDlg::~CProgramsDlg()
{
}

BOOL CProgramsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CView::PreCreateWindow(cs))
		return FALSE;

	return TRUE;

}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg diagnostics

#ifdef _DEBUG
void CProgramsDlg::AssertValid() const
{
	CView::AssertValid();
}

void CProgramsDlg::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

/*CReportSampleDoc* CProgramsDlg::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CReportSampleDoc)));
	return (CReportSampleDoc*)m_pDocument;
}*/
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg message handlers

int CProgramsDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPReportView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CProgramsDlg::OnDestroy()
{
	SaveXML();

	SaveReportState();

	CView::OnDestroy();
}


void CProgramsDlg::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	CXTPReportControl& wndReport = GetReportCtrl();

	if (m_ilIcons.Create(16,16, ILC_COLOR24|ILC_MASK, 0, 1))
	{
		CBitmap bitmap;
		VERIFY(bitmap.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bitmap, RGB(255, 0, 255));	
		wndReport.SetImageList(&m_ilIcons);
	}

	//
	//  Add columns
	//
	CXTPReportColumn* pCol;
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_FLAG, _T(""), 10));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_PROGRAM, g_pXML->str(1003), 100));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_VERSION, g_pXML->str(1004), 101));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_NEWVERSION, g_pXML->str(1005), 102));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_LANGUAGE, g_pXML->str(1006), 103));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_TLA, g_pXML->str(1006), 104));
	pCol->AllowRemove(FALSE);

	//
	//  Add records
	//
	AddSampleRecords();

	// Set the report to group by Language
	pCol = GetReportCtrl().GetColumns()->Find(COLUMN_LANGUAGE);
	GetReportCtrl().GetColumns()->GetGroupsOrder()->Clear();
	GetReportCtrl().GetColumns()->GetGroupsOrder()->Add(pCol);
	pCol = GetReportCtrl().GetColumns()->Find(COLUMN_TLA);
	pCol->SetVisible(FALSE);
	GetReportCtrl().Populate();
	GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());


	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &((CProgramsFrame*)GetParentFrame())->m_wndFilterEdit);
		m_wndFilterEdit.pWnd = (CProgramsFrame*)GetParentFrame();
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}


	LoadReportState();
	wndReport.CollapseAll();

	CString csBuf;
	csBuf.Format(_T("%s %d.%02d (%s)"), g_pXML->str(1024), m_nVersionXML/100, m_nVersionXML%100, m_csDateXML);
	((CProgramsFrame*)GetParentFrame())->m_wndStatusBar.SetWindowText(csBuf);
}

void CProgramsDlg::LoadReportState()
{
	UINT nBytes = 0;
	LPBYTE pData = 0;

	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Programs"), REG_ROOT);
	if (!regGetBin(csBuf, _T(""), _T("ReportControl"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

	// Get filtertext for this Report
	CString sFilterText = regGetStr(csBuf, _T(""), _T("FilterText"), _T(""));
	GetReportCtrl().SetFilterText(sFilterText);
	CProgramsFrame* pWnd = (CProgramsFrame*)GetParentFrame();
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
	GetReportCtrl().Populate();
}

void CProgramsDlg::SaveReportState()
{
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Communication\\Dialogs\\Programs"), REG_ROOT);
	regSetBin(csBuf, _T(""), _T("ReportControl"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	CString sFilterText = GetReportCtrl().GetFilterText();
	regSetStr(csBuf, _T(""), _T("FilterText"), sFilterText);
}

CString LoadResourceString(UINT nID)
{
	CString str;
	VERIFY(str.LoadString(nID));
	return str;
}

void CProgramsDlg::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportControl& wndReport = GetReportCtrl();
	CXTPReportRow* pRow = wndReport.GetFocusedRow();
	CXTPReportRows* pRows = GetReportCtrl().GetRows();

	if (!pRow) return;

	int nItem = pRow->GetIndex();

	PROGRAM prg;
	LANGUAGE lng;
	CString csName, csLang;
	CString csVersion, csVersionWeb;
	int nLoop, nLoop2, nLangIndex=0;
	BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

	CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRow->GetRecord());
	if (pRecord)
	{
		CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
		csName = pItem->GetCaption(0);

		pItem = pRecord->GetItem(COLUMN_VERSION);	// version
		csVersion = pItem->GetCaption(0);
		csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

		pItem = pRecord->GetItem(COLUMN_NEWVERSION);	// versionweb
		csVersionWeb = pItem->GetCaption(0);
		csVersionWeb = csVersionWeb.Left(csVersionWeb.Find(_T(" ("), 0));

		pItem = pRecord->GetItem(COLUMN_TLA);	// language
		csLang = pItem->GetCaption(0);


		// search for the program
		for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

			if(prg.nID == pRecord->m_nProgID &&
				prg.csName == csName)
			{
				// search for the language
				for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
				{
					lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

					if(lng.csLang == csLang)
					{
						bFoundLang = TRUE;
						break;
					}
				}

				bFoundPrg = TRUE;
				break;
			}
	
			nLangIndex += prg.nLangs;
		}

		if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

		// set icons according to the choice made.
		// enable the download- and info-buttons.
		((CProgramsFrame*)GetParent())->SetButtonEnabled(0, TRUE);	// info

		// do we have an newer version to download?
//		if(lng.nVersionWeb != 0 /*&& (lng.nVersion < lng.nVersionWeb)*/)
		if(lng.csVersionWeb != _T(""))
			((CProgramsFrame*)GetParent())->SetButtonEnabled(1, TRUE);	// download from web
		else
			((CProgramsFrame*)GetParent())->SetButtonEnabled(1, FALSE);	// download from web


		if(pRows->GetCount() > 0)
		{
			if(nItem > 1)
			{
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, TRUE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, TRUE);
			}
			else
			{
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
			}

			if(nItem < pRows->GetCount()-1)
			{
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, TRUE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, TRUE);
			}
			else
			{
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
			}
		}
	}
}

void CProgramsDlg::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;

	if (lpNMKey->nVKey == VK_RETURN)
	{
		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, GetReportCtrl().GetFocusedRow()->GetRecord());
		if (pRecord)
		{
			if (pRecord->SetRead())
			{
				GetReportCtrl().Populate();
			}
		}
	}
}

void CProgramsDlg::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pRow)
	{
		int nItem = 0, nProg=0, nLang=0;
		CString csBuf;

		// find out which program we have chosen.
		nItem = pItemNotify->pRow->GetIndex();

		PROGRAM prg;
		LANGUAGE lng;
		CString csName, csLang;
		CString csVersion, csVersionWeb;
		int nLoop, nLoop2, nLangIndex=0;
		BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pItemNotify->pRow->GetRecord());
		if (pRecord)
		{
			CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
			csName = pItem->GetCaption(0);

			pItem = pRecord->GetItem(COLUMN_VERSION);	// version
			csVersion = pItem->GetCaption(0);
			csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_NEWVERSION);	// versionweb
			csVersionWeb = pItem->GetCaption(0);
			csVersionWeb = csVersionWeb.Left(csVersionWeb.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_TLA);	// language
			csLang = pItem->GetCaption(0);

			// search for the program
			for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
			{
				prg = theApp.m_caPrograms.GetAt(nLoop);

				if(prg.nID == pRecord->m_nProgID &&
					prg.csName == csName)
				{
					// search for the language
					for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
					{
						lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

						if(lng.csVersion == csVersion &&
							lng.csVersionWeb == csVersionWeb &&
							lng.csLang == csLang)
						{
							nLang = nLangIndex + nLoop2;
							bFoundLang = TRUE;
							break;
						}
					}

					nProg = nLoop;
					bFoundPrg = TRUE;
					break;
				}

				nLangIndex += prg.nLangs;
			}

			if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

			// send the indexes to the info-dialog.
			if(pDoc->m_nType == 0)	// digitech pro
			{
				theApp.m_nSelProgramDP = nProg;
				theApp.m_nSelLangDP = nLang;
				theApp.m_csSelProgramDP = csName;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				theApp.m_nSelProgramMC = nProg;
				theApp.m_nSelLangMC = nLang;
				theApp.m_csSelProgramMC = csName;
			}
			theApp.m_nType = pDoc->m_nType;

			// open the info-dialog
			CDocTemplate *pTemplate;
			CWinApp* pApp = AfxGetApp();
			CString csDocName, csResStr, csDocTitle;
			csResStr.LoadString(IDR_LICENSES);

			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(csDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				csDocName = '\n' + csDocName;

				if (pTemplate && csDocName.Compare(csResStr) == 0)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while(posDOC != NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
						POSITION pos = pDocument->GetFirstViewPosition();
						if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
						{
							CView* pView = pDocument->GetNextView(pos);
							pView->GetParent()->BringWindowToTop();
							pView->GetParent()->SetFocus();
							posDOC = (POSITION)1;
							break;
						}
					}

					if(posDOC == NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
						CString sDocTitle;
						pDocument->SetTitle(g_pXML->str(1000));
						pDocument->m_nType = pDoc->m_nType;
					}
				}
			}

		}
	}
}

void CProgramsDlg::AddSampleRecords()
{
	CXTPReportControl& wndReport = GetReportCtrl();

	ParseXML();

	BOOL bDisplay;
	CStringArray caLangs;
	GetLangSettings(theApp.m_csDP_Langs, _T(";"), &caLangs);

	// loop through all programs
	PROGRAM prg;
	LANGUAGE lng;
	int nStartLang=0, nLang;
	CString csVer, csVerWeb, csLang, csBuf1, csBuf2;

	for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);

		if(prg.nProgtype == pDoc->m_nType)
		{
			// get the different versions/languages
			for(nLang=0; nLang<prg.nLangs; nLang++)
			{
				lng = theApp.m_caLanguages.GetAt(nStartLang + nLang);

				if(caLangs.GetSize() > 0)
				{
					bDisplay = FALSE;
					for(int nLoop=0; nLoop<caLangs.GetSize(); nLoop++)
					{
						if(lng.csLang.CompareNoCase(caLangs.GetAt(nLoop)) == 0)
						{
							bDisplay = TRUE;
						}
					}
				}
				else
				{
					bDisplay = TRUE;
				}


				if(bDisplay == TRUE)
				{
					csVer = lng.csVersion;
					if(lng.csVersionWeb != _T(""))
						csVerWeb = lng.csVersionWeb;

					csLang = m_cLocale.GetLangString(lng.csLang);

					
					csBuf1.Format(_T(" (%s)"), lng.csDate);
					csBuf2.Format(_T(" (%s)"), lng.csDateWeb);
					if(lng.csDate != "") csVer += csBuf1;
					if(lng.csDateWeb != "") csVerWeb += csBuf2;

					if(lng.bVisible == true || lng.csVersion != _T(""))	// only show a invisible program if we already have it.
					{
						if(lng.bLocal == TRUE)
							wndReport.AddRecord(new CMessageRecord(FALSE, prg.nID, prg.csName, csVer, csVerWeb, csLang, lng.csLang));
						else
							wndReport.AddRecord(new CMessageRecord(FALSE, prg.nID, prg.csName, _T(""), csVerWeb, csLang, lng.csLang));
					}
				}
			}
		}

		nStartLang += prg.nLangs;
	}
}

/*---------------------------------------------------------------------*/
int CProgramsDlg::GetLocalXML()
{
	theApp.m_caPrograms.RemoveAll();
	theApp.m_caLanguages.RemoveAll();
	theApp.m_caKeys.RemoveAll();

	CString csDestpath;
	if(pDoc->m_nType == 0) csDestpath = theApp.m_csDP_Filepath;	// digitechpro
	else if(pDoc->m_nType == 1) csDestpath = theApp.m_csMC_Filepath;	// mantax computer
	else if(pDoc->m_nType == 2) csDestpath = theApp.m_csD_Filepath;	// digitech

	XMLNode xNode = XMLNode::openFileHelper(csDestpath + _T("Programs.xml"), _T("XML"));

	if(xNode.isEmpty() != 1)
	{
		xNode = xNode.getChildNode(_T("Programs"));	// get the first tag, "Programs"

		if(xNode.isEmpty() != 1)
		{

			PROGRAM prg;
			KEY key;
			LANGUAGE lng;
			CString csID, csName, csVersion, csVersionWeb, csPath, csDesc, csBuf;
			XMLNode xChild, xKey, xLang;
			int i, iterator=0, j, jterator=0, nKeys=0, nLangs=0, nProgtype;

			// get the version and date of the XML
			csBuf = xNode.getChildNode(_T("Version"), 0).getText();
			if(csBuf != _T("")) m_nVersionXML = _tstoi(csBuf);

			csBuf = xNode.getChildNode(_T("Date")).getText();
			if(csBuf != _T("")) m_csDateXML = csBuf;


			int n = xNode.nChildNode(_T("Product"));
			for(i=0; i<n; i++)
			{
				prg.nKeys = 0;
				prg.nLangs = 0;

				xChild = xNode.getChildNode(_T("Product"), &iterator);
				csID = xChild.getChildNode(_T("ID")).getText();
				csName = xChild.getChildNode(_T("Name")).getText();
				csBuf = xChild.getChildNode(_T("Progtype")).getText();
				if(csBuf != _T("")) nProgtype = _tstoi(csBuf);

				nLangs = xChild.nChildNode(_T("Lang"));
				if(nLangs != 0)	// we have some langs
				{
					for(j=0; j<nLangs; j++)
					{
						xLang = xChild.getChildNode(_T("Lang"), &jterator);

						lng.csLang = xLang.getChildNode(_T("ID")).getText();
						csBuf = xLang.getChildNode(_T("Version")).getText();
						if(csBuf != _T(""))
						{
							lng.nVersion = (_tstof(m_cLocale.FixLocale(csBuf).GetBuffer()) * 10.0);
							lng.csVersion = csBuf;
						}
						else
						{
							lng.nVersion = 0;
							lng.csVersion = _T("");
						}
						lng.nVersionWeb = 0;
						lng.csVersionWeb = _T("");

						lng.csPath = xLang.getChildNode(_T("Filename")).getText();
						lng.csPathWeb = _T("");
						lng.csDesc = xLang.getChildNode(_T("Description")).getText();
						lng.csDate = xLang.getChildNode(_T("Date")).getText();
						csBuf = xLang.getChildNode(_T("Local")).getText();
						if(csBuf != _T("")) lng.bLocal = _tstoi(csBuf);
						else lng.bLocal = FALSE;
						csBuf = xLang.getChildNode(_T("Visible")).getText();
						if(csBuf != _T("")) lng.bVisible = _tstoi(csBuf);
						else lng.bVisible = true;

						lng.csDoc = _T("");
						lng.csDoc = xLang.getChildNode(_T("Doc")).getText();

						if(lng.bLocal == FALSE)
						{
							lng.nVersionWeb = lng.nVersion;
							lng.csVersionWeb = lng.csVersion;
							lng.nVersion = 0;
							lng.csVersion = _T("");
						}


						theApp.m_caLanguages.Add(lng);
						prg.nLangs++;
					}

					jterator = 0;
				}

				nKeys = xChild.nChildNode(_T("Key"));
				if(nKeys != 0)	// we have some keys
				{
					for(j=0; j<nKeys; j++)
					{
						xKey = xChild.getChildNode(_T("Key"), &jterator);
						key.nKey = 0;
						key.nLevel = 0;
						key.nSerial = 0;
						key.nCode = 0;
						key.csComment = _T("");

						csBuf = xKey.getChildNode(_T("ID")).getText();
						if(csBuf != _T("")) key.nKey = _tstoi(csBuf);

						csBuf = xKey.getChildNode(_T("Level")).getText();
						if(csBuf != _T("")) key.nLevel = _tstoi(csBuf);

						csBuf = xKey.getChildNode(_T("Serial")).getText();
						if(csBuf != _T("")) key.nSerial = _tstoi(csBuf);

						csBuf = xKey.getChildNode(_T("Code")).getText();
						if(csBuf != _T("")) key.nCode = _tstoi(csBuf);

						csBuf = xKey.getChildNode(_T("Comment")).getText();
						if(csBuf != _T("")) key.csComment = csBuf;

						theApp.m_caKeys.Add(key);
						prg.nKeys++;
					}

					jterator = 0;
				}

				prg.csName = csName;
				prg.nID = _tstoi(csID);
				prg.nProgtype = nProgtype;
				theApp.m_caPrograms.Add(prg);
			}
		}
	}

	return 0;
}

int CProgramsDlg::GetWebXML()
{
	// retrieve the web XML.
	W3Client w3;
	CString csBuf, csBuf2;
	int nRet = 0, nRet2 = 0;

	if(theApp.m_bLocal == TRUE)
	{
		CFile fh;
		if(fh.Open(theApp.m_csLocalpath + _T("programs.xml"), CFile::modeRead, 0))
		{
			fh.Read(csBuf.GetBufferSetLength(fh.GetLength()), fh.GetLength());
			fh.Close();
		}
	}
	else
	{
		if(!(w3.Connect(_T("http://") + theApp.m_csWebserver))) return -1;

		if(w3.Request(theApp.m_csWebpath + _T("programs.xml")))
		{
			if((nRet = w3.QueryResult()) == 200)
			{
				unsigned char tbuf[1026];

				while(nRet2 = w3.Response(tbuf, 1024))
				{
					tbuf[nRet2]   = '\0';
					tbuf[nRet2+1] = '\0';
					csBuf += (wchar_t*)tbuf;
				};
			}
		}
		w3.Close();

		if(nRet != 200)
		{
			if(nRet == 400)	// could not find "programs.xml" !
				MessageBox(g_pXML->str(1129), g_pXML->str(1101), MB_OK|MB_ICONERROR);

			return -1;
		}
	}

	// parse the buffer
	XMLResults pResults;
	XMLNode xNode = XMLNode::parseString(csBuf, _T("XML"), &pResults);
	if(pResults.error != eXMLErrorNone && pResults.error != eXMLErrorFileNotFound) // an error occured?
	{
		csBuf.Format(_T("Error on %d, %d.\r\n%s"), pResults.nLine, pResults.nColumn, XMLNode::getError(pResults.error));
		AfxMessageBox(csBuf);
	}

	if(xNode.isEmpty() != 1)
	{
		xNode = xNode.getChildNode(_T("Programs"));	// get the first tag, "Programs"

		if(xNode.isEmpty() != 1)
		{
			PROGRAM prgWeb, prgLocal;
			LANGUAGE lng;
			BOOL bFound;

			// get the version and date of the XML
			csBuf = xNode.getChildNode(_T("Version")).getText();
			if(csBuf != _T("")) m_nVersionXML = _tstoi(csBuf);

			csBuf = xNode.getChildNode(_T("Date")).getText();
			if(csBuf != _T("")) m_csDateXML = csBuf;


			int n = xNode.nChildNode(_T("Product"));

			CString csID, csName, csVersion, csPath, csDesc;
			XMLNode xChild, xKey, xLang;
			int i, iterator=0, jterator=0, nProgtype, nLoop, nLangIndex=0, j;

			for(i=0; i<n; i++)
			{
				xChild = xNode.getChildNode(_T("Product"), &iterator);

				csID = xChild.getChildNode(_T("ID")).getText();
				csName = xChild.getChildNode(_T("Name")).getText();
				csBuf = xChild.getChildNode(_T("Progtype")).getText();
				if(csBuf != _T("")) nProgtype = _tstoi(csBuf);

				prgWeb.nLangs = 0;
				prgWeb.nKeys = 0;
				prgWeb.csName = csName;
				if(csID != _T("")) prgWeb.nID = _tstoi(csID);
				prgWeb.nProgtype = nProgtype;
				prgWeb.nLangs = xChild.nChildNode(_T("Lang"));
				int nLangs = prgWeb.nLangs;

				bFound = FALSE;

				nLangIndex = 0;
				for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
				{
					prgLocal = theApp.m_caPrograms.GetAt(nLoop);

					if(prgLocal.nID == prgWeb.nID)	// same program?
					{
						bFound = TRUE;
						break;
					}

					nLangIndex += prgLocal.nLangs;
				}

				if(bFound == TRUE)	// we have found the program
				{
					if(prgWeb.nLangs != 0)	// we have some langs
					{
						for(j=0; j<prgWeb.nLangs; j++)
						{
							xLang = xChild.getChildNode(_T("Lang"), &jterator);


							if(j < prgLocal.nLangs)
								lng = theApp.m_caLanguages.GetAt(nLangIndex + j);
							else
							{
								lng.csDoc = _T("");
								lng.csPath = _T("");
								lng.csDesc = _T("");
								lng.csDate = _T("");
								lng.nVersion = 0;
								lng.csVersion = _T("");
							}

							lng.csLang = xLang.getChildNode(_T("ID")).getText();
							csBuf = xLang.getChildNode(_T("Version")).getText();
							if(csBuf != _T(""))
							{
								lng.nVersionWeb = (_tstof(m_cLocale.FixLocale(csBuf)) * 10.0);
								lng.csVersionWeb = csBuf;
							}

							lng.csPathWeb = xLang.getChildNode(_T("Filename")).getText();
							lng.csDesc = xLang.getChildNode(_T("Description")).getText();
							lng.csDateWeb = xLang.getChildNode(_T("Date")).getText();
							csBuf = xLang.getChildNode(_T("Visible")).getText();
							if(csBuf != _T("")) lng.bVisible = _tstoi(csBuf);
							else lng.bVisible = true;

							if(lng.csLang != _T("") || lng.csPathWeb != _T(""))
							{
								if(j < prgLocal.nLangs)
									theApp.m_caLanguages.SetAt(nLangIndex + j, lng);
								else
									theApp.m_caLanguages.InsertAt(nLangIndex + j, lng);
							}
							else
							{
								nLangs--;
							}
						}

						jterator = 0;
					}

					prgLocal.nLangs = nLangs;

					// update the program
					theApp.m_caPrograms.SetAt(nLoop, prgLocal);
				}
				else if(bFound == FALSE)	// program doesn't exist in local file.
				{
					if(prgWeb.nLangs != 0)	// we have some langs
					{
						for(j=0; j<prgWeb.nLangs; j++)
						{
							xLang = xChild.getChildNode(_T("Lang"), &jterator);

							lng.csDoc = _T("");
							lng.csLang = xLang.getChildNode(_T("ID")).getText();
							lng.nVersion = 0;
							lng.csVersion = _T("");
							csBuf = xLang.getChildNode(_T("Version")).getText();
							if(csBuf != _T(""))
							{
								lng.nVersionWeb = (_tstof(m_cLocale.FixLocale(csBuf)) * 10.0);
								lng.csVersionWeb = csBuf;
							}
							lng.csPath = _T("");
							lng.csPathWeb = xLang.getChildNode(_T("Filename")).getText();
							lng.csDesc = xLang.getChildNode(_T("Description")).getText();
							lng.csDateWeb = xLang.getChildNode(_T("Date")).getText();
							lng.bLocal = FALSE;
							if(csBuf != _T("")) lng.bVisible = _tstoi(csBuf);
							else lng.bVisible = true;

							if(lng.csLang != _T("") || lng.csPathWeb != _T(""))
								theApp.m_caLanguages.InsertAt(nLangIndex + j, lng);
							else
								nLangs--;
						}

						jterator = 0;
					}

					prgWeb.nLangs = nLangs;

					// add it
					theApp.m_caPrograms.Add(prgWeb);
				}
			}
		}
	}

	return 0;
}

// used to get both the local and the remote (if possible) XML-files.
int CProgramsDlg::ParseXML()
{
	GetLocalXML();

	if(GetWebXML() == -1)
	{
		return FALSE;
	}

	SaveXML();

	return TRUE;
}


int CProgramsDlg::SaveXML()
{
	CFile fh;

	CString csDestpath;
	if(pDoc->m_nType == 0) csDestpath = theApp.m_csDP_Filepath;	// digitechpro
	else if(pDoc->m_nType == 1) csDestpath = theApp.m_csMC_Filepath;	// mantax computer
	else if(pDoc->m_nType == 2) csDestpath = theApp.m_csD_Filepath;	// digitech

	CString csFile = csDestpath + _T("Programs.xml");

	// make a backup-copy of the xml first, in case of failure
	CopyFile(csFile, csFile + _T(".bak"), FALSE);


	if(fh.Open(csFile, CFile::modeCreate|CFile::modeWrite) != 0)
	{
		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		int nKey = 0, nLoop2, nLoop3, nLang=0;
		TCHAR szBuf[1024];

		const static WORD UNICODE_MARK = 0xfeff;
		fh.Write(&UNICODE_MARK, 2);

		_stprintf(szBuf, _T("<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n<Programs>\r\n\t<Path>%s</Path>\r\n\t<Version>%d</Version>\r\n\t<Date>%s</Date>\r\n"),
			csFile, m_nVersionXML, m_csDateXML);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)	// loop through all programs
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

			_stprintf(szBuf, _T("\t<Product>\r\n\t\t<ID>%d</ID>\r\n\t\t<Name>%s</Name>\r\n\t\t<Progtype>%d</Progtype>\r\n"),
				prg.nID,
				prg.csName,
				prg.nProgtype);
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

			if(prg.nLangs != 0)
			{
				for(nLoop3=nLang; nLoop3<(nLang+prg.nLangs); nLoop3++)
				{
					lng = theApp.m_caLanguages.GetAt(nLoop3);

					_stprintf(szBuf, _T("\t\t<Lang>\r\n\t\t\t<ID>%s</ID>\r\n\t\t\t<Version>%s</Version>\r\n\t\t\t<Local>%d</Local>\r\n\t\t\t<Filename>%s</Filename>\r\n\t\t\t<Doc>%s</Doc>\r\n\t\t\t<Description>%s</Description>\r\n\t\t\t<Date>%s</Date>\r\n\t\t\t<Visible>%d</Visible>\r\n\t\t</Lang>\r\n"),
								   lng.csLang,
								   lng.csVersion,
								   lng.bLocal,
								   lng.csPath,
								   lng.csDoc,
								   lng.csDesc,
								   lng.csDate,
								   lng.bVisible);
					fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
				}

				nLang += prg.nLangs;
			}

			if(prg.nKeys != 0)
			{
				for(nLoop2=nKey; nLoop2<(nKey+prg.nKeys); nLoop2++)
				{
					key = theApp.m_caKeys.GetAt(nLoop2);

					_stprintf(szBuf, _T("\t\t<Key>\r\n\t\t\t<ID>%d</ID>\r\n\t\t\t<Level>%d</Level>\r\n\t\t\t<Serial>%04d</Serial>\r\n\t\t\t<Code>%04d</Code>\r\n\t\t\t<Comment>%s</Comment>\r\n\t\t</Key>\r\n"), 
						key.nKey, key.nLevel, key.nSerial, key.nCode, key.csComment);
					fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
				}
				nKey += prg.nKeys;
			}
			_stprintf(szBuf, _T("\t</Product>\r\n"));
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
		}

		_stprintf(szBuf, _T("</Programs>\r\n"));
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));

		fh.Close();
	}

	return 0;
}

// show the licensekeys for the current selected program.
void CProgramsDlg::OnBnClickedLicense()
{
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();

	if(pRow)
	{
		int nItem = 0, nLang=0, nProg=0;
		CString csBuf;

		// find out which program we have chosen.
		nItem = pRow->GetIndex();

		PROGRAM prg;
		LANGUAGE lng;
		CString csName, csLang;
		CString csVersion, csVersionWeb;
		int nVersion, nVersionWeb, nLoop, nLoop2, nLangIndex=0;
		BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRow->GetRecord());
		if (pRecord)
		{
			CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
			csName = pItem->GetCaption(0);

			pItem = pRecord->GetItem(COLUMN_VERSION);	// version
			nVersion = (_tstof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
			csVersion = pItem->GetCaption(0);
			csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_NEWVERSION);	// versionweb
			nVersionWeb = (_tstof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
			csVersionWeb = pItem->GetCaption(0);
			csVersionWeb = csVersionWeb.Left(csVersionWeb.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_TLA);	// language
			csLang = pItem->GetCaption(0);

			// search for the program
			for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
			{
				prg = theApp.m_caPrograms.GetAt(nLoop);

				if(prg.nID == pRecord->m_nProgID &&
					prg.csName == csName)
				{
					// search for the language
					for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
					{
						lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

						if(lng.csVersion == csVersion &&
							lng.csVersionWeb == csVersionWeb &&
							lng.csLang == csLang)
						{
							nLang = nLangIndex + nLoop2;
							bFoundLang = TRUE;
							break;
						}
					}

					nProg = nLoop;
					bFoundPrg = TRUE;
					break;
				}

				nLangIndex += prg.nLangs;
			}

			if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

			// send the indexes to the info-dialog.
			if(pDoc->m_nType == 0)	// digitech pro
			{
				theApp.m_nSelProgramDP = nProg;
				theApp.m_nSelLangDP = nLang;
				theApp.m_csSelProgramDP = csName;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				theApp.m_nSelProgramMC = nProg;
				theApp.m_nSelLangMC = nLang;
				theApp.m_csSelProgramMC = csName;
			}
			theApp.m_nType = pDoc->m_nType;


			// open the info-dialog
			CDocTemplate *pTemplate;
			CWinApp* pApp = AfxGetApp();
			CString csDocName, csResStr, csDocTitle;
			csResStr.LoadString(IDR_LICENSES);

			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(csDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				csDocName = '\n' + csDocName;

				if (pTemplate && csDocName.Compare(csResStr) == 0)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while(posDOC != NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
						POSITION pos = pDocument->GetFirstViewPosition();
						if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
						{
							CView* pView = pDocument->GetNextView(pos);
							pView->GetParent()->BringWindowToTop();
							pView->GetParent()->SetFocus();
							posDOC = (POSITION)1;
							break;
						}
					}

					if(posDOC == NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
						CString sDocTitle;
						pDocument->SetTitle(g_pXML->str(1000));
						pDocument->m_nType = pDoc->m_nType;
						pDocument->m_nSelLang = nLoop2;
						pDocument->m_nSelProgram = nLoop;
					}
				}
			}

		}
	}
}


// download/update program button is clicked.
void CProgramsDlg::OnBnClickedDownload()
{
	W3Client w3;
	CString csBuf, csBuf2;
	int nRet = 0, nVersion = 0;
	PROGRAM prg;
	int nLang=0, nProg=0;

	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();

	if(pRow)
	{
		int nItem = 0;

		// find out which program we have chosen.
		nItem = pRow->GetIndex();

		PROGRAM prg;
		LANGUAGE lng;
		KEY key;
		CString csName, csLang;
		CString csVersion, csVersionWeb;
		int nVersion, nVersionWeb, nLoop, nLoop2, nLangIndex=0, nKeyIndex=0;
		BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

		key.csComment = "";
		key.nCode = -1;
		key.nKey = -1;
		key.nLevel = -1;
		key.nSerial = -1;

		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRow->GetRecord());
		if (pRecord)
		{
			CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
			csName = pItem->GetCaption(0);

			pItem = pRecord->GetItem(COLUMN_VERSION);	// version
			nVersion = (_tstof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
			csVersion = pItem->GetCaption(0);
			csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_NEWVERSION);	// versionweb
			nVersionWeb = (_tstof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
			csVersionWeb = pItem->GetCaption(0);
			csVersionWeb = csVersionWeb.Left(csVersionWeb.Find(_T(" ("), 0));
			
			pItem = pRecord->GetItem(COLUMN_TLA);	// language
			csLang = pItem->GetCaption(0);

			// search for the program
			for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
			{
				prg = theApp.m_caPrograms.GetAt(nLoop);

				if(prg.nID == pRecord->m_nProgID &&
					prg.csName == csName)
				{
					// search for the language
					for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
					{
						lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

						if(lng.csVersion == csVersion &&
							lng.csVersionWeb == csVersionWeb &&
							lng.csLang == csLang)
						{
							nLang = nLangIndex + nLoop2;
							bFoundLang = TRUE;
							break;
						}
					}

					if(prg.nKeys != 0)
					{
						for(nLoop2=nKeyIndex; nLoop2<(nKeyIndex+prg.nKeys); nLoop2++)
						{
							key = theApp.m_caKeys.GetAt(nLoop2);
						}
					}

					nProg = nLoop;
					bFoundPrg = TRUE;
					break;
				}

				nKeyIndex += prg.nKeys;
				nLangIndex += prg.nLangs;
			}

			if(bFoundPrg == FALSE || bFoundLang == FALSE) return;


			// send the indexes to the info-dialog.
			if(pDoc->m_nType == 0)	// digitech pro
			{
				theApp.m_nSelProgramDP = nProg;
				theApp.m_nSelLangDP = nLang;
				theApp.m_csSelProgramDP = csName;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				theApp.m_nSelProgramMC = nProg;
				theApp.m_nSelLangMC = nLang;
				theApp.m_csSelProgramMC = csName;
			}

			// create the retrieve-thread and show the GUI.
			CTransferDlg dlg;
			dlg.m_csFilename = lng.csPathWeb;
			dlg.m_csFilenameShort = prg.csName;
			if(pDoc->m_nType == 0) dlg.m_csDestpath = theApp.m_csDP_Filepath;	// digitechpro
			else if(pDoc->m_nType == 1) dlg.m_csDestpath = theApp.m_csMC_Filepath;	// mantax computer
			else if(pDoc->m_nType == 2) dlg.m_csDestpath = theApp.m_csD_Filepath;	// digitech
			dlg.m_nLength = 0;
			dlg.m_nDevice = pDoc->m_nType;	// which device?
			if(theApp.m_bLocal == TRUE)
				dlg.m_nType = 3;	// local
			else
				dlg.m_nType = 2;	// inet
			dlg.m_nDirection = 1;	// download
			dlg.m_csLang = csLang;
			dlg.m_nProdID = prg.nID;
			
			dlg.m_nCode = key.nCode;
			dlg.m_nSerial = key.nSerial;
			dlg.m_nLevel = key.nLevel;

			nRet = dlg.DoModal();

			// Update the Local XML-file to reflect the new file.
			if(nRet == IDOK && dlg.m_bError == FALSE && dlg.m_bAbort == FALSE)
			{
				///////////////////////////////////////////////////
				// unpack the hmi-file                           //
				///////////////////////////////////////////////////
				csBuf = dlg.m_csFilename.Right(4);
				if(csBuf.CompareNoCase(_T(".hmi")) == 0)
				{
					TRY
					{
						g_pZIP->Open(dlg.m_csDestpath + lng.csPathWeb, CZipArchive::zipOpenReadOnly);


						int nIndex = -1;
						nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

						// we got a package.xml?
						if(nIndex != -1)
						{
							g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

							// parse the xml-file
							XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTemppath + _T("package.xml"), _T("XML"));

							if(xNode.isEmpty() != 1)
							{
								xNode = xNode.getChildNode(_T("package"));	// get the first tag, "package"

								if(xNode.isEmpty() != 1)
								{
									XMLNode xChild;
									CString csBuf, csFile, csDest;
									int i, iterator=0;
									BOOL bDoc = FALSE;

									csBuf = xNode.getAttribute(_T("id"));
									if(csBuf != _T(""))
									{
										csDest = dlg.m_csDestpath + csBuf;
										CreateDirectory(csDest, NULL);
										prg.nID = _tstoi(csBuf);
									}

									csBuf = xNode.getAttribute(_T("name"));
									if(csBuf != _T("")) prg.csName = csBuf;

									csBuf = xNode.getAttribute(_T("lang"));
									if(csBuf != _T("")) lng.csLang = csBuf;

									csBuf = xNode.getAttribute(_T("version"));
									if(csBuf != _T("")) lng.nVersionWeb = (int(_tstof(m_cLocale.FixLocale(csBuf)) * 10.0));
									lng.csVersionWeb = csBuf;

									csBuf = xNode.getAttribute(_T("description"));
									if(csBuf != _T("")) lng.csDesc = csBuf;

									csBuf = xNode.getAttribute(_T("date"));
									if(csBuf != _T("")) lng.csDate = csBuf;

									lng.csDoc = _T("");

									int n = xNode.nChildNode(_T("file"));
									for(i=0; i<n; i++)
									{
										xChild = xNode.getChildNode(_T("file"), &iterator);

										// extract the files found in the xml file.
										csFile = xChild.getAttribute(_T("name"));
										nIndex = g_pZIP->FindFile(csFile, CZipArchive::ffNoCaseSens, true);
										g_pZIP->ExtractFile(nIndex, csDest);

										csBuf = xChild.getAttribute(_T("type"));
										if(csBuf == _T("DPDOC")) lng.csDoc += ( csFile + _T(";"));
										else if(csBuf == _T("DP")) lng.csPath = csFile;
									}
								}
							}

							// delete the xml-file
							DeleteFile(theApp.m_csTemppath + _T("package.xml"));
						}

						g_pZIP->Close();
					}

					CATCH(CException, pEx)
					{
						TCHAR   szCause[255];
						CString strFormatted;

						pEx->GetErrorMessage(szCause, 255);
						strFormatted = _T("Error: ");
						strFormatted += szCause;

						AfxMessageBox(strFormatted);
					}
					END_CATCH


					// delete the package
					DeleteFile(dlg.m_csDestpath + lng.csPathWeb);
				}
				else
				{
					lng.csPath = lng.csPathWeb;
				}
				///////////////////////////////////////////////////

				lng.bLocal = TRUE;
				lng.nVersion = lng.nVersionWeb;
				lng.csVersion = lng.csVersionWeb;
				theApp.m_caLanguages.SetAt(nLang, lng);

				SaveXML();

				// update the version
				CXTPReportRecordItem* pItem;
				pItem = pRecord->GetItem(COLUMN_VERSION);	// version

				CString csBuf2;
				csBuf2.Format(_T(" (%s)"), lng.csDate);
				csBuf = lng.csVersion;
				if(lng.csDate != "") csBuf += csBuf2;
				pItem->SetCaption(csBuf);

				pRecord->SetRead();	// remove the flag (if it was there)


				// enable the download- and info-buttons.
				((CProgramsFrame*)GetParent())->SetButtonEnabled(0, TRUE);	// info

				// do we have an newer version to download?
				if(lng.csVersionWeb != _T("") /*&& (lng.nVersion < lng.nVersionWeb)*/)
					((CProgramsFrame*)GetParent())->SetButtonEnabled(1, TRUE);	// download
				else
					((CProgramsFrame*)GetParent())->SetButtonEnabled(1, FALSE);	// download
			}
		}
	}
}

void CProgramsDlg::OnSetFocus(CWnd* pOldWnd)
{
	CView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPReportRows* pRows = GetReportCtrl().GetRows();
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();
	int nItem;
	if(pRow) nItem = pRow->GetIndex();
	if(pRows->GetCount() > 0)
	{
		if(nItem > 1)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, TRUE);
		}

		if(nItem < pRows->GetCount()-1)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, TRUE);
		}
	}

	GetReportCtrl().SetFocus();
}

// navigation
void CProgramsDlg::OnNavigate(int nID)
{
	int nRow;

	CXTPReportRows* pRows = GetReportCtrl().GetRows();
	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();
	nRow = pRow->GetIndex();

	switch(nID)
	{
		case ID_DBNAVIG_START:
			nRow = 1;
		break;

		case ID_DBNAVIG_NEXT:
			nRow++;
		break;

		case ID_DBNAVIG_PREV:
			nRow--;
		break;

		case ID_DBNAVIG_END:
			nRow = pRows->GetCount()-1;
		break;
	};

	GetReportCtrl().SetFocusedRow(pRows->GetAt(nRow));

	OnBnClickedLicense();

	GetParent()->BringWindowToTop();
	GetParent()->SetFocus();
}

void CProgramsDlg::OnShowFieldFilter()
{
	CString S;
	CProgramsFrame* pWnd = (CProgramsFrame*)GetParentFrame();
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		if(bShow == TRUE)
		{
			m_wndFilterEdit.SetWindowText(GetReportCtrl().GetFilterText());
		}
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != "");
	}
}

void CProgramsDlg::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CProgramsFrame* pWnd = (CProgramsFrame*)GetParentFrame();
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}
