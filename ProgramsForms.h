#ifndef _ProgramsFORMS_H_
#define _ProgramsFORMS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ProgramsDlg.h"
#include "SampleSuiteDlg.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CProgramsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CProgramsFrame)
public:
	CProgramsFrame();

// Attributes
public:
	CXTPDockingPaneManager m_paneManager;
	CXTPPropertyGrid m_wndPropertyGrid;

	// Toolbar
	CXTPToolBar m_wndToolBar;

// Operations
public:
	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
	//{{AFX_MSG(CProgramsFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
protected:
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
