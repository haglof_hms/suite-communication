// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500


// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS


//#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

//#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
//#endif // _AFX_NO_OLE_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "Resource.h"

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#define IDC_TABCONTROL						9998
const LPCTSTR PROGRAM_NAME			  		= _T("Communication");					// Name of suite/module, used on setting Language filename; 051214 p�d

typedef struct _tagPROGRAM
{
	int nID;			// product-id
	int nProgtype;		// type of program
	CString csName;		// name of the program
	CString csLang;		// languages the program support

	int nKeys;			// num of keys
	int nLangs;			// num of langs
} PROGRAM;

typedef struct _tagLANGUAGE
{
	bool bLocal;		// does the file exist locally?
	CString csLang;		// name of the language
	int nVersion;		// version of the local file (if any)
	int nVersionWeb;	// version of the web file
	CString csPath;		// path to the program
	CString csPathWeb;	// webpath to the program
	CString csDesc;		// description about the program
	CString csDesc2;	// description about the new version
	CString csDate;
	CString csDateWeb;
	bool bVisible;		// visible or not
	
	CString csDoc;		// path to documentation

	CString csVersion;
	CString csVersionWeb;
} LANGUAGE;

typedef struct _tagKEY
{
	int nKey;		// the key id
	int nSerial;	// serialnumber of hardware
	int nLevel;		// licenselevel
	int nCode;		// code
	CString csComment;	// comment
} KEY;

// communications defines
#define WM_COM_PACKET	WM_USER + 1000
#define WM_COM_ABORT	WM_USER + 1001
#define WM_COM_LENGTH	WM_USER + 1002
#define WM_COM_DONE		WM_USER + 1003
#define WM_COM_FILE		WM_USER + 1004
#define WM_COM_ASCII	WM_USER + 1005

#include "ResLangFileReader.h"
#include "pad_hms_miscfunc.h"
#include <afxdlgs.h>
#include <afxctl.h>
#include <htmlhelp.h>
#include "HelpComm.h"
