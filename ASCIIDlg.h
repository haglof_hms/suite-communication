#pragma once

#include "SampleSuiteForms.h"
#include "ComThread.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CASCIIFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CASCIIFrame)

	BOOL m_bOnce;
public:
	CASCIIFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CASCIIFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CASCIIFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);

	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
	//{{AFX_MSG(CASCIIFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};

// CASCIIDlg form view

class CASCIIDlg : public CXTResizeFormView //CFormView
{
	DECLARE_DYNCREATE(CASCIIDlg)

protected:
	CASCIIDlg();           // protected constructor used by dynamic creation
	virtual ~CASCIIDlg();

	CFont m_font;
	CMDISampleSuiteDoc* pDoc;
	CString m_csStatus;

	int m_nComport;
	int m_nBaud;
	CString m_csFilepath;
	CString m_csBuf;

public:
	HWND m_hParentWnd;
	CString m_csDefaultpath;

	enum { IDD = IDD_ASCII };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	CComThread* m_pCComThread;	// the communications-thread
	LRESULT OnPacket(WPARAM wParm, LPARAM lParm);
//	LRESULT OnLength(WPARAM wParm, LPARAM lParm);
	LRESULT OnDone(WPARAM wParm, LPARAM lParm);
//	LRESULT OnFile(WPARAM wParm, LPARAM lParm);
	afx_msg LRESULT OnCommandHelp(WPARAM, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
public:
	BOOL m_bChanged;
	CString m_csText;
	afx_msg void OnDestroy();
	afx_msg int OnSave();
	afx_msg void OnReport();
	afx_msg void OnErase();
	afx_msg void OnNew();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
};
