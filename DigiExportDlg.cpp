// DigiExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "DigiExportDlg.h"
#include ".\digiexportdlg.h"
#include <cmath>


// CDigiExportDlg dialog

IMPLEMENT_DYNAMIC(CDigiExportDlg, CDialog)
CDigiExportDlg::CDigiExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDigiExportDlg::IDD, pParent)
{
	m_csName = _T("");
	m_nPart = 0;
	m_nMethod = 0;
	m_csRadius = _T("");
	m_fRadius = 0;
	m_csArea = _T("");
	m_fArea = 0;
	m_nPrevSel = -1;
	m_csInfo = _T("");
}

CDigiExportDlg::~CDigiExportDlg()
{
}

void CDigiExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_STAND, m_cListBox);
	DDX_Text(pDX, IDC_EDIT_NAME, m_csName);
	DDX_Text(pDX, IDC_EDIT_PART, m_nPart);
	DDX_Control(pDX, IDC_COMBO_METHOD, m_cMethod);
	DDX_CBIndex(pDX, IDC_COMBO_METHOD, m_nMethod);
	DDX_Text(pDX, IDC_EDIT_RADIUS, m_csRadius);
	DDX_Text(pDX, IDC_EDIT_AREA, m_csArea);
	DDX_Text(pDX, IDC_STATIC_INFO, m_csInfo);
}


BEGIN_MESSAGE_MAP(CDigiExportDlg, CDialog)
	ON_LBN_SELCHANGE(IDC_LIST_STAND, OnLbnSelchangeList1)
	ON_EN_CHANGE(IDC_EDIT_RADIUS, OnEnChangeEditRadius)
	ON_EN_CHANGE(IDC_EDIT_AREA, OnEnChangeEditArea)
	ON_EN_UPDATE(IDC_EDIT_NAME, OnEnUpdateEditName)
	ON_BN_CLICKED(IDOK, &CDigiExportDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CDigiExportDlg message handlers
BOOL CDigiExportDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// change language
	SetDlgItemText(IDC_STATIC_STANDS, g_pXML->str(1094));	// stands
	SetDlgItemText(IDC_STATIC_NAME, g_pXML->str(1095));	// name
	SetDlgItemText(IDC_STATIC_PART, g_pXML->str(1096));	// part
	SetDlgItemText(IDC_STATIC_METHOD, g_pXML->str(1097));	// method
	SetDlgItemText(IDC_STATIC_RADIUS, g_pXML->str(1108));	// radius
	SetDlgItemText(IDC_STATIC_AREA, g_pXML->str(1109));	// area


	STAND stand;
	CString csBuf;

	for(int nStand=0; nStand<theApp.m_caStands.GetSize(); nStand++)
	{
		stand = theApp.m_caStands.GetAt(nStand);
		
		csBuf.Format(_T("%d - %s"), nStand+1, stand.szName);
		m_cListBox.AddString(csBuf);
	}

	m_cMethod.AddString(g_pXML->str(1098));
	m_cMethod.AddString(g_pXML->str(1107));

	if(theApp.m_caStands.GetSize() > 0)
	{
		m_cListBox.SetCurSel(0);
		OnLbnSelchangeList1();
	}

	return TRUE;
}

// user selected a stand
void CDigiExportDlg::OnLbnSelchangeList1()
{
	STAND stand;
	CString csBuf;
	int nSel = m_cListBox.GetCurSel();

	// save the previous stand, if needed.
	if(m_nPrevSel != -1)
	{
		UpdateData(TRUE);

		stand = theApp.m_caStands.GetAt(m_nPrevSel);
		_tcscpy(stand.szName, m_csName);
		stand.nPart = m_nPart;
		stand.nMethod = m_nMethod;

		csBuf = m_csRadius;
		csBuf.Replace(',', '.');
		m_fRadius = _tstof(csBuf);
		stand.fPlotRadius = m_fRadius;

		theApp.m_caStands.SetAt(m_nPrevSel, stand);

		csBuf.Format(_T("%d - %s"), m_nPrevSel+1, stand.szName);
		m_cListBox.DeleteString(m_nPrevSel);
		m_cListBox.InsertString(m_nPrevSel, csBuf);
	}

	// load the data into the editfields etc
	if(nSel != -1)
	{
		m_nPrevSel = nSel;

		stand = theApp.m_caStands.GetAt(nSel);
		
		m_csName = stand.szName;
		m_nPart = stand.nPart;
		m_nMethod = stand.nMethod;
		m_csRadius.Format(_T("%.2f"), stand.fPlotRadius);
		m_csArea.Format(_T("%.2f"), stand.fPlotRadius * stand.fPlotRadius * 3.1415);

		m_csInfo.Format(_T("%s %d, %s %d"), g_pXML->str(1110), stand.nNumPlots, g_pXML->str(1111), stand.nNumTrees);

		UpdateData(FALSE);
	}
}

// user changed the radius
void CDigiExportDlg::OnEnChangeEditRadius()
{
	UpdateData(TRUE);

	CString csBuf;
	csBuf = m_csRadius;
	csBuf.Replace(',', '.');

	m_fRadius = _tstof(csBuf);
	m_fArea = m_fRadius * m_fRadius * 3.1415;

	m_csArea.Format(_T("%.2f"), m_fArea);

	UpdateData(FALSE);
}

// user changed the area
void CDigiExportDlg::OnEnChangeEditArea()
{
	UpdateData(TRUE);

	CString csBuf;
	csBuf = m_csArea;
	csBuf.Replace(',', '.');

	m_fArea = _tstof(csBuf);
	m_fRadius = sqrt(m_fArea / 3.1415);

	m_csRadius.Format(_T("%.2f"), m_fRadius);

	UpdateData(FALSE);
}

void CDigiExportDlg::OnEnUpdateEditName()
{
	UpdateData(TRUE);

	CString csBuf;
	int nSel = m_cListBox.GetCurSel();
	csBuf.Format(_T("%d - %s"), nSel+1, m_csName);

	STAND stand;
	stand = theApp.m_caStands.GetAt(nSel);
	_tcscpy(stand.szName, m_csName);
	theApp.m_caStands.SetAt(nSel, stand);

	m_cListBox.DeleteString(nSel);
	m_cListBox.InsertString(nSel, csBuf);
	m_cListBox.SetCurSel(nSel);
}

void CDigiExportDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	int nSel = m_cListBox.GetCurSel();
	STAND stand;
	stand = theApp.m_caStands.GetAt(nSel);
	stand.fPlotRadius = m_fRadius;
	stand.nMethod = m_nMethod;
	stand.nPart = m_nPart;
	_tcscpy(stand.szName, m_csName);
	theApp.m_caStands.SetAt(nSel, stand);

	OnOK();
}
